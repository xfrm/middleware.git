package main

import (
	"context"
	"log"
	"time"

	"gitee.com/xfrm/middleware/xlog"
)

// LogFile write log to file+console(debug)
func LogFile(xl *xlog.XLogger) {
	xl.Debug(context.TODO(), "debug message")
	xl.Debugf(context.TODO(), "debug message %s", "format")
	xl.Debugw(context.TODO(), "debug message key-value",
		"bkey", false,
		"key1", "value1",
		"intk", 10,
		"fkey", 66.6)
	xl.Info(context.TODO(), "info message")
	xl.Infof(context.TODO(), "info message %s", "format")
	xl.Infow(context.TODO(), "info message key-value",
		"bkey", false,
		"key1", "value1",
		"intk", 10,
		"fkey", 32.1,
		"map_key", map[string]interface{}{"key1": "value1", "key2": 10})

	xl.Warn(context.TODO(), "warn message")
	xl.Warnf(context.TODO(), "warn message %s", "format")
	xl.Warnw(context.TODO(), "warn message key-value",
		"bkey", false,
		"key1", "value1",
		"intk", 10,
		"fkey", 32.2)

	xl.Error(context.TODO(), "error message")
	xl.Errorf(context.TODO(), "error message: %s", "format")
	xl.Errorw(context.TODO(), "error message key-value",
		"bkey", false,
		"key1", "value1",
		"intk", 10,
		"fkey", 32.3)
}

// LogConsole log to console
func LogConsole(xl *xlog.XLogger) {
	xl.Info(context.TODO(), "info message")
	xl.Infof(context.TODO(), "info message %s", "format")
	xl.Infow(context.TODO(), "info message key-value",
		"bkey", false,
		"key1", "value1",
		"intk", 10,
		"fkey", 32.0)

	xl.Warn(context.TODO(), "warn message")
	xl.Warnf(context.TODO(), "warn message %s", "format")
	xl.Warnw(context.TODO(), "warn message key-value",
		"bkey", false,
		"key1", "value1",
		"intk", 10,
		"fkey", 32.0,
		"map_key", map[string]interface{}{"key1": "value1", "key2": 10})

	xl.Error(context.TODO(), "error message")
	xl.Errorf(context.TODO(), "error message: %s", "format")
	xl.Errorw(context.TODO(), "error message key-value",
		"bkey", false,
		"key1", "value1",
		"intk", 10,
		"fkey", 32.0)
}

func main() {
	// 默认按小时切割, path, filename, maxage, level, format type
	xl, err := xlog.New("./", "test.log", xlog.InfoLevel, xlog.JSONFormatType, false)
	if err != nil {
		log.Fatalf("xlog new failed, %v", err)
	}
	xl.SetService("test_service")
	defer xl.Sync()

	xlConsole, _ := xlog.NewConsole(xlog.DebugLevel)
	xlConsole.SetService("test_service")
	defer xlConsole.Sync()
	i := 0
	for {
		i++
		LogFile(xl)
		if i == 3 {
			xl.SetLevel(xlog.DebugLevel)
		}
		if i == 10 {
			xl.SetLevel(xlog.InfoLevel)
		}
		//LogConsole(xlConsole)
		time.Sleep(time.Second * 1)
	}
}
