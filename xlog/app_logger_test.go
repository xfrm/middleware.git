package xlog

import (
	"context"
	"testing"

	"gitee.com/xfrm/middleware/xcontext"

	"github.com/stretchr/testify/assert"
)

func TestSetAppLogLevel(t *testing.T) {
	InitAppLog("./", "app.log", InfoLevel)
	assert.Equal(t, InfoLevel, AppLogLevel())
	SetAppLogLevel("debug")
	assert.Equal(t, DebugLevel, AppLogLevel())
}

func TestContextLane(t *testing.T) {
	ctx := context.Background()
	Infof(ctx, "master")
	ctx = context.WithValue(ctx, xcontext.ContextKeyControl, &simpleContextControlRouter{
		group: "t1",
	})
	Infof(ctx, "t1")
	Warnf(ctx, "t1")
	Errorf(ctx, "t1")
}

type simpleContextControlRouter struct {
	group string
}

func (s simpleContextControlRouter) GetControlRouteGroup() (string, bool) {
	return s.group, true
}

func (s simpleContextControlRouter) SetControlRouteGroup(group string) error {
	s.group = group
	return nil
}
