module gitee.com/xfrm/middleware/xips


go 1.12

require (
	github.com/ipipdotnet/ipdb-go v1.3.3
	github.com/oschwald/maxminddb-golang v1.10.0
	github.com/sinlov/qqwry-golang v0.0.0-20191204062238-bea9868bbbf4
)
