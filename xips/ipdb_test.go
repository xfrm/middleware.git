package xips

import (
	"context"
	"io/ioutil"
	"strings"
	"testing"
)

func Test_Ipdb(t *testing.T) {
	filein := "/Users/tanghc/Downloads/qqwry.ipdb"
	ipdb, _ := NewIpdb(filein, 0)
	//var mmdbRecord interface{}
	//var mmdbRecord interface{}
	for _, v := range readIps() {
		r := ipdb.GetRegion(context.Background(), v.ip)
		if r.Province != v.name {
			t.Logf("ip: %s, provin: %s, r: %v", v.ip, v.name, r)
		}
	}
}

type ipname struct {
	ip   string
	name string
}

func readIps() []ipname {
	var ipnames []ipname
	data, _ := ioutil.ReadFile("/Users/tanghc/Documents/project/ipalfish/gitlab/server/baseutil/geoip/ips/ips.txt")
	lines := strings.Split(string(data), "\n")
	for _, line := range lines {
		arr := strings.Split(line, "\t")
		ipnames = append(ipnames, ipname{
			ip:   arr[1],
			name: arr[0],
		})
	}
	return ipnames
}
