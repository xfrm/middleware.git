package xips

import (
	"bytes"
	"compress/zlib"
	"context"
	"encoding/binary"
	"io/ioutil"
	"testing"
)

func Test_Cz88(t *testing.T) {
	buf, err := GetOnline()
	if err != nil {
		panic(err)
	}

	filein := "/Users/tanghc/Downloads/qqwry.dat"

	err = ioutil.WriteFile(filein, buf, 0644)
	if err != nil {
		panic(err)
	}

	cz88, _ := NewQQWry(filein, 0)
	//r := cz88.GetRegion(context.Background(), "121.196.192.188")
	//r := cz88.GetRegion(context.Background(), "81.2.69.142")
	//r := cz88.GetRegion(context.Background(), "110.19.191.196")
	r := cz88.GetRegion(context.Background(), "36.182.44.61")
	t.Logf("r: %v  ", r)
}

// @ref https://zhangzifan.com/update-qqwry-dat.html

func getKey() (uint32, error) {
	//resp, err := http.Get("http://update.cz88.net/ip/copywrite.rar")
	//if err != nil {
	//	return 0, err
	//}
	//defer resp.Body.Close()
	//
	//if body, err := ioutil.ReadAll(resp.Body); err != nil {
	if body, err := ioutil.ReadFile("/Users/tanghc/Downloads/copywrite.rar"); err != nil {
		return 0, err
	} else {
		// @see https://stackoverflow.com/questions/34078427/how-to-read-packed-binary-data-in-go
		return binary.LittleEndian.Uint32(body[5*4:]), nil
	}
}

func GetOnline() ([]byte, error) {
	//resp, err := http.Get("http://update.cz88.net/ip/qqwry.rar")
	//if err != nil {
	//	return nil, err
	//}
	//defer resp.Body.Close()
	//
	//if body, err := ioutil.ReadAll(resp.Body); err != nil {
	if body, err := ioutil.ReadFile("/Users/tanghc/Downloads/qqwry.rar"); err != nil {
		return nil, err
	} else {
		if key, err := getKey(); err != nil {
			return nil, err
		} else {
			for i := 0; i < 0x200; i++ {
				key = key * 0x805
				key++
				key = key & 0xff

				body[i] = byte(uint32(body[i]) ^ key)
			}

			reader, err := zlib.NewReader(bytes.NewReader(body))
			if err != nil {
				return nil, err
			}

			return ioutil.ReadAll(reader)
		}
	}
}
