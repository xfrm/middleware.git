package xconcurrent

import (
	"context"
	"fmt"
	"testing"
)

var ctx = context.Background()

func Test_PageExecute(t *testing.T) {
	output := PageExecute(ctx, 5, 2, []interface{}{int64(1), int64(2), int64(3), int64(4), int64(5), int64(6), int64(7), int64(8)}, func(ctx2 context.Context, onePageInput []interface{}) ([]interface{}, error) {
		r := make([]interface{}, 0, len(onePageInput))
		for _, v := range onePageInput {
			id, ok := v.(int64)
			fmt.Println("check ", id, ok)
			r = append(r, id*10)
		}
		return r, nil
	})
	for _, v := range output {
		id, ok := v.(int64)
		fmt.Println("result:", id, ok)
	}
}
func Test_ProduceCosumeExecute(t *testing.T) {
	output := ProduceCosumeExecute(ctx, 5, []interface{}{int64(1), int64(2), int64(3), int64(4), int64(5), int64(6), int64(7), int64(8)}, func(ctx2 context.Context, onePageInput []interface{}) ([]interface{}, error) {
		r := make([]interface{}, 0, len(onePageInput))
		for _, v := range onePageInput {
			id, ok := v.(int64)
			fmt.Println("check ", id, ok)
			r = append(r, id*10)
		}
		return r, nil
	})
	for _, v := range output {
		id, ok := v.(int64)
		fmt.Println("result:", id, ok)
	}
}
func Test_ProduceCosumeExecuteV2(t *testing.T) {
	output := ProduceCosumeExecuteV2(ctx, 5, []interface{}{int64(1), int64(2), int64(3), int64(4), int64(5), int64(6), int64(7), int64(8)}, func(ctx2 context.Context, one interface{}) ([]interface{}, error) {
		r := make([]interface{}, 0)
		id, ok := one.(int64)
		fmt.Println("check ", id, ok)
		r = append(r, id*10)
		return r, nil
	})
	for _, v := range output {
		id, ok := v.(int64)
		fmt.Println("result:", id, ok)
	}
}
