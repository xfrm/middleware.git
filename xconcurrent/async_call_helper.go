package xconcurrent

import (
	"context"
	"fmt"
	"gitee.com/xfrm/middleware/xlog"
	"sync"
	"sync/atomic"
	"time"
)

type AsyncCallHelper struct {
	callChan   chan func() error
	callCount  int64
	execCount  int64
	conCount   int64
	withBuffer bool
	callBuffer []func() error
	mutex      sync.Mutex
	since      time.Time
}

func NewAsyncCallHelper(maxChanLen, maxprocs int, withBuffer bool) *AsyncCallHelper {
	m := &AsyncCallHelper{
		callChan:   make(chan func() error, maxChanLen),
		withBuffer: withBuffer,
		since:      time.Now(),
	}

	for i := 0; i < maxprocs; i++ {
		go m.run()
	}

	go m.qps()

	return m
}

func (m *AsyncCallHelper) AsyncCall(call func() error) {
	//fun := "AsyncCallHelper.AsyncCall -->"

	atomic.AddInt64(&m.callCount, 1)

	if m.withBuffer == false {
		m.callChan <- call

	} else {

		select {
		case m.callChan <- call:

		default:
			//fmt.Printf("%s insert buffer", fun)

			m.mutex.Lock()
			m.callBuffer = append(m.callBuffer, call)
			m.mutex.Unlock()

		}
	}
}
func (m *AsyncCallHelper) doCall(call func() error) error {
	atomic.AddInt64(&m.execCount, 1)
	atomic.AddInt64(&m.conCount, 1)
	err := call()
	atomic.AddInt64(&m.conCount, -1)
	return err
}

func (m *AsyncCallHelper) run() {
	fun := "AsyncCallHelper.run-->"

	ticker := time.NewTicker(time.Second * 30)
	defer ticker.Stop()

	for {
		select {
		case call := <-m.callChan:
			err := m.doCall(call)
			if err != nil {
				fmt.Printf("%s err:%s", fun, err)
			}

		case <-ticker.C:
			if m.withBuffer {
				m.handleCallBuffer()
			}
		}
	}
}

func (m *AsyncCallHelper) handleCallBuffer() {
	fun := "AsyncCallHelper.handleCallBuffer-->"

	m.mutex.Lock()
	buffer := m.callBuffer
	m.callBuffer = nil
	m.mutex.Unlock()

	for _, call := range buffer {
		err := m.doCall(call)
		if err != nil {
			fmt.Printf("%s err:%s", fun, err)
		}
	}
}

func (m *AsyncCallHelper) qps() {
	fun := "AsyncCallHelper.qps -->"

	ticker := time.NewTicker(30 * time.Second)
	for {
		select {
		case <-ticker.C:
			count := atomic.SwapInt64(&m.callCount, 0)
			execount := atomic.SwapInt64(&m.execCount, 0)
			rt := int64(0)
			if execount > 0 {
				rt = time.Since(m.since).Nanoseconds() / 1000000 / execount
			}
			xlog.Infof(context.Background(), "%s qps:%d, exeuqps:%d, conc:%d, rt:%dms", fun, count/30, execount/30, m.conCount, rt)
			m.resetClock()

		}
	}
}
func (m *AsyncCallHelper) resetClock() {
	m.mutex.Lock()
	defer m.mutex.Unlock()
	m.since = time.Now()
}
