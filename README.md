# 简介
seaweed是伴鱼的微服务框架，包含各类基础库，如服务注册、服务发现、打点统计、限流降级、redis sdk、mysql sdk等等。

## 基础库
### 打点统计
[xprometheus](./xstat/xmetric/xprometheus/README.md)

### mysql/tidb驱动库
[xsql](./xsql/README.md)

### mongodb驱动库
[xmgo](./xmgo/README.md)

### 日志库
[xlog](./xlog/README.md)

### 配置库
[xconfig](./xconfig/README.md)

### mq sdk
[xmq](./xmq/README.md)

### redis sdk
[xcache](./xcache/README.md)

### xutil(工具库)
[xutil](./xutil/README.md)

### xsuite(测试套件库)
[xsuite](./xsuite/README.md)
