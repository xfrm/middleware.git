package xtrace

const defaultSpanOpName = "defaultOp"

// TransformOptions keep options for converting a *thriftutil.Context into context.Context
type TransformOptions struct {
	OpName    string
	NoSpan    bool
	NoHead    bool
	NoControl bool
}

// TransformOption defines the interface for any specific option type
type TransformOption interface {
	apply(*TransformOptions)
}

// OpName specify operation name for a generated span, NoSpan can render this option useless.
type OpName string

func (o OpName) apply(tops *TransformOptions) {
	tops.OpName = string(o)
}

// NoSpan stop the conversion from generating new span
type NoSpan bool

func (i NoSpan) apply(topts *TransformOptions) {
	topts.NoSpan = bool(i)
}

// NoHead stop the conversion from keeping the context head
type NoHead bool

func (i NoHead) apply(topts *TransformOptions) {
	topts.NoHead = bool(i)
}

// NoControl stop the conversion from keeping the control head
type NoControl bool

func (i NoControl) apply(topts *TransformOptions) {
	topts.NoControl = bool(i)
}
