package mock

import (
	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/log"
)

type Span struct {
	SetTagFn func(key string, value interface{})
	SetTagCn int
}

func (s *Span) Finish() {
	panic("implement me")
}

func (s *Span) FinishWithOptions(opts opentracing.FinishOptions) {
	panic("implement me")
}

func (s *Span) Context() opentracing.SpanContext {
	panic("implement me")
}

func (s *Span) SetOperationName(operationName string) opentracing.Span {
	panic("implement me")
}

func (s *Span) SetTag(key string, value interface{}) opentracing.Span {
	s.SetTagFn(key, value)
	s.SetTagCn += 1
	return s
}

func (s *Span) LogFields(fields ...log.Field) {
	panic("implement me")
}

func (s *Span) LogKV(alternatingKeyValues ...interface{}) {
	panic("implement me")
}

func (s *Span) SetBaggageItem(restrictedKey, value string) opentracing.Span {
	panic("implement me")
}

func (s *Span) BaggageItem(restrictedKey string) string {
	panic("implement me")
}

func (s *Span) Tracer() opentracing.Tracer {
	panic("implement me")
}

func (s *Span) LogEvent(event string) {
	panic("implement me")
}

func (s Span) LogEventWithPayload(event string, payload interface{}) {
	panic("implement me")
}

func (s Span) Log(data opentracing.LogData) {
	panic("implement me")
}
