package xtrace

import (
	"context"
	"net/http"
	"testing"

	"github.com/opentracing/opentracing-go"
)

func TestTraceHttpRequest(t *testing.T) {
	initTracer(t)
	defer clearTracer(t)

	tracer := getTracer(t)
	defaultReq, _ := http.NewRequest("GET", "www.ipalfish.com", nil)

	cases := []struct {
		ctx             context.Context
		req             *http.Request
		shouldHaveError bool
	}{
		{nil, nil, true},
		{nil, defaultReq, true},
		{context.Background(), nil, true},
		{context.Background(), defaultReq, true},
		{opentracing.ContextWithSpan(genContext(nil), tracer.StartSpan("op")), defaultReq, false},
	}

	for i, c := range cases {
		err := TraceHTTPRequest(c.ctx, c.req)
		if (err != nil) && (!c.shouldHaveError) {
			t.Errorf("Test %d: should not have err:%v", i, err)
		}

		if (err == nil) && c.shouldHaveError {
			t.Errorf("Test %d: should have err", i)
		}

		if err == nil {
			headers := c.req.Header
			if _, ok := headers["Mockpfx-Ids-Sampled"]; !ok {
				t.Errorf("Test %d: should have sample info in http header", i)
			}

			if _, ok := headers["Mockpfx-Ids-Spanid"]; !ok {
				t.Errorf("Test %d: should have spanid in http header", i)
			}

			if _, ok := headers["Mockpfx-Ids-Traceid"]; !ok {
				t.Errorf("Test %d: should have traceid in http header", i)
			}
		}
	}
}
