// Copyright 2014 The mqrouter Author. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"context"
	"fmt"
	"time"

	"gitee.com/xfrm/middleware/xcontext"
	"gitee.com/xfrm/middleware/xlog"
	"gitee.com/xfrm/middleware/xmq"
	"gitee.com/xfrm/middleware/xtrace"

	"github.com/opentracing/opentracing-go"
)

type Msg struct {
	Id   int
	Body string
}

type simpleContextControlRouter struct {
	group string
}

func (s simpleContextControlRouter) GetControlRouteGroup() (string, bool) {
	return s.group, true
}

func (s simpleContextControlRouter) SetControlRouteGroup(group string) error {
	s.group = group
	return nil
}

func main() {
	examplePulsar()
}

func exampleKafka() {
	_ = xtrace.InitDefaultTracer("mq.test")
	topic := "palfish.test.test"

	ctx := context.Background()
	sc := &simpleContextControlRouter{group: "t1"}
	ctx = context.WithValue(ctx, xcontext.ContextKeyHead, "hahahaha")
	ctx = context.WithValue(ctx, xcontext.ContextKeyControl, sc)
	span, ctx := opentracing.StartSpanFromContext(ctx, "main")
	if span != nil {
		defer span.Finish()
	}

	//go func() {
	//	var msgs []xmq.Message
	//	for i := 0; i < 3; i++ {
	//		value := &Msg{
	//			Id:   1,
	//			Body: fmt.Sprintf("%d", i),
	//		}
	//
	//		msgs = append(msgs, xmq.Message{
	//			Key:   value.Body,
	//			Value: value,
	//		})
	//		err := xmq.WriteMsg(ctx, topic, value.Body, value)
	//		xlog.Infof(ctx, "in msg: %v, err:%v", value, err)
	//	}
	//	err := xmq.WriteMsgs(ctx, topic, msgs...)
	//	xlog.Infof(ctx, "in msgs: %v, err:%v", msgs, err)
	//}()

	go func() {
		msg := &Msg{
			Id:   1,
			Body: "test",
		}
		jobID, err := xmq.WriteDelayMsg(ctx, topic, msg, 5)
		xlog.Infof(ctx, "write delay msg, jobID = %s, err = %v", jobID, err)
	}()

	//ctx1 := context.Background()
	/*
		//err := mq.SetOffsetAt(ctx1, topic, 1, time.Date(2019, time.December, 4, 0, 0, 0, 0, time.UTC))
		//err := mq.SetOffset(ctx1, topic, 1, -2)
		if err != nil {
			slog.Infof(ctx, "2222222222222222,err:%v", err)
		}
	*/
	//go func() {
	//	for i := 0; i < 100; i++ {
	//		var msg Msg
	//		ctx, err := xmq.ReadMsgByGroup(ctx, topic, "group3", &msg)
	//		xlog.Infof(ctx, "1111111111111111out msg: %v, ctx:%v, err:%v", msg, ctx, err)
	//	}
	//}()

	go func() {
		for i := 0; i < 10; i++ {
			var msg Msg
			ctx, ack, jobID, err := xmq.FetchDelayMsgAndID(ctx, topic, &msg)
			xlog.Infof(ctx, "1111111111111111out msg: %v, ctx:%v, err:%v", msg, ctx, err)
			err = ack.Ack(ctx)
			xlog.Infof(ctx, "2222222222222222out msg: %v, ctx:%v, err:%v", msg, ctx, err)
			xlog.Infof(ctx, "jobID = %s", jobID)
			time.Sleep(1 * time.Second)
		}
	}()

	time.Sleep(15 * time.Second)
	xmq.Close()
}

func examplePulsar() {
	_ = xtrace.InitDefaultTracer("mq.test")
	topic := "persistent://base/test/test"

	ctx := context.Background()
	ctx = context.WithValue(ctx, xcontext.ContextKeyHead, "hahahaha")
	span, ctx := opentracing.StartSpanFromContext(ctx, "main")
	if span != nil {
		defer span.Finish()
	}

	go func() {
		for i := 0; i < 10; i++ {
			msg := &Msg{
				Id:   i + 10,
				Body: "test",
			}
			messageID, err := xmq.WritePulsarMsg(ctx, topic, fmt.Sprintf("%d", i), msg)
			xlog.Infof(ctx, "WritePulsarMsg, messageID: %v, err: %v", messageID, err)
		}
	}()

	go func() {
		for i := 0; i < 100; i++ {
			var msg Msg
			ctx, handler, err := xmq.FetchPulsarMsg(ctx, topic, "test-pulsar", &msg)
			xlog.Infof(ctx, "1111111111111111out i: %d msg: %v, ctx:%v, err:%v", i, msg, ctx, err)
			err = handler.Ack(ctx)
			xlog.Infof(ctx, "ack msg i: %d msg: %v, ctx:%v, err:%v", i, msg, ctx, err)
			time.Sleep(1 * time.Second)
		}
	}()

	time.Sleep(20 * time.Second)
	xmq.Close()
}
