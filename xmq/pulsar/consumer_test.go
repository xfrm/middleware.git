package pulsar

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_getConsumerName(t *testing.T) {
	fmt.Println(getName())
}

func TestNewConsumerWithConfig(t *testing.T) {
	consumer, err := NewConsumerWithConfig(context.Background(), nil)
	assert.Nil(t, consumer, nil)
	assert.Error(t, err, ErrConfigNotNull)
}

func TestNewConsumer(t *testing.T) {
	consumer, err := NewConsumer(context.Background(), "persistent://base/test/test", "test")
	assert.NoError(t, err)
	assert.NotNil(t, consumer)
	assert.NoError(t, consumer.Close())
}
