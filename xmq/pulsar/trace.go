package pulsar

import (
	"context"
	"encoding/json"

	"gitee.com/xfrm/middleware/xcontext"
	"gitee.com/xfrm/middleware/xtrace"
	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
)

type Payload struct {
	Carrier opentracing.TextMapCarrier `json:"c"`
	Value   string                     `json:"v"`
	Head    interface{}                `json:"h"`
	Control *mqPayloadControl          `json:"t"`
}

type mqPayloadControl struct {
	Group string `json:"group"`
}

func (s *mqPayloadControl) GetControlRouteGroup() (string, bool) {
	return s.Group, true
}

func (s *mqPayloadControl) SetControlRouteGroup(group string) error {
	s.Group = group
	return nil
}

func generatePayload(ctx context.Context, value interface{}) (*Payload, error) {
	carrier := opentracing.TextMapCarrier(make(map[string]string))
	span := xtrace.SpanFromContext(ctx)
	if span != nil {
		_ = xtrace.GlobalTracer().Inject(
			span.Context(),
			opentracing.TextMap,
			carrier)
	}

	msg, err := json.Marshal(value)
	if err != nil {
		return nil, err
	}
	head := ctx.Value(xcontext.ContextKeyHead)
	control := new(mqPayloadControl)
	group, ok := xcontext.GetControlRouteGroup(ctx)
	if ok {
		control.Group = group
	}
	return &Payload{
		Carrier: carrier,
		Value:   string(msg),
		Head:    head,
		Control: control,
	}, nil
}

func parsePayload(payload *Payload, opName string, value interface{}) (context.Context, error) {
	tracer := opentracing.GlobalTracer()
	spanCtx, err := tracer.Extract(opentracing.TextMap, opentracing.TextMapCarrier(payload.Carrier))
	var span opentracing.Span
	if err == nil {
		span = tracer.StartSpan(opName, ext.RPCServerOption(spanCtx))
	} else {
		span = tracer.StartSpan(opName)
	}
	ctx := context.Background()
	ctx = opentracing.ContextWithSpan(ctx, span)
	ctx = context.WithValue(ctx, xcontext.ContextKeyHead, payload.Head)
	ctx = context.WithValue(ctx, xcontext.ContextKeyControl, payload.Control)

	err = json.Unmarshal([]byte(payload.Value), value)
	if err != nil {
		return ctx, err
	}

	return ctx, nil
}
