package pulsar

import (
	"context"
	"crypto/rand"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"math/big"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"gitee.com/xfrm/middleware/xtime"

	"gitee.com/xfrm/middleware/xlog"

	"gitee.com/xfrm/middleware/xmq/pub"

	"gitee.com/xfrm/middleware/xtrace"

	. "gitee.com/xfrm/middleware/internal/middlewareconf"
	"github.com/apache/pulsar-client-go/pulsar"
)

var (
	ErrBrokerNotNull           = errors.New("broker cannot be null")
	ErrConfigNotNull           = errors.New("config cannot be nil")
	ErrTopicInfoNotNull        = errors.New("topic info cannot be null")
	ErrSubscriptionNameNotNull = errors.New("subscription name cannot be null")
	ErrSubNameNotChinese       = errors.New("subscription name cannot be chinese")
)

type Consumer struct {
	pulsar.Consumer
	client           pulsar.Client
	config           *ConsumerConfig
	subscriptionName string
	url              string
	topic            string
}

func NewConsumer(ctx context.Context, topic, subscriptionName string) (*Consumer, error) {

	if pub.IsChinese(subscriptionName) {
		return nil, ErrSubNameNotChinese
	}
	middlewareConfig, err := GetMiddlewareConfig()
	if err != nil {
		return nil, err
	}
	config, err := middlewareConfig.GetMQConfig(ctx, topic, MiddlewareTypePulsar)
	if err != nil {
		return nil, err
	}

	topic = pub.WrapTopicFromContext(ctx, topic)
	consumerConfig := &ConsumerConfig{
		ConsumerOptions: &pulsar.ConsumerOptions{
			Topic:            topic,
			SubscriptionName: subscriptionName,
			Type:             transSubsciptionType(config.SubscriptionType),
			Name:             getName(),
		},
		Broker:           strings.Join(config.MQAddr, ApolloMQBrokersSep),
		OperationTimeout: config.TimeOut,
	}

	client, err := pulsar.NewClient(pulsar.ClientOptions{
		URL:              consumerConfig.Broker,
		OperationTimeout: consumerConfig.OperationTimeout,
	})
	if err != nil {
		return nil, fmt.Errorf("Could not instantiate Pulsar client: %v", err)
	}

	consumer, err := client.Subscribe(*consumerConfig.ConsumerOptions)
	if err != nil {
		client.Close()
		return nil, err
	}

	return &Consumer{
		Consumer:         consumer,
		client:           client,
		subscriptionName: subscriptionName,
		url:              strings.Join(config.MQAddr, ApolloMQBrokersSep),
		topic:            topic,
		config:           consumerConfig,
	}, nil
}

func NewConsumerWithConfig(ctx context.Context, config *ConsumerConfig) (*Consumer, error) {
	fun := "NewConsumerWithConfig -->"

	if err := validateConsumerConfig(config); err != nil {
		return nil, err
	}

	client, err := pulsar.NewClient(pulsar.ClientOptions{
		URL:               config.Broker,
		ConnectionTimeout: config.ConnectionTimeout,
		OperationTimeout:  config.OperationTimeout,
	})
	if err != nil {
		return nil, fmt.Errorf("%s new client, err: %v", fun, err)
	}

	config.Topic = pub.WrapTopicFromContext(ctx, config.Topic)
	consumer, err := client.Subscribe(*config.ConsumerOptions)
	if err != nil {
		client.Close()
		return nil, fmt.Errorf("%s subscribe, err: %v", fun, err)
	}
	return &Consumer{
		Consumer:         consumer,
		client:           client,
		subscriptionName: config.SubscriptionName,
		url:              config.Broker,
		topic:            config.Topic,
		config:           config,
	}, err
}

func (c *Consumer) setSpanTags(span xtrace.Span) {
	span.SetTag(xtrace.TagComponent, pub.TraceComponent)
	span.SetTag(xtrace.TagPalfishMessageBusType, pub.TraceMessageBusTypePulsar)
	span.SetTag(xtrace.TagSpanKind, xtrace.SpanKindConsumer)
	span.SetTag(xtrace.TagMessageBusDestination, c.topic)
	span.SetTag(xtrace.TagMessagingSystem, xtrace.MessagingSystemPulsar)
	span.SetTag(xtrace.TagMessagingDestinationKind, xtrace.MessagingDestinationKindTopic)
	span.SetTag(xtrace.TagMessagingDestination, c.topic)
	span.SetTag(xtrace.TagMessagingPulsarConsumerGroup, c.subscriptionName)
	span.SetTag(xtrace.TagNetPeerIP, c.url)
}

func (c *Consumer) ReadMsg(ctx context.Context, v interface{}, ov interface{}) error {
	span := xtrace.SpanFromContext(ctx)
	if span != nil {
		c.setSpanTags(span)
	}

	st := xtime.NewTimeStat()
	msg, err := c.Receive(ctx)
	pub.StatReqDuration(ctx, c.topic, "Consumer.ReadMsg", pub.TraceMessageBusTypePulsar, st.Millisecond())

	if err != nil {
		return err
	}
	defer c.Ack(msg)

	err = json.Unmarshal(msg.Payload(), v)
	if err != nil {
		return err
	}

	err = json.Unmarshal(msg.Payload(), ov)

	return err
}

func (c *Consumer) FetchMsg(ctx context.Context, v interface{}, ov interface{}) (*pulsarHandler, error) {
	span := xtrace.SpanFromContext(ctx)
	if span != nil {
		c.setSpanTags(span)
	}

	st := xtime.NewTimeStat()
	msg, err := c.Receive(ctx)
	pub.StatReqDuration(ctx, c.topic, "Consumer.FetchMsg", pub.TraceMessageBusTypePulsar, st.Millisecond())
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(msg.Payload(), v)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(msg.Payload(), ov)
	if err != nil {
		return nil, err
	}
	ackHandler := newPulsarHandler(msg, c.Consumer)
	return ackHandler, nil
}

func (c *Consumer) Close() error {
	c.Consumer.Close()
	c.client.Close()

	return nil
}

func (c *Consumer) SetOffsetAt(ctx context.Context, t time.Time) error {
	fun := "SetOffsetAt -->"
	if c.config.BrokerAdmin == "" {
		return fmt.Errorf("%s invalid BrokerAdmin", fun)
	}
	topics := strings.Split(c.topic, "://")
	if len(topics) != 2 {
		return fmt.Errorf("%s invalid Topic", fun)
	}
	res, err := http.Post(fmt.Sprintf("%s/admin/v2/persistent/%s/subscription/%s/resetcursor/%d",
		c.config.BrokerAdmin,
		topics[1],
		c.Subscription(),
		t.UnixNano()/1e6), "application/json", nil)
	if err != nil {
		return err
	}
	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)
	if string(body) != "" {
		return fmt.Errorf("%s error:%s", fun, string(body))
	}
	return nil
}

func (c *Consumer) SetOffset(ctx context.Context, offset interface{}) error {
	switch offset.(type) {
	case pulsar.MessageID:
		return c.Consumer.Seek(offset.(pulsar.MessageID))
	}
	return nil
}

func transSubsciptionType(stype int) pulsar.SubscriptionType {
	switch stype {
	case 0:
		return pulsar.Exclusive
	case 1:
		return pulsar.Shared
	case 2:
		return pulsar.Failover
	case 3:
		return pulsar.KeyShared
	default:
		xlog.Errorf(context.TODO(), "invalid subsciption type:%d in conf", stype)
		return pulsar.Failover
	}
}

func getName() string {
	progname := filepath.Base(os.Args[0])
	hostname, _ := os.Hostname()
	num, _ := rand.Int(rand.Reader, big.NewInt(10000))
	return fmt.Sprintf("%s@%s (gitee.com/xfrm/middleware/xmq) %d", progname, hostname, num)
}

func validateConsumerConfig(config *ConsumerConfig) error {
	if config == nil {
		return ErrConfigNotNull
	}
	if config.Broker == "" {
		return ErrBrokerNotNull
	}
	if config.Topic == "" && len(config.Topics) == 0 {
		return ErrTopicInfoNotNull
	}
	if config.SubscriptionName == "" {
		return ErrSubscriptionNameNotNull
	}
	if pub.IsChinese(config.SubscriptionName) {
		return ErrSubNameNotChinese
	}
	return nil
}
