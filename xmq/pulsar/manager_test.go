package pulsar

import (
	"context"
	"reflect"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitee.com/xfrm/middleware/xconfig"

	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
)

type ManagerSuite struct {
	suite.Suite

	manager *Manager
}

func (p *ManagerSuite) SetupSuite() {
	ctx := context.Background()
	center, err := xconfig.NewConfigCenter(ctx, xconfig.ConfigTypeApollo, "", "test.test", []string{mqNamespace})
	p.Require().NoError(err)

	manager, err := NewManager(ctx, center)
	p.Require().NoError(err)

	p.manager = manager
}

func (p *ManagerSuite) Test_getProducer() {
	ctx := context.Background()
	conf := &instanceConf{
		group: "",
		role:  0,
		topic: "persistent://base/test/test",
		lane:  "",
	}
	producer, err := p.manager.getProducer(ctx, conf)
	p.NoError(err)
	p.Equal("pulsar://pulsar-test.pri.ibanyu.com:16650", producer.url)
}

// 测试变更配置后重新生成新的实例
func Test_applyChange(t *testing.T) {
	ctx := context.Background()
	center, err := xconfig.NewConfigCenter(ctx, xconfig.ConfigTypeApollo, "", "test.test", []string{mqNamespace})
	require.NoError(t, err)
	manager, err := NewManager(ctx, center)
	require.NoError(t, err)

	conf := &instanceConf{
		role:  producerRoleType,
		topic: "persistent://base/test/test",
	}
	// 先创建一个实例，然后加入instances
	instance, err := manager.newInstance(ctx, conf)
	require.NoError(t, err)

	manager.instances.Store(manager.buildKey(conf), instance)
	assert.Equal(t, 1, syncMapSize(manager.instances))

	change := &xconfig.Change{
		OldValue:   "pulsar://10.111.130.10:16650",
		NewValue:   "pulsar://10.111.130.37:16650",
		ChangeType: xconfig.MODIFY,
	}
	manager.applyChange(ctx, "persistent://base/test/test.producer.default.broker", change)
	assert.Equal(t, 1, syncMapSize(manager.instances))

	// 两个instance不相等即可
	producer, err := manager.getProducer(ctx, conf)
	assert.NoError(t, err)
	ip := instance.(*Producer)
	assert.NotEqual(t, ip, producer)
}

func TestManagerSuite(t *testing.T) {
	suite.Run(t, new(ManagerSuite))
}

func Test_instanceConfFromString(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name     string
		args     args
		wantConf *instanceConf
		wantErr  bool
	}{
		{
			name: "实例配置测试",
			args: args{
				s: "sub@producer@persistent://base/bmq/test@t1",
			},
			wantConf: &instanceConf{
				group: "sub",
				role:  0,
				topic: "persistent://base/bmq/test",
				lane:  "t1",
			},
			wantErr: false,
		},
		{
			name: "实例配置测试，无group",
			args: args{
				s: "@producer@persistent://base/bmq/test@t1",
			},
			wantConf: &instanceConf{
				group: "",
				role:  0,
				topic: "persistent://base/bmq/test",
				lane:  "t1",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotConf, err := instanceConfFromString(tt.args.s)
			if (err != nil) != tt.wantErr {
				t.Errorf("instanceConfFromString() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotConf, tt.wantConf) {
				t.Errorf("instanceConfFromString() gotConf = %v, want %v", gotConf, tt.wantConf)
			}
		})
	}
}

func Test_mqRoleTypeFromString(t *testing.T) {
	type args struct {
		it string
	}
	tests := []struct {
		name    string
		args    args
		wantT   roleType
		wantErr bool
	}{
		{
			name:    "测试，从字符串转换到role",
			args:    args{it: "consumer"},
			wantT:   consumerRoleType,
			wantErr: false,
		},
		{
			name:    "测试，producer",
			args:    args{it: "producer"},
			wantT:   producerRoleType,
			wantErr: false,
		},
		{
			name:    "测试，err",
			args:    args{it: "producer123"},
			wantT:   producerRoleType,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotT, err := mqRoleTypeFromString(tt.args.it)
			if (err != nil) != tt.wantErr {
				t.Errorf("mqRoleTypeFromString() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotT != tt.wantT {
				t.Errorf("mqRoleTypeFromString() gotT = %v, want %v", gotT, tt.wantT)
			}
		})
	}
}

func syncMapSize(syncMap sync.Map) (ret int) {
	syncMap.Range(func(k, v interface{}) bool {
		ret += 1
		return true
	})
	return
}
