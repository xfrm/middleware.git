package pulsar

import (
	"context"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitee.com/xfrm/middleware/xconfig"
)

func Test_parseTopicConfigKey(t *testing.T) {
	type args struct {
		key string
	}
	tests := []struct {
		name    string
		args    args
		want    *apolloMQKey
		wantErr bool
	}{
		{
			name: "测试转换topic config key",
			args: args{key: "persistent://base/bmq/test.producer.default.broker"},
			want: &apolloMQKey{
				topic: "persistent://base/bmq/test",
				role:  0,
				lane:  "",
				item:  "broker",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := parseTopicConfigKey(tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseTopicConfigKey() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseTopicConfigKey() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_buildTopicConfigKey(t *testing.T) {
	type args struct {
		ctx   context.Context
		topic string
		item  string
		role  roleType
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "测试生成topic config key",
			args: args{
				ctx:   context.Background(),
				topic: "persistent://base/bmq/test",
				item:  "broker",
				role:  1,
			},
			want: "persistent://base/bmq/test.consumer.default.broker",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := buildTopicConfigKey(tt.args.ctx, tt.args.topic, tt.args.item, tt.args.role); got != tt.want {
				t.Errorf("buildTopicConfigKey() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getPulsarProducerOption(t *testing.T) {
	ctx := context.Background()
	center, err := xconfig.NewConfigCenter(ctx, xconfig.ConfigTypeApollo, "base.bmq", []string{mqNamespace})
	assert.NoError(t, err)
	producerConfig, err := getProducerConfig(ctx, "persistent://base/bmq/test", center)
	assert.NoError(t, err)
	assert.Equal(t, "pulsar://pulsar-test.pri.ibanyu.com:16650", producerConfig.Broker)
	assert.Equal(t, "persistent://base/bmq/test", producerConfig.Topic)
}

func Test_getConsumerConfig(t *testing.T) {
	ctx := context.Background()
	center, err := xconfig.NewConfigCenter(ctx, xconfig.ConfigTypeApollo, "", "base.bmq", []string{mqNamespace})
	assert.NoError(t, err)
	consumerConfig, err := getConsumerConfig(ctx, "persistent://base/bmq/test", "", center)
	assert.NoError(t, err)
	assert.Equal(t, "pulsar://pulsar-test.pri.ibanyu.com:16650", consumerConfig.Broker)
	assert.Equal(t, "persistent://base/bmq/test", consumerConfig.Topic)
}

func Test_getConsumerConfigMasterLane(t *testing.T) {
	ctx := context.Background()
	center, err := xconfig.NewConfigCenter(ctx, xconfig.ConfigTypeApollo, "", "base.bmq", []string{mqNamespace})
	assert.NoError(t, err)
	consumerConfig, err := getConsumerConfig(ctx, "persistent://base/bmq/test", "", center)
	assert.NoError(t, err)
	assert.Equal(t, "pulsar://pulsar-test.pri.ibanyu.com:16650", consumerConfig.Broker)
	assert.Equal(t, "persistent://base/bmq/test", consumerConfig.Topic)
}
