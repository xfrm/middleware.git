package pulsar

import (
	"context"

	"github.com/apache/pulsar-client-go/pulsar"
)

type pulsarHandler struct {
	msg      pulsar.Message
	consumer pulsar.Consumer
}

func (p *pulsarHandler) Ack(ctx context.Context) error {
	p.consumer.Ack(p.msg)
	return nil
}

func newPulsarHandler(msg pulsar.Message, consumer pulsar.Consumer) *pulsarHandler {
	return &pulsarHandler{
		msg:      msg,
		consumer: consumer,
	}
}
