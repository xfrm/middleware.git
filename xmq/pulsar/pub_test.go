package pulsar

import (
	"context"
	"fmt"
	"time"
)

type msg struct {
	Key   int    `json:"key"`
	Value string `json:"value"`
}

func (p *ManagerSuite) TestWriteMsg() {
	ctx := context.Background()
	messageID, err := p.manager.WriteMsg(ctx, "persistent://base/test/test", "1", &msg{
		Key:   1,
		Value: "test",
	})
	p.NoError(err)
	fmt.Printf("messageID: %v", messageID)
}

func (p *ManagerSuite) TestReadMsg() {
	ctx := context.Background()
	msg1 := &msg{}
	ctx, _ = context.WithTimeout(ctx, 3*time.Second)
	_, err := p.manager.WriteMsg(ctx, "persistent://base/test/test", "1", &msg{
		Key:   1,
		Value: "test",
	})
	ctx, err = p.manager.ReadMsg(ctx, "persistent://base/test/test", "test", msg1)
	p.NoError(err)
}
