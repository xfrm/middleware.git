package pulsar

import (
	"context"
	"testing"
	"time"

	"github.com/apache/pulsar-client-go/pulsar"

	"github.com/stretchr/testify/assert"
)

func TestProducerConnectError(t *testing.T) {

}

func TestNewProducer(t *testing.T) {
	producer, err := NewProducer(context.Background(), "persistent://base/test/test")
	assert.NoError(t, err)
	assert.NotNil(t, producer)
	assert.NoError(t, producer.Close())
}

func TestNewProducerWithConfig(t *testing.T) {
	producer, err := NewProducerWithConfig(context.Background(), &ProducerConfig{
		ProducerOptions: &pulsar.ProducerOptions{
			Topic: "persistent://base/test/test",
		},
		Broker:           "pulsar://pulsar-test.pri.ibanyu.com:16650",
		OperationTimeout: 10 * time.Second,
	})
	assert.NoError(t, err)
	assert.NotNil(t, producer)
	assert.NoError(t, producer.Close())
}
