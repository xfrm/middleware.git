package kafka

import (
	"context"
	"fmt"
	"strings"

	"gitee.com/xfrm/middleware/xlog"
)

const (
	// REBALANCE_IN_PROGRESS
	ErrorMsgRebalanceInProgress = "Rebalance In Progress"
)

type errorLogger struct {
}

func getErrorLogger() *errorLogger {
	return &errorLogger{}
}

func (m *errorLogger) Printf(format string, items ...interface{}) {
	errMsg := fmt.Sprintf(format, items...)
	if strings.Contains(errMsg, ErrorMsgRebalanceInProgress) {
		xlog.Warnf(context.Background(), errMsg)
		return
	}
	xlog.Errorf(context.Background(), format, items...)
}
