package xmq

import (
	"context"
	"fmt"
	"strings"
	"sync"
	"testing"

	"gitee.com/xfrm/middleware/xmq/pub"

	"gitee.com/xfrm/middleware/xmq/kafka"

	"gitee.com/xfrm/middleware/internal/middlewareconf"
	"gitee.com/xfrm/middleware/xconfig"
	"gitee.com/xfrm/middleware/xcontext"

	"github.com/stretchr/testify/assert"
)

const defaultTestTopic = "palfish.test.test"

func TestInstanceConfFromString(t *testing.T) {
	cases := []struct {
		s            string
		expectErr    bool
		expectedConf *instanceConf
	}{
		{
			fmt.Sprintf("default@0@%s@g1@0", defaultTestTopic),
			false,
			&instanceConf{
				group:     "default",
				role:      RoleTypeKafkaReader,
				topic:     defaultTestTopic,
				groupId:   "g1",
				partition: 0,
			},
		},
		{
			"default@1@base.copywriting-transformation-counter.count@@0",
			false,
			&instanceConf{
				group:     "default",
				role:      RoleTypeKafkaWriter,
				topic:     "base.copywriting-transformation-counter.count",
				groupId:   "",
				partition: 0,
			},
		},
		{
			"test@1@topic@g1@1",
			false,
			&instanceConf{
				group:     "test",
				role:      RoleTypeKafkaWriter,
				topic:     "topic",
				groupId:   "g1",
				partition: 1,
			},
		},
		{
			"test@test@test@test@test",
			true,
			nil,
		},
		{
			"test@3@topic@test-test@0",
			false,
			&instanceConf{
				group:     "test",
				role:      RoleTypePulsarConsumer,
				topic:     "topic",
				groupId:   "test-test",
				partition: 0,
			},
		},
		{
			"test@1@topic@test@test",
			true,
			nil,
		},
	}

	for _, c := range cases {
		conf, err := instanceConfFromString(c.s)
		assert.Equal(t, c.expectErr, err != nil)
		assert.Equal(t, c.expectedConf, conf)
	}
}

func getSyncMapSizeUnSafe(m sync.Map) (ret int) {
	m.Range(func(k, v interface{}) bool {
		ret += 1
		return true
	})
	return
}

func TestInstanceManager_applyChangeEvent_apolloConfig(t *testing.T) {
	ctx := context.TODO()

	t.Run("ignore add event", func(t *testing.T) {
		m := NewInstanceManager()

		conf := &instanceConf{
			group:     "default",
			role:      0,
			topic:     defaultTestTopic,
			groupId:   "g1",
			partition: 0,
		}

		ce := &xconfig.ChangeEvent{
			Source:    xconfig.Apollo,
			Namespace: xconfig.DefaultMQNamespace,
			Changes: map[string]*xconfig.Change{
				m.buildKey(conf): {
					NewValue:   "localhost:9092",
					ChangeType: xconfig.ADD,
				},
			},
		}

		assert.Equal(t, 0, getSyncMapSizeUnSafe(m.instances))
		m.applyChangeEvent(ctx, ce)
	})

	t.Run("modify/delete instance", func(t *testing.T) {
		m := NewInstanceManager()

		conf := &instanceConf{
			group:     "default",
			role:      0,
			topic:     defaultTestTopic,
			groupId:   "g1",
			partition: 0,
		}

		in, err := m.newInstance(ctx, conf)
		assert.Equal(t, nil, err)
		m.add(conf, in)

		ce := &xconfig.ChangeEvent{
			Source:    xconfig.Apollo,
			Namespace: xconfig.DefaultMQNamespace,
			Changes: map[string]*xconfig.Change{
				strings.Join([]string{
					defaultTestTopic,
					xcontext.GetControlRouteGroupWithMasterName(ctx, pub.DefaultRouteGroup),
					fmt.Sprint(middlewareconf.MiddlewareTypeKafka),
					"brokers",
				}, "."): {
					NewValue:   "localhost:9092",
					ChangeType: xconfig.MODIFY,
				},
			},
		}

		assert.Equal(t, 1, getSyncMapSizeUnSafe(m.instances))
		m.applyChangeEvent(ctx, ce)
		assert.Equal(t, 1, getSyncMapSizeUnSafe(m.instances))
		modifiedIn := m.get(ctx, conf)
		assert.NotEqual(t, modifiedIn, in)
		_, ok := modifiedIn.(*kafka.KafkaReader)
		assert.True(t, ok)
	})

	t.Run("modify/delete default config", func(t *testing.T) {
		m := NewInstanceManager()

		conf := &instanceConf{
			group:     "unknown",
			role:      0,
			topic:     defaultTestTopic,
			groupId:   "g1",
			partition: 0,
		}

		in, err := m.newInstance(ctx, conf)
		assert.Equal(t, nil, err)
		m.add(conf, in)

		ce := &xconfig.ChangeEvent{
			Source:    xconfig.Apollo,
			Namespace: xconfig.DefaultMQNamespace,
			Changes: map[string]*xconfig.Change{
				strings.Join([]string{
					defaultTestTopic,
					xcontext.GetControlRouteGroupWithMasterName(ctx, pub.DefaultRouteGroup),
					fmt.Sprint(middlewareconf.MiddlewareTypeKafka),
					"brokers",
				}, "."): {
					ChangeType: xconfig.MODIFY,
				},
			},
		}

		assert.Equal(t, 1, getSyncMapSizeUnSafe(m.instances))
		m.applyChangeEvent(ctx, ce)
		assert.Equal(t, 1, getSyncMapSizeUnSafe(m.instances))
		modifiedIn := m.get(ctx, conf)
		assert.NotEqual(t, modifiedIn, in)
		_, ok := modifiedIn.(*kafka.KafkaReader)
		assert.True(t, ok)
	})
}

func TestInstanceManager_getDelayClient(t *testing.T) {
	ctx := context.TODO()

	t.Run("test get delay client", func(t *testing.T) {
		m := NewInstanceManager()
		conf := &instanceConf{
			group:     "unknown",
			role:      RoleTypeDelayClient,
			topic:     defaultTestTopic,
			groupId:   "g1",
			partition: 0,
		}
		client := m.getDelayClient(ctx, conf)

		assert.NotNil(t, client)
	})

}

func TestInstanceManager_getDelayClientGroup(t *testing.T) {
	ctx := context.TODO()
	ctx = context.WithValue(ctx, xcontext.ContextKeyControl, simpleContextControlRouter{"t1"})
	t.Run("test get delay client", func(t *testing.T) {
		m := NewInstanceManager()
		conf := &instanceConf{
			group:     "unknown",
			role:      RoleTypeDelayClient,
			topic:     defaultTestTopic,
			groupId:   "g1",
			partition: 0,
		}
		client := m.getDelayClient(ctx, conf)

		assert.NotNil(t, client)
	})
}

type simpleContextControlRouter struct {
	group string
}

func (s simpleContextControlRouter) GetControlRouteGroup() (string, bool) {
	return s.group, true
}

func (s simpleContextControlRouter) SetControlRouteGroup(group string) error {
	s.group = group
	return nil
}
