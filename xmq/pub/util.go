package pub

import (
	"context"
	"fmt"
	"unicode"

	"gitee.com/xfrm/middleware/xcontext"
)

const (
	DefaultRouteGroup = "default"

	TraceComponent            = "xmq"
	TraceMessageBusTypeKafka  = "kafka"
	TraceMessageBusTypeDelay  = "delay"
	TraceMessageBusTypePulsar = "pulsar"
)

func WrapTopicFromContext(ctx context.Context, topic string) string {
	group, ok := xcontext.GetControlRouteGroup(ctx)
	if !ok || group == "" {
		return topic
	}
	return fmt.Sprintf("%s_%s", topic, group)
}

func IsChinese(str string) bool {
	var count int
	for _, v := range str {
		if unicode.Is(unicode.Han, v) {
			count++
			break
		}
	}
	return count > 0
}
