package pub

import "context"

type Handler interface {
	CommitMsg(ctx context.Context) error
}

type AckHandler interface {
	Ack(ctx context.Context) error
}
