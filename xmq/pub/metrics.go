package pub

import (
	"context"
	"gitee.com/xfrm/middleware/xstat/xmetric"
	"gitee.com/xfrm/middleware/xstat/xmetric/xprometheus"
	"gitee.com/xfrm/middleware/xtrace"
)

const (
	namespace = "palfish"
	subsystem = "mq_requests"
)

var (
	buckets = []float64{5, 10, 25, 50, 100, 250, 500, 1000, 2500, 5000, 10000}

	_metricRequestDuration = xprometheus.NewHistogram(&xprometheus.HistogramVecOpts{
		Namespace:  namespace,
		Subsystem:  subsystem,
		Name:       "duration_ms",
		Help:       "mq requests duration(ms)",
		LabelNames: []string{"topic", "command", xprometheus.LabelMessageBusType, xprometheus.LabelCallerService, xprometheus.LabelCallerEndpoint},
		Buckets:    buckets,
	})

	_metricMessageNumber = xprometheus.NewCounter(&xprometheus.CounterVecOpts{
		Namespace:  namespace,
		Subsystem:  subsystem,
		Name:       "message_number",
		Help:       "mq message number",
		LabelNames: []string{"topic", "command", xprometheus.LabelMessageBusType, xprometheus.LabelCallerService},
	})

	_metricFetchNumber = xprometheus.NewCounter(&xprometheus.CounterVecOpts{
		Namespace:  namespace,
		Subsystem:  subsystem,
		Name:       "fetch_number",
		Help:       "mq fetch number",
		LabelNames: []string{"topic", "command", xprometheus.LabelMessageBusType, xprometheus.LabelCallerService},
	})

	_metricTimeoutsNumber = xprometheus.NewCounter(&xprometheus.CounterVecOpts{
		Namespace:  namespace,
		Subsystem:  subsystem,
		Name:       "timeout_number",
		Help:       "mq timeout number",
		LabelNames: []string{"topic", "command", xprometheus.LabelMessageBusType, xprometheus.LabelCallerService},
	})

	_metricBytesNumber = xprometheus.NewCounter(&xprometheus.CounterVecOpts{
		Namespace:  namespace,
		Subsystem:  subsystem,
		Name:       "bytes_number",
		Help:       "mq total bytes",
		LabelNames: []string{"topic", "command", xprometheus.LabelMessageBusType, xprometheus.LabelCallerService},
	})

	_metricWriteNumber = xprometheus.NewCounter(&xprometheus.CounterVecOpts{
		Namespace:  namespace,
		Subsystem:  subsystem,
		Name:       "write_number",
		Help:       "mq write number",
		LabelNames: []string{"topic", "command", xprometheus.LabelMessageBusType, xprometheus.LabelCallerService},
	})

	_metricErrorNumber = xprometheus.NewCounter(&xprometheus.CounterVecOpts{
		Namespace:  namespace,
		Subsystem:  subsystem,
		Name:       "error_number",
		Help:       "mq error number",
		LabelNames: []string{"topic", "command", xprometheus.LabelMessageBusType, xprometheus.LabelCallerService},
	})
)

func StatReqDuration(ctx context.Context, topic, command, messageBusType string, durationMS int64) {
	_metricRequestDuration.With("topic", topic, "command", command, xprometheus.LabelMessageBusType, messageBusType,
		xprometheus.LabelCallerService, xtrace.ServiceName(), xprometheus.LabelCallerEndpoint, xtrace.ExtractSpanOperationName(ctx)).Observe(float64(durationMS))
}

func GetMessageNumberMetric() xmetric.Counter {
	return _metricMessageNumber
}

func GetFetchNumberMetric() xmetric.Counter {
	return _metricFetchNumber
}

func GetTimeoutsNumberMetric() xmetric.Counter {
	return _metricTimeoutsNumber
}

func GetBytesNumberMetric() xmetric.Counter {
	return _metricBytesNumber
}

func GetWriteNumberMetric() xmetric.Counter {
	return _metricWriteNumber
}

func GetErrorNumberMetric() xmetric.Counter {
	return _metricErrorNumber
}
