package pub

import "testing"

func TestIsChinese(t *testing.T) {
	type args struct {
		str string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "测试中文字符",
			args: args{str: "is中文is"},
			want: true,
		},
		{
			name: "测试中文字符2",
			args: args{str: "isis"},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsChinese(tt.args.str); got != tt.want {
				t.Errorf("IsChinese() = %v, want %v", got, tt.want)
			}
		})
	}
}
