package xrsa

import (
	"context"
	"crypto"
	"io/ioutil"
	"reflect"
	"testing"
)

var ctx = context.Background()
var thirdpub *XPublicKey
var xpub *XPublicKey
var xpri *XPrivateKey

func init() {
	var thirdPubFile = "/Users/tanghc/Documents/rsa/demo1/third_rsa_public_key.pem"
	var pubFile = "/Users/tanghc/Documents/rsa/demo1/my_rsa_public_key.pem"
	var priFile = "/Users/tanghc/Documents/rsa/demo1/my_private_key_pkcs1.pem"
	//var pubFile = "/Users/tanghc/Documents/rsa/rsa_public.pem"
	//var priFile = "/Users/tanghc/Documents/rsa/rsa_private.pem"

	thirdpubpem, _ := ioutil.ReadFile(thirdPubFile)
	thirdpub, _ = NewPemPublicKey(thirdpubpem)
	pubpem, _ := ioutil.ReadFile(pubFile)
	xpub, _ = NewPemPublicKey(pubpem)
	pripem, _ := ioutil.ReadFile(priFile)
	xpri, _ = NewPemPrivateKey(pripem, PKCS1)
}
func TestXPublicKey_EncryptEncode(t *testing.T) {
	type args struct {
		ctx       context.Context
		plaintext []byte
		encoder   RsaCoder
	}
	tests := []struct {
		name    string
		fields  *XPublicKey
		args    args
		want    []byte
		want1   string
		wantErr bool
	}{
		{
			name:   "test1",
			fields: xpub,
			args: args{
				ctx:       ctx,
				plaintext: []byte("abcd"),
				encoder:   &Base64RsaEncoder{},
			},
			want:    nil,
			want1:   "",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := tt.fields
			got, got1, err := m.EncryptEncode(tt.args.ctx, tt.args.plaintext, tt.args.encoder)
			if (err != nil) != tt.wantErr {
				t.Errorf("EncryptEncode() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("EncryptEncode() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("EncryptEncode() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestXPublicKey_DecryptEncode(t *testing.T) {
	var ci = "YYUZeXZfkkV8AVOZNqYvOjgLCPB+THsFm67yqCfJb5G5MAXlGzcpvBR2Dq2e+7pazZTMZtkWCLZ0wflxXAEnuYywB5b9HiUjW4ccjgVtx93AKIVjf5rUMTM7CDIHVzOaaVLZu6eTIVirVqDDTiR0+V0lfKHfQ6W8grC28wW3+qN9wdHG35CMVQbHJi99n7eSR+qnjrN/kF1gR4Dl6ShnrxJF93wtX+rG2bQZ3Cmj+vEj6Pzod8RGkqbN/EiSGw2XE6FLZZ/cyqHwnx0ihg2ToUXUTT0Nbqwp1f2JQTU5IQUS7ygJnWeTriT2H9BvQJQZWayEOPwQDewvDGO4zzU84w=="
	d, err := xpri.DecodeDecrypt(ctx, ci, &Base64RsaEncoder{})
	t.Logf("de: %s, err: %v", string(d), err)
}
func TestXPublicKey_DecryptEncode1(t *testing.T) {
	var ci = "4dde3b0773ba91d1af7623addd45519d0976ff8ea91c56d2cf44d59a58369253d5d999ca694fa5677929b833dfd4729df15e0c17c36f1e0024180e6d13e051bf7ac2a69cd5d87e6595995331359203ef36628841b58ab3bb86d6e04a654528c9dfc53c1ba3061542359196ffff435fd657d62d42c1a277950140bc24d0089195"
	d, err := xpri.DecodeDecrypt(ctx, ci, &HexRsaEncoder{})
	t.Logf("de: %s, err: %v", string(d), err)
}
func TestXPublicKey_Sign(t *testing.T) {
	var src = "ssssssssssss"
	s, err := xpri.SignEncode([]byte(src), crypto.SHA256, &HexRsaEncoder{})
	t.Logf("s: %s, err: %v", string(s), err)
}
func TestXPublicKey_Verify(t *testing.T) {
	//var src = "ssssssssssss"
	//var sign = "a88fb561b3010435a21e4a88a3b8e574e8552fdc4444415246c08ab2aa506d9fae46e5027966b107005990aeec06118ea2ea458338533871de3965b5178511ceea1feb17516814247d3332fd24f00689509c980acf1a595f7ff374691d9dda4bf95104bc99e5c70752371551ed0c490f9903a891666664b98427f0418b2ad877"
	var src = "result={\"orderStatus\":\"FAIL\",\"respCode\":\"200004\",\"respMsg\":\"订单不存在\"}&success=true&key=86a8eec5ae958e9948b7450439cc57e2"
	var sign = "11e6c50d60ebdeacfe5d55dad1a0c11925b0850602d42346d1824e1e97d1581f4b21b258962a2289842a8c75d6af7c687a30d25af64ad467a2f050311e90149aee5fd60868b85dfeb3e2ac8d52509fc4a75c57c33ca8f511e0afdd92d5f025a2e70bc86d0f678019a3a040862defcb3e54c327a452ad623a9d88eec71b996cbb"
	//err:=xpub.DecodeVerify(ctx, []byte(src), sign, crypto.SHA256, &HexRsaEncoder{})
	err := thirdpub.DecodeVerify(ctx, []byte(src), sign, crypto.SHA256, &HexRsaEncoder{})
	t.Logf("verify: %v", err)
}
