package trie

// runeTrie is a rune-wise trie implementation.
type SearchTrie struct {
	prefix Trie
	full   Trie
}

func (m *SearchTrie) Search(word string) []string {
	return m.full.SearchByPrefix(word)
}

func (m *SearchTrie) Insert(word string) bool {
	ok := m.prefix.Insert(word)
	var rs = []rune(word)
	for i, _ := range rs {
		m.full.Insert(string(rs[i:]))
	}
	return ok
}

func (m *SearchTrie) Contains(word string) bool {
	return m.prefix.Contains(word)
}

func (m *SearchTrie) SearchByPrefix(prefix string) []string {
	return m.prefix.SearchByPrefix(prefix)
}

func (m *SearchTrie) StartsWith(prefix string) bool {
	return m.prefix.StartsWith(prefix)
}

func (m *SearchTrie) Size() int {
	return m.prefix.Size()
}

func NewSearchTrie(prefixTrie Trie, fullTrie Trie) *SearchTrie {
	return &SearchTrie{
		prefix: prefixTrie,
		full:   fullTrie,
	}
}
