package trie

import (
	"fmt"
	"reflect"
	"sort"
	"testing"
)

// to prevent compiler optimizations, all benchmark results are stored
// in the package-level variable

var filledSearchHashSet = (func() map[string]struct{} {
	s := make(map[string]struct{})
	for _, w := range words {
		s[w] = struct{}{}
	}

	return s
})()

var filledRuneSearch = (func() Trie {
	t := NewSearchTrie(NewRuneTrie(), NewRuneTrie())
	for _, w := range words {
		t.Insert(w)
	}

	return t
})()

func TestRuneSearchContains(t *testing.T) {
	trie := NewSearchTrie(NewRuneTrie(), NewRuneTrie())

	trie.Insert("c")
	trie.Insert("apple")
	trie.Insert("banana")

	cases := []struct {
		word   string
		exists bool
	}{
		{"c", true},
		{"cc", false},
		{"ce", false},
		{"banana", true},
		{"app", false},
		{"aple", false},
		{"ban", false},
		{"apple", true},
		{"apple1", false},
		{"aaapple", false},
	}

	for _, c := range cases {
		actual := trie.Contains(c.word)
		if actual != c.exists {
			if c.exists {
				t.Errorf("%s is expected to be found in the trie", c.word)
			} else {
				t.Errorf("%s is not expected to be found in the trie", c.word)
			}
		}
	}
}

func TestRuneSearchStartsWith(t *testing.T) {
	trie := NewSearchTrie(NewRuneTrie(), NewRuneTrie())

	trie.Insert("apple")
	trie.Insert("aplhabet")
	trie.Insert("tree")

	if !trie.StartsWith("a") {
		t.Error("trie StartsWith(a) must return true, but returned false")
	}

	if trie.StartsWith("try") {
		t.Error("trie StartsWith(try) must return false, but returned true")
	}
}

func TestRuneSearchSearchByPrefix(t *testing.T) {
	trie := NewSearchTrie(NewRuneTrie(), NewRuneTrie())

	trie.Insert("c")
	trie.Insert("apple")
	trie.Insert("banana")
	trie.Insert("alphabet")
	trie.Insert("alcohol")

	actual := trie.SearchByPrefix("a")
	sort.Strings(actual)

	expected := []string{"alcohol", "alphabet", "apple"}
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("%v != %v", actual, expected)
	}
}
func TestRuneSearchSearch(t *testing.T) {
	trie := NewSearchTrie(NewRuneTrie(), NewRuneTrie())

	trie.Insert("c")
	trie.Insert("apple")
	trie.Insert("banana")
	trie.Insert("alphabet")
	trie.Insert("alcohol")

	actual := trie.Search("a")
	sort.Strings(actual)
	fmt.Println(actual)
}

func BenchmarkRuneSearchInsert(b *testing.B) {
	var r bool
	for i := 0; i < b.N; i++ {
		t := NewSearchTrie(NewRuneTrie(), NewRuneTrie())
		for _, w := range words {
			r = t.Insert(w)
		}
	}

	benchmarkResult = r
}
