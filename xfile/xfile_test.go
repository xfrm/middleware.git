package xfile

import (
	"encoding/hex"
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

/*
判断给定的filename是否是一个文件
*/
func TestIsFile(t *testing.T) {

	filename := "/"
	ret, err := IsFile(filename)
	if err != nil || ret {
		t.Fatalf("ret:%v err:%v, filename:%v", ret, err, filename)
	}

	filename = "kkkkk"
	ret, err = IsFile(filename)
	if err == nil || ret {
		t.Fatalf("ret:%v err:%v, filename:%v", ret, err, filename)
	}

	filename = "/tmp/xfile.xxx2999"
	ret, err = IsFile(filename)
	if err == nil || ret {
		t.Fatalf("ret:%v err:%v, filename:%v", ret, err, filename)
	}

	filename = "/tmp/xfile.exits"
	file, err := os.Create(filename)
	if err != nil {
		t.Fatalf("create file:%v failed, err:%v", filename, err)
	}

	ret, err = IsFile(filename)
	if err != nil || !ret {
		t.Fatalf("ret:%v err:%v, filename:%v", ret, err, filename)
	}

	file.Close()
	filename = "/tmp/.xfile.exits"
	file, err = os.Create(filename)
	if err != nil {
		t.Fatalf("create file:%v failed, err:%v", filename, err)
	}

	ret, err = IsFile(filename)
	if err != nil || !ret {
		t.Fatalf("ret:%v err:%v, filename:%v", ret, err, filename)
	}

	file.Close()
	dir := "/tmp/xdir.exits/a/b/c/d/e/f/s/d"
	err = os.MkdirAll(dir, 0755)
	if err != nil {
		t.Fatalf("create file:%v failed, err:%v", filename, err)
	}

	filename = "/tmp/xdir.exits/a/b/c/d/e/f/s/d/xfile.xml"
	file, err = os.Create(filename)
	if err != nil {
		t.Fatalf("create file:%v failed, err:%v", filename, err)
	}

	ret, err = IsFile(filename)
	if err != nil || !ret {
		t.Fatalf("ret:%v err:%v, filename:%v", ret, err, filename)
	}

	defer file.Close()
}

func TestMD5Sum(t *testing.T) {
	fileName := "./xfile_test.go"
	md5sum, err := MD5Sum(fileName)
	if err != nil {
		t.Error(err)
	}
	t.Logf("%s md5sum is %s \n", fileName, hex.EncodeToString(md5sum))
}

// 获取一个文件的大小
func TestFileSize(t *testing.T) {
	filename := `./xfile_test.go`
	size, err := FileSize(filename)
	if err != nil {
		t.Fatalf("try to size of file[%s], errored %#v", filename, err)
	}
	fmt.Printf("%s size is %d\n", filename, size)
}

func TestDirExists(t *testing.T) {
	dirName := "./"
	if !DirExists(dirName) {
		t.Fatal("test DirExists failed")
	}
}

func TestDirEmpty(t *testing.T) {
	dirName := "./"
	if DirEmpty(dirName) {
		t.Fatal("test DirEmpty failed")
	}
}

func TestDirSize(t *testing.T) {
	dirname := `./`
	size, err := DirSize(dirname)
	if err != nil {
		t.Fatalf("try to size of dir[%s], errored %#v", dirname, err)
	}
	fmt.Printf("%s size is %d\n", dirname, size)
}

func TestFileCopy(t *testing.T) {
	srcfile := `./xfile_test.go`
	destfile := `./xfile_copyed_test.go`
	err := FileCopy(srcfile, destfile)
	assert.NoError(t, err)

	srcFileMD5, err := MD5Sum(srcfile)
	assert.NoError(t, err)

	destFileMD5, err := MD5Sum(destfile)
	assert.NoError(t, err)
	assert.Equal(t, srcFileMD5, destFileMD5)

	err = os.Remove(destfile)
	assert.NoError(t, err)
}

/*
判断给定的filename是否是一个目录
*/
func TestIsDir(t *testing.T) {
	filename := "/"
	ret, err := IsDir(filename)
	if err != nil || !ret {
		t.Fatalf("ret:%v err:%v, filename:%v", ret, err, filename)
	}

	filename = "kkkkk"
	ret, err = IsDir(filename)
	if err == nil || ret {
		t.Fatalf("ret:%v err:%v, filename:%v", ret, err, filename)
	}

	filename = "/tmp/xdir.xxx2999/dkd/skdkdf"
	ret, err = IsDir(filename)
	if err == nil || ret {
		t.Fatalf("ret:%v err:%v, filename:%v", ret, err, filename)
	}

	filename = "/tmp/xdir.exits"
	err = os.MkdirAll(filename, 0755)
	if err != nil {
		t.Fatalf("create dir:%v failed, err:%v", filename, err)
	}

	ret, err = IsDir(filename)
	if err != nil || !ret {
		t.Fatalf("ret:%v err:%v, filename:%v", ret, err, filename)
	}

	filename = "/tmp/.dir.exits"
	err = os.MkdirAll(filename, 0755)
	if err != nil {
		t.Fatalf("create file:%v failed, err:%v", filename, err)
	}

	ret, err = IsDir(filename)
	if err != nil || !ret {
		t.Fatalf("ret:%v err:%v, filename:%v", ret, err, filename)
	}

	filename = "/tmp/.dir.exits/a/b/c/d/e/f/d/k/e/"
	err = os.MkdirAll(filename, 0755)
	if err != nil {
		t.Fatalf("create file:%v failed, err:%v", filename, err)
	}

	ret, err = IsDir(filename)
	if err != nil || !ret {
		t.Fatalf("ret:%v err:%v, filename:%v", ret, err, filename)
	}
}

func TestRecursiveCallOnFile(t *testing.T) {
	EachFile("/home", func(pathname string, fi os.FileInfo) error {
		if !fi.IsDir() {
			cnt, err := LineCount(pathname)
			if err != nil {
				t.Error(err)
				t.Fail()
				return err
			}
			fmt.Printf("%s\t%d \n", pathname, cnt)
		}
		return nil
	})
}

func TestLoadFile(t *testing.T) {
	_, err := LoadFile("./xfile_test.go")
	if err != nil {
		t.Errorf("LoadFile error")
	}
}

func TestEachLine(t *testing.T) {
	err := EachLine(`./xfile_test.go`, func(line string) bool {
		fmt.Println(line)
		return false
	}, true)
	if err != nil {
		t.Error(err)
		t.Fail()
	}
}
