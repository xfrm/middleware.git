package xcache

import (
	"context"
	"fmt"
	"time"

	"gitee.com/xfrm/middleware/xcache/constants"
	"gitee.com/xfrm/middleware/xlog"
)

type CacheData interface {
	// 序列化接口
	Marshal() ([]byte, error)
	// 反序列化接口
	Unmarshal([]byte) error
	// cache miss load数据接口
	Load(key string) error
}

// 采用json进行序列化的的cache
type Cache struct {
	expire        int
	redisClient   *RedisClient
	prefix        string
	withNamespace bool
	namespace     string
}

// redis 地址列表，key前缀，过期时间
func NewCommonCache(serverName, prefix string, poolSize, expire int) (*Cache, error) {
	fun := "NewCommonCache-->"

	redisClient, err := NewCommonRedis(serverName, poolSize)
	if err != nil {
		xlog.Errorf(context.TODO(), "%s NewCommonRedis, serverNam:%s err: %v", fun, serverName, err)
	}

	return &Cache{
		redisClient: redisClient,
		expire:      expire,
		prefix:      prefix,
	}, err
}

// NewCoreCache new cache use core redis
func NewCoreCache(serverName, prefix string, poolSize, expire int) (*Cache, error) {
	fun := "NewCoreCache-->"

	redisClient, err := NewCoreRedis(serverName, poolSize)
	if err != nil {
		xlog.Errorf(context.TODO(), "%s NewCoreRedis, serverNam:%s err:%s", fun, serverName, err)
	}

	return &Cache{
		redisClient: redisClient,
		expire:      expire,
		prefix:      prefix,
	}, err
}

// NewCacheByNamespace new cache by apollo config middleware/infra.cache
func NewCacheByNamespace(ctx context.Context, namespace, prefix string, expire int) (*Cache, error) {
	fun := "NewCacheByNamespace"

	client, err := NewRedisByNamespace(ctx, namespace)
	if err != nil {
		xlog.Errorf(ctx, "%s GetConfig, namespace: %s err: %s", fun, namespace, err.Error())
	}
	return &Cache{
		expire:        expire,
		redisClient:   client,
		prefix:        prefix,
		withNamespace: true,
		namespace:     namespace,
	}, err
}

func (m *Cache) setData(key string, data CacheData) error {
	fun := "Cache.setData -->"
	expire := time.Duration(m.expire) * time.Second
	sdata, merr := data.Marshal()
	if merr != nil {
		sdata = []byte(merr.Error())
		merr = fmt.Errorf("%s marshal err, cache key:%s err:%v", fun, key, merr)
		expire = constants.CacheDirtyExpireTime
	}

	client, err := m.getRedisClient()
	if err != nil {
		return fmt.Errorf("%s get redis client err:%s", fun, err.Error())
	}

	err = client.Set(m.fixKey(key), sdata, expire).Err()
	if err != nil {
		return fmt.Errorf("%s set err, cache key:%s err:%v", fun, key, err)
	}

	if merr != nil {
		return merr
	}

	return nil
}

func (m *Cache) fixKey(key string) string {
	if len(m.prefix) > 0 {
		return fmt.Sprintf("%s.%s", m.prefix, key)
	}

	return key
}

func (m *Cache) getData(key string, data CacheData) error {
	fun := "Cache.getData -->"
	client, err := m.getRedisClient()
	if err != nil {
		return fmt.Errorf("%s get redis client err:%s", fun, err.Error())
	}
	sdata, err := client.Get(m.fixKey(key)).Bytes()
	if err != nil {
		return err
	}

	err = data.Unmarshal(sdata)
	if err != nil {
		return fmt.Errorf("reply data unmarshal err:%v", err)
	}

	return nil
}

// GetCache get cache data interface from key
func (m *Cache) GetCache(key string, data CacheData) error {
	fun := "Cache.GetCache -->"

	err := m.getData(key, data)
	if err == nil {
		return nil
	} else if err.Error() == constants.RedisNil {
		// 空的情况也返回正常
		return nil

	} else if err != nil {
		xlog.Warnf(context.TODO(), "%s cache key:%s err:%v", fun, key, err)
		return err
	}

	return nil
}

// Get get data if miss load and set data to cache
func (m *Cache) Get(key string, data CacheData) error {
	fun := "Cache.Get -->"

	err := m.getData(key, data)
	if err == nil {
		return nil
	}

	if err.Error() != constants.RedisNil {
		xlog.Errorf(context.TODO(), "%s cache key:%s err:%v", fun, key, err)
		return fmt.Errorf("%s cache key:%s err:%v", fun, key, err)
	}

	xlog.Infof(context.TODO(), "%s miss key:%s", fun, key)

	err = data.Load(key)
	if err != nil {
		xlog.Warnf(context.TODO(), "%s load err, cache key:%s err:%s", fun, key, err)
		return err
	}

	err = m.setData(key, data)
	if err != nil {
		xlog.Warnf(context.TODO(), "%s setData err, key:%s, err:%v", fun, key, err)
	}
	return err
}

// Set set data to cache
func (m *Cache) Set(key string, data CacheData) error {
	fun := "Cache.Set -->"
	err := m.setData(key, data)
	if err != nil {
		xlog.Errorf(context.TODO(), "%s setData err, key:%s, err:%v", fun, key, err)
	}
	return err
}

// Del delete data from cache
func (m *Cache) Del(key string) error {
	fun := "Cache.Del-->"

	client, err := m.getRedisClient()
	if err != nil {
		xlog.Errorf(context.TODO(), "%s get redis client err:%s", fun, err.Error())
		return fmt.Errorf("%s get redis client err:%s", fun, err.Error())
	}
	err = client.Del(m.fixKey(key)).Err()
	if err != nil {
		xlog.Errorf(context.TODO(), "del cache key:%s err:%s", key, err.Error())
		return fmt.Errorf("del cache key:%s err:%s", key, err.Error())
	}

	return nil
}

// get namespace update client
func (m *Cache) getRedisClient() (*RedisClient, error) {
	if m.withNamespace {
		return NewRedisByNamespace(context.Background(), m.namespace)
	}
	return m.redisClient, nil
}
