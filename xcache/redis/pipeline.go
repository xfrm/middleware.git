package redis

import (
	"context"
	"strings"
	"time"

	"gitee.com/xfrm/middleware/xtrace"
	"github.com/go-redis/redis"
)

type Pipeline struct {
	namespace string
	pipeline  redis.Pipeliner
	opts      *options
	cluster   string
}

func (m *Pipeline) Cluster() string {
	if m == nil {
		return ""
	}

	return m.cluster
}

func (m *Pipeline) fixKey(key string) string {
	if m.opts.noFixKey {
		return key
	}
	parts := []string{
		m.namespace,
		m.opts.wrapper,
		key,
	}
	if !m.opts.useWrapper {
		parts = []string{
			m.namespace,
			key,
		}
	}
	return strings.Join(parts, ".")
}

func (m *Pipeline) setSpanTags(ctx context.Context, cmders ...redis.Cmder) {
	if span := xtrace.SpanFromContext(ctx); span != nil {
		span.SetTag(xtrace.TagComponent, traceComponent)
		span.SetTag(xtrace.TagDBType, xtrace.DBTypeRedis)
		span.SetTag(xtrace.TagPalfishDBCluster, m.cluster)
		span.SetTag(xtrace.TagSpanKind, xtrace.SpanKindClient)
		for _, cmder := range cmders {
			if cmder == nil {
				continue
			}

			span.SetTag(xtrace.TagDBStatement, redisCmd(cmder))
		}
	}
}

func (m *Pipeline) Get(ctx context.Context, key string) *redis.StringCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.Get(k)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) MGet(ctx context.Context, keys ...string) *redis.SliceCmd {
	var fixKeys = make([]string, len(keys))
	for k, v := range keys {
		key := m.fixKey(v)
		fixKeys[k] = key
	}
	rcmd := m.pipeline.MGet(fixKeys...)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) Set(ctx context.Context, key string, value interface{}, expiration time.Duration) *redis.StatusCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.Set(k, value, expiration)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) MSet(ctx context.Context, pairs ...interface{}) *redis.StatusCmd {
	var fixPairs = make([]interface{}, len(pairs))
	for k, v := range pairs {
		if (k & 1) == 0 {
			key := m.fixKey(v.(string))
			fixPairs[k] = key
		} else {
			fixPairs[k] = v
		}
	}
	rcmd := m.pipeline.MSet(fixPairs...)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) GetBit(ctx context.Context, key string, offset int64) *redis.IntCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.GetBit(k, offset)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) SetBit(ctx context.Context, key string, offset int64, value int) *redis.IntCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.SetBit(k, offset, value)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) Exists(ctx context.Context, key string) *redis.IntCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.Exists(k)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) Del(ctx context.Context, keys ...string) *redis.IntCmd {
	var tkeys []string
	for _, key := range keys {
		tkeys = append(tkeys, m.fixKey(key))
	}

	rcmd := m.pipeline.Del(tkeys...)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) Expire(ctx context.Context, key string, expiration time.Duration) *redis.BoolCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.Expire(k, expiration)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) Incr(ctx context.Context, key string) *redis.IntCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.Incr(k)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) IncrBy(ctx context.Context, key string, value int64) *redis.IntCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.IncrBy(k, value)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) Decr(ctx context.Context, key string) *redis.IntCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.Decr(k)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) DecrBy(ctx context.Context, key string, value int64) *redis.IntCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.DecrBy(k, value)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) SetNX(ctx context.Context, key string, value interface{}, expiration time.Duration) *redis.BoolCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.SetNX(k, value, expiration)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) HSet(ctx context.Context, key string, field string, value interface{}) *redis.BoolCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.HSet(k, field, value)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) HDel(ctx context.Context, key string, fields ...string) *redis.IntCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.HDel(k, fields...)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) HExists(ctx context.Context, key string, field string) *redis.BoolCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.HExists(k, field)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) HGet(ctx context.Context, key string, field string) *redis.StringCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.HGet(k, field)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) HGetAll(ctx context.Context, key string) *redis.StringStringMapCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.HGetAll(k)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) HIncrBy(ctx context.Context, key string, field string, incr int64) *redis.IntCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.HIncrBy(k, field, incr)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) HIncrByFloat(ctx context.Context, key string, field string, incr float64) *redis.FloatCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.HIncrByFloat(k, field, incr)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) HKeys(ctx context.Context, key string) *redis.StringSliceCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.HKeys(k)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) HLen(ctx context.Context, key string) *redis.IntCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.HLen(k)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) HMGet(ctx context.Context, key string, fields ...string) *redis.SliceCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.HMGet(k, fields...)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) HMSet(ctx context.Context, key string, fields map[string]interface{}) *redis.StatusCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.HMSet(k, fields)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) HSetNX(ctx context.Context, key string, field string, val interface{}) *redis.BoolCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.HSetNX(k, field, val)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) HVals(ctx context.Context, key string) *redis.StringSliceCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.HVals(k)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) ZAdd(ctx context.Context, key string, members ...redis.Z) *redis.IntCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.ZAdd(k, members...)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) ZAddNX(ctx context.Context, key string, members ...redis.Z) *redis.IntCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.ZAddNX(k, members...)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) ZAddNXCh(ctx context.Context, key string, members ...redis.Z) *redis.IntCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.ZAddNXCh(k, members...)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) ZAddXX(ctx context.Context, key string, members ...redis.Z) *redis.IntCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.ZAddXX(k, members...)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) ZAddXXCh(ctx context.Context, key string, members ...redis.Z) *redis.IntCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.ZAddXXCh(k, members...)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) ZAddCh(ctx context.Context, key string, members ...redis.Z) *redis.IntCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.ZAddCh(k, members...)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) ZCard(ctx context.Context, key string) *redis.IntCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.ZCard(k)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) ZCount(ctx context.Context, key, min, max string) *redis.IntCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.ZCount(k, min, max)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) ZRange(ctx context.Context, key string, start, stop int64) *redis.StringSliceCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.ZRange(k, start, stop)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) ZRangeByLex(ctx context.Context, key string, by redis.ZRangeBy) *redis.StringSliceCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.ZRangeByLex(k, by)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) ZRangeByScore(ctx context.Context, key string, by redis.ZRangeBy) *redis.StringSliceCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.ZRangeByScore(k, by)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) ZRangeWithScores(ctx context.Context, key string, start, stop int64) *redis.ZSliceCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.ZRangeWithScores(k, start, stop)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) ZRevRange(ctx context.Context, key string, start, stop int64) *redis.StringSliceCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.ZRevRange(k, start, stop)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) ZRevRangeWithScores(ctx context.Context, key string, start, stop int64) *redis.ZSliceCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.ZRevRangeWithScores(k, start, stop)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) ZRank(ctx context.Context, key string, member string) *redis.IntCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.ZRank(k, member)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) ZRevRank(ctx context.Context, key string, member string) *redis.IntCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.ZRevRank(k, member)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) ZRem(ctx context.Context, key string, members []interface{}) *redis.IntCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.ZRem(k, members...)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) ZIncr(ctx context.Context, key string, member redis.Z) *redis.FloatCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.ZIncr(k, member)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) ZIncrNX(ctx context.Context, key string, member redis.Z) *redis.FloatCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.ZIncrNX(k, member)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) ZIncrXX(ctx context.Context, key string, member redis.Z) *redis.FloatCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.ZIncrXX(k, member)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) ZIncrBy(ctx context.Context, key string, increment float64, member string) *redis.FloatCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.ZIncrBy(k, increment, member)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) ZScore(ctx context.Context, key string, member string) *redis.FloatCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.ZScore(k, member)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) LIndex(ctx context.Context, key string, index int64) *redis.StringCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.LIndex(k, index)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) LInsert(ctx context.Context, key, op string, pivot, value interface{}) *redis.IntCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.LInsert(k, op, pivot, value)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) LLen(ctx context.Context, key string) *redis.IntCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.LLen(k)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) LPop(ctx context.Context, key string) *redis.StringCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.LPop(k)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) LPush(ctx context.Context, key string, values ...interface{}) *redis.IntCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.LPush(k, values...)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) LPushX(ctx context.Context, key string, value interface{}) *redis.IntCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.LPushX(k, value)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) LRange(ctx context.Context, key string, start, stop int64) *redis.StringSliceCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.LRange(k, start, stop)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) LRem(ctx context.Context, key string, count int64, value interface{}) *redis.IntCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.LRem(k, count, value)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) LSet(ctx context.Context, key string, index int64, value interface{}) *redis.StatusCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.LSet(k, index, value)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) LTrim(ctx context.Context, key string, start, stop int64) *redis.StatusCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.LTrim(k, start, stop)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) RPop(ctx context.Context, key string) *redis.StringCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.RPop(k)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) RPush(ctx context.Context, key string, values ...interface{}) *redis.IntCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.RPush(k, values...)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) RPushX(ctx context.Context, key string, value interface{}) *redis.IntCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.RPushX(k, value)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) TTL(ctx context.Context, key string) *redis.DurationCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.TTL(k)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) SAdd(ctx context.Context, key string, members ...interface{}) *redis.IntCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.SAdd(k, members...)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) SCard(ctx context.Context, key string) *redis.IntCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.SCard(k)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) SPop(ctx context.Context, key string) *redis.StringCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.SPop(k)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) SRem(ctx context.Context, key string, members ...interface{}) *redis.IntCmd {
	k := m.fixKey(key)
	rcmd := m.pipeline.SRem(k, members...)
	m.setSpanTags(ctx, rcmd)
	return rcmd
}

func (m *Pipeline) Exec(ctx context.Context) ([]redis.Cmder, error) {
	rcmd, err := m.pipeline.Exec()
	m.setSpanTags(ctx, rcmd...)
	return rcmd, err
}

func (m *Pipeline) Discard(ctx context.Context) error {
	return m.pipeline.Discard()
}

func (m *Pipeline) Close() error {
	return m.pipeline.Close()
}
