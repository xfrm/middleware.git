package redis

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestNewClient(t *testing.T) {
	ctx := context.Background()
	// 新配置中配置错的
	client, err := NewClient(ctx, "test/test", "test")
	assert.NoError(t, err)
	_, err = client.Set(ctx, "test", "test", 5*time.Second).Result()
	assert.NoError(t, err)

	// 旧配置中已经存在的，新配置中还未配置的
	client, err = NewClient(ctx, "base/report", "test")
	assert.NoError(t, err)
	_, err = client.Set(ctx, "test", "test", 3*time.Second).Result()
	assert.NoError(t, err)
	result, err := client.Get(ctx, "test").Result()
	assert.NoError(t, err)
	assert.Equal(t, "test", result)
}
