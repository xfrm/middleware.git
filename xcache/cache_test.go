package xcache

import (
	"context"
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

type Test struct {
	ID int `json:"id"`
}

func (p *Test) Marshal() ([]byte, error) {
	return json.Marshal(&p)
}

func (p *Test) Unmarshal(data []byte) error {
	return json.Unmarshal(data, p)
}

// cache miss load
func (p *Test) Load(key string) error {
	p.ID = 1
	return nil
}

func TestNewCommonCache(t *testing.T) {
	id := 3
	c, err := NewCommonCache("base/changeboard", "test", 10, 30)
	if err != nil {
		t.Errorf("NewCommonCache err: %s", err.Error())
	}
	var test Test
	test.ID = id
	err = c.Set("test", &test)
	assert.NoError(t, err)
	err = c.Get("test", &test)
	assert.NoError(t, err)
	assert.Equal(t, id, test.ID)
}

func TestCache_Set(t *testing.T) {
	ctx := context.Background()
	c, err := NewCacheByNamespace(ctx, "test/test", "test", 60)
	if err != nil {
		t.Errorf("NewCacheByNamespace err: %s", err.Error())
	}
	var test Test
	test.ID = 3
	err = c.Set("test", &test)
	assert.NoError(t, err)
}

func TestCache_Get(t *testing.T) {
	ctx := context.Background()
	c, err := NewCacheByNamespace(ctx, "base/test", "test", 60)
	if err != nil {
		t.Errorf("NewCacheByNamespace err: %s", err.Error())
	}
	var test Test
	test.ID = 3
	err = c.Set("test", &test)
	assert.NoError(t, err)

	test.ID = 0
	err = c.Get("test", &test)
	assert.NoError(t, err)
	assert.Equal(t, 3, test.ID)

}
