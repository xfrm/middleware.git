## 简介
xcache是伴鱼内部redis sdk，内含cache(缓存场景)、redisext(存储、复杂数据结构场景)两个子包，可完全替代sutil/cache(在外部，不安全)。使用方式上完全同sutil/cache，仅仅是引用路径不同。