package xcache

import (
	"context"
	"time"

	"gitee.com/xfrm/middleware/xcache/redis"
	"gitee.com/xfrm/middleware/xcontext"
	"gitee.com/xfrm/middleware/xlog"

	go_redis "github.com/go-redis/redis"
)

const (
	defaultTimeout = time.Second * 2
)

// NewCommonRedis new redis client in common.codis.pri.ibanyu.com
func NewCommonRedis(serverName string, poolSize int) (*RedisClient, error) {
	return newRedisClient("common.codis.pri.ibanyu.com:19000", serverName, poolSize)
}

// NewCoreRedis new redis client in core.codis.pri.ibanyu.com
func NewCoreRedis(serverName string, poolSize int) (*RedisClient, error) {
	return newRedisClient("core.codis.pri.ibanyu.com:19000", serverName, poolSize)
}

// NewRedisByAddr new redis client by addr
func NewRedisByAddr(addr, serverName string, poolSize int) (*RedisClient, error) {
	return newRedisClient(addr, serverName, poolSize)
}

// NewRedisByNamespace new redis client by apollo config
func NewRedisByNamespace(ctx context.Context, namespace string) (*RedisClient, error) {
	fun := "NewRedisByNamespace -->"
	client, err := redis.DefaultInstanceManager.GetInstance(ctx, getInstanceConf(ctx, namespace))
	if err != nil {
		xlog.Errorf(ctx, "%s GetInstance: namespace %s, err: %s", fun, namespace, err.Error())
	}
	return &RedisClient{
		client:    client,
		namespace: namespace,
	}, err
}

func getInstanceConf(ctx context.Context, namespace string) *redis.InstanceConf {
	return &redis.InstanceConf{
		Group:     xcontext.GetControlRouteGroupWithDefault(ctx, defaultRouteGroup),
		Namespace: namespace,
		Wrapper:   WrapperTypeCache,
	}
}

type RedisClient struct {
	client    *redis.Client
	namespace string
}

func newRedisClient(addr, serverName string, poolSize int) (*RedisClient, error) {
	fun := "newRedisClient-->"

	client, err := redis.NewDefaultClient(context.Background(), serverName, addr, WrapperTypeCache, poolSize, false, defaultTimeout)
	if err != nil {
		xlog.Errorf(context.TODO(), "%s NewDefaultClient: serverName %s err: %s", fun, serverName, err.Error())
	}
	return &RedisClient{
		client:    client,
		namespace: serverName,
	}, err
}

// Get ...
func (m *RedisClient) Get(key string) *go_redis.StringCmd {
	ctx := context.Background()
	return m.client.Get(ctx, key)
}

// Set ...
func (m *RedisClient) Set(key string, value interface{}, expiration time.Duration) *go_redis.StatusCmd {
	ctx := context.Background()
	return m.client.Set(ctx, key, value, expiration)
}

// Del ...
func (m *RedisClient) Del(keys ...string) *go_redis.IntCmd {
	ctx := context.Background()
	return m.client.Del(ctx, keys...)
}

// Incr ...
func (m *RedisClient) Incr(key string) *go_redis.IntCmd {
	ctx := context.Background()
	return m.client.Incr(ctx, key)
}

// SetNX ...
func (m *RedisClient) SetNX(key string, value interface{}, expiration time.Duration) *go_redis.BoolCmd {
	ctx := context.Background()
	return m.client.SetNX(ctx, key, value, expiration)
}

// TTL ...
func (m *RedisClient) TTL(key string) *go_redis.DurationCmd {
	ctx := context.Background()
	return m.client.TTL(ctx, key)
}

// Expire ...
func (m *RedisClient) Expire(key string, expiration time.Duration) *go_redis.BoolCmd {
	ctx := context.Background()
	return m.client.Expire(ctx, key, expiration)
}
