package value

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

type Test struct {
	Id int64
}

func load(ctx context.Context, key interface{}) (value interface{}, err error) {

	//return nil, fmt.Errorf("not found")
	return &Test{
		Id: 1,
	}, nil
}

func TestGet(t *testing.T) {
	ctx := context.Background()
	ass := assert.New(t)
	key := "test"

	c := NewCache("test/test", "test", 20*time.Second, load)

	var test Test
	err := c.Del(ctx, key)
	ass.NoError(err)
	err = c.Get(ctx, key, &test)
	ass.NoError(err)
	ass.Equal(int64(1), test.Id)
}

func TestGetCacheData(t *testing.T) {
	ctx := context.Background()
	ass := assert.New(t)
	key := "test"

	c := NewCache("test/test", "test", 5*time.Second, load)

	_, err := c.loadValueToCache(ctx, key)
	ass.NoError(err)

	val, err := c.GetCacheData(ctx, key)
	ass.NoError(err)
	result := val.(map[string]interface{})
	ass.Equal(float64(1), result["Id"].(float64))
}
