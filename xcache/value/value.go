// Copyright 2014 The mqrouter Author. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// TODO: 删除旧版本的 cache sdk
// TODO: 将 redis 文件夹中的 config.go, instance.go, redis.go 都拿到 sdk 根目录下
//       config 和 instance 不应该属于 redis

package value

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"gitee.com/xfrm/middleware/xcache"
	"gitee.com/xfrm/middleware/xcache/constants"
	"gitee.com/xfrm/middleware/xcache/redis"
	"gitee.com/xfrm/middleware/xcontext"
	"gitee.com/xfrm/middleware/xlog"
	"gitee.com/xfrm/middleware/xtime"
	"gitee.com/xfrm/middleware/xtrace"
)

// key类型只支持int（包含有无符号，8，16，32，64位）和string
type LoadFunc func(ctx context.Context, key interface{}) (value interface{}, err error)

// Cache
type Cache struct {
	namespace string
	prefix    string
	load      LoadFunc
	expire    time.Duration
	cluster   string
}

// NewCache
func NewCache(namespace, prefix string, expire time.Duration, load LoadFunc) *Cache {
	return &Cache{
		namespace: namespace,
		prefix:    prefix,
		load:      load,
		expire:    expire,
	}
}

func (m *Cache) getInstanceConf(ctx context.Context) *redis.InstanceConf {
	return &redis.InstanceConf{
		Group:     xcontext.GetControlRouteGroupWithDefault(ctx, constants.DefaultRouteGroup),
		Namespace: m.namespace,
		Wrapper:   xcache.WrapperTypeCache,
	}
}

func (m *Cache) getInstance(ctx context.Context) (client *redis.Client, err error) {
	client, err = redis.DefaultInstanceManager.GetInstance(ctx, m.getInstanceConf(ctx))
	if client == nil {
		return
	}

	m.cluster = client.Cluster()
	return
}

// Get get value from cache
func (m *Cache) GetCacheData(ctx context.Context, key interface{}) (value interface{}, err error) {
	command := "cache.value.SimpleGet"
	span, ctx := xtrace.StartSpanFromContext(ctx, command)
	st := xtime.NewTimeStat()
	defer func() {
		span.Finish()
		statReqDuration(ctx, m.namespace, command, m.cluster, st.Millisecond())
		statReqErr(ctx, m.namespace, command, m.cluster, err)
	}()

	err = m.getValueFromCache(ctx, key, &value)
	return
}

// Get get value from cache if miss load value to cache
func (m *Cache) Get(ctx context.Context, key, value interface{}) error {
	fun := "Cache.Get -->"
	// TODO 目前统计的是cache层的Get，后面需要拆分为redis层、cache层
	command := "cache.value.Get"
	span, ctx := xtrace.StartSpanFromContext(ctx, command)
	st := xtime.NewTimeStat()
	defer func() {
		span.Finish()
		statReqDuration(ctx, m.namespace, command, m.cluster, st.Millisecond())
	}()

	err := m.getValueFromCache(ctx, key, value)
	if err == nil {
		statReqHit(ctx, m.namespace, command, m.cluster)
		return nil
	}

	if err.Error() != redis.RedisNil {
		statReqErr(ctx, m.namespace, command, m.cluster, err)
		xlog.Errorf(ctx, "%s cache key: %v err: %v", fun, key, err)
		return fmt.Errorf("%s cache key: %v err: %v", fun, key, err)
	}
	statReqMiss(ctx, m.namespace, command, m.cluster)

	data, err := m.loadValueToCache(ctx, key)
	if err != nil {
		statReqErr(ctx, m.namespace, command, m.cluster, err)
		xlog.Errorf(ctx, "%s loadValueToCache key: %v err: %v", fun, key, err)
		return err
	}

	err = json.Unmarshal(data, value)
	if err != nil {
		statReqErr(ctx, m.namespace, command, m.cluster, err)
		return errors.New(string(data))
	}

	return nil
}

// Del ...
func (m *Cache) Del(ctx context.Context, key interface{}) error {
	fun := "Cache.Del -->"
	command := "cache.value.Del"

	span, ctx := xtrace.StartSpanFromContext(ctx, command)
	st := xtime.NewTimeStat()
	defer func() {
		span.Finish()
		statReqDuration(ctx, m.namespace, command, m.cluster, st.Millisecond())
	}()

	skey, err := m.prefixKey(key)
	if err != nil {
		statReqErr(ctx, m.namespace, command, m.cluster, err)
		xlog.Errorf(ctx, "%s fixkey, key: %v err: %v", fun, key, err)
		return err
	}

	client, err := m.getInstance(ctx)
	if err != nil {
		statReqErr(ctx, m.namespace, command, m.cluster, err)
		xlog.Errorf(ctx, "%s get instance err, namespace: %s", fun, m.namespace)
		return err
	}

	err = client.Del(ctx, skey).Err()
	if err != nil {
		statReqErr(ctx, m.namespace, command, m.cluster, err)
		return fmt.Errorf("del cache key: %v err: %s", key, err.Error())
	}

	return nil
}

// Load load value and set to cache
func (m *Cache) Load(ctx context.Context, key interface{}) error {
	command := "cache.value.Load"
	span, ctx := xtrace.StartSpanFromContext(ctx, command)
	st := xtime.NewTimeStat()
	defer func() {
		span.Finish()
		statReqDuration(ctx, m.namespace, command, m.cluster, st.Millisecond())
	}()

	_, err := m.loadValueToCache(ctx, key)
	statReqErr(ctx, m.namespace, command, m.cluster, err)

	return err
}

func (m *Cache) keyToString(key interface{}) (string, error) {
	switch t := key.(type) {
	case string:
		return t, nil
	case int8:
		return fmt.Sprintf("%d", key), nil
	case int16:
		return fmt.Sprintf("%d", key), nil
	case int32:
		return fmt.Sprintf("%d", key), nil
	case int64:
		return fmt.Sprintf("%d", key), nil
	case uint8:
		return fmt.Sprintf("%d", key), nil
	case uint16:
		return fmt.Sprintf("%d", key), nil
	case uint32:
		return fmt.Sprintf("%d", key), nil
	case uint64:
		return fmt.Sprintf("%d", key), nil
	case int:
		return fmt.Sprintf("%d", key), nil
	default:
		return "", errors.New("key err: unsupported type")
	}
}

func (m *Cache) prefixKey(key interface{}) (string, error) {
	fun := "Cache.prefixKey -->"

	skey, err := m.keyToString(key)
	if err != nil {
		xlog.Errorf(context.TODO(), "%s key: %v err:%s", fun, key, err)
		return "", err
	}

	if len(m.prefix) > 0 {
		return fmt.Sprintf("%s.%s", m.prefix, skey), nil
	}

	return skey, nil
}

func (m *Cache) getValueFromCache(ctx context.Context, key, value interface{}) error {
	fun := "Cache.getValueFromCache -->"

	skey, err := m.prefixKey(key)
	if err != nil {
		return err
	}

	client, err := m.getInstance(ctx)
	if err != nil {
		xlog.Errorf(ctx, "%s get instance err, namespace: %s", fun, m.namespace)
		return err
	}

	data, err := client.Get(ctx, skey).Bytes()
	if err != nil {
		return err
	}

	//slog.Infof(ctx, "%s key: %v data: %s", fun, key, string(data))

	err = json.Unmarshal(data, value)
	if err != nil {
		return errors.New(string(data))
	}

	return nil
}

func (m *Cache) loadValueToCache(ctx context.Context, key interface{}) (data []byte, err error) {
	fun := "Cache.loadValueToCache -->"
	expire := m.expire

	value, err := m.load(ctx, key)
	if err != nil {
		xlog.Warnf(ctx, "%s load err, cache key:%v err:%v", fun, key, err)
		data = []byte(`{}`)
		expire = constants.CacheDirtyExpireTime

	} else {
		data, err = json.Marshal(value)
		if err != nil {
			xlog.Errorf(ctx, "%s marshal err, cache key:%v err:%v", fun, key, err)
			data = []byte(`{}`)
			expire = constants.CacheDirtyExpireTime
		}
	}

	skey, err := m.prefixKey(key)
	if err != nil {
		xlog.Errorf(ctx, "%s fixkey, key: %v err:%v", fun, key, err)
		return nil, err
	}

	client, err := m.getInstance(ctx)
	if err != nil {
		xlog.Errorf(ctx, "%s get instance err, namespace: %s", fun, m.namespace)
		return nil, err
	}

	rerr := client.Set(ctx, skey, data, expire).Err()
	if rerr != nil {
		xlog.Errorf(ctx, "%s set err, cache key:%v rerr:%v", fun, key, rerr)
	}

	if err != nil {
		return nil, err
	}

	return data, nil
}
