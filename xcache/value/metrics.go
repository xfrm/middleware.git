package value

import (
	"context"

	"gitee.com/xfrm/middleware/xstat/xmetric/xprometheus"
	"gitee.com/xfrm/middleware/xtrace"
)

const (
	namespace = "palfish"
	subsystem = "cache_requests"
)

var (
	buckets = []float64{5, 10, 25, 50, 100, 250, 500, 1000, 2500}

	_metricRequestDuration = xprometheus.NewHistogram(&xprometheus.HistogramVecOpts{
		Namespace:  namespace,
		Subsystem:  subsystem,
		Name:       "duration_ms",
		Help:       "cache requests duration(ms), include load time.",
		LabelNames: []string{"namespace", "command", "cluster", xprometheus.LabelCallerEndpoint},
		Buckets:    buckets,
	})
	_metricReqErr = xprometheus.NewCounter(&xprometheus.CounterVecOpts{
		Namespace:  namespace,
		Subsystem:  subsystem,
		Name:       "err_total",
		Help:       "cache.value error total",
		LabelNames: []string{"namespace", "command", "cluster", xprometheus.LabelCallerEndpoint},
	})
	_metricHits = xprometheus.NewCounter(&xprometheus.CounterVecOpts{
		Namespace:  namespace,
		Subsystem:  subsystem,
		Name:       "hits_total",
		Help:       "cache.value hits total",
		LabelNames: []string{"namespace", "command", "cluster", xprometheus.LabelCallerEndpoint},
	})
	_metricMiss = xprometheus.NewCounter(&xprometheus.CounterVecOpts{
		Namespace:  namespace,
		Subsystem:  subsystem,
		Name:       "miss_total",
		Help:       "cache.value miss total",
		LabelNames: []string{"namespace", "command", "cluster", xprometheus.LabelCallerEndpoint},
	})
)

func statReqDuration(ctx context.Context, namespace, command, cluster string, durationMS int64) {
	_metricRequestDuration.With("namespace", namespace, "command", command, "cluster", cluster,
		xprometheus.LabelCallerEndpoint, xtrace.ExtractSpanOperationName(ctx)).Observe(float64(durationMS))
}

func statReqErr(ctx context.Context, namespace, command, cluster string, err error) {
	if err != nil {
		_metricReqErr.With("namespace", namespace, "command", command, "cluster", cluster,
			xprometheus.LabelCallerEndpoint, xtrace.ExtractSpanOperationName(ctx)).Inc()
	}
	return
}

func statReqHit(ctx context.Context, namespace, command, cluster string) {
	_metricHits.With("namespace", namespace, "command", command, "cluster", cluster,
		xprometheus.LabelCallerEndpoint, xtrace.ExtractSpanOperationName(ctx)).Inc()
}

func statReqMiss(ctx context.Context, namespace, command, cluster string) {
	_metricMiss.With("namespace", namespace, "command", command, "cluster", cluster,
		xprometheus.LabelCallerEndpoint, xtrace.ExtractSpanOperationName(ctx)).Inc()
}
