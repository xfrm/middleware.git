package xcache

const (
	SpanLogKeyKey    = "key"
	SpanLogCacheType = "cache"
	SpanLogOp        = "op"
)

const defaultRouteGroup = "default"

const (
	WrapperTypeCache      = "c"
	WrapperTypeRedisExt   = "e"
	WrapperTypeRedisBloom = "b"
)
