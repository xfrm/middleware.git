package redisext

import (
	"context"
	crypto_rand "crypto/rand"
	"fmt"
	"math/rand"
	"testing"
	"time"

	"github.com/go-redis/redis"
	"github.com/stretchr/testify/assert"
)

var GlobalUidStrs = []string{
	"626791115470696448",
	"626791952083353600",
	"626792243654590464",
}

func TestBloom(t *testing.T) {
	bloom, err := NewRedisBloom("base/report", "bloom_test", 100, 0.01)
	assert.Nil(t, err)

	err = bloom.Add(context.Background(), "test_ele")
	assert.Nil(t, err)

	ok, err := bloom.Exists(context.Background(), "test_ele")
	assert.Nil(t, err)
	assert.True(t, ok)

	ok, err = bloom.Exists(context.Background(), "unexists_ele")
	assert.Nil(t, err)
	assert.False(t, ok)

	err = bloom.Del(context.Background())
	assert.Nil(t, err)
}

func TestBloomM(t *testing.T) {
	bloom, err := NewRedisBloom("base/report", "bloom_test", 100, 0.01)
	assert.Nil(t, err)

	err = bloom.Madd(context.Background(), "test_ele1", "test_ele2", "test_ele3")
	assert.Nil(t, err)

	oks, err := bloom.Mexists(context.Background(), "test_ele1", "test_ele2", "unexists_ele1", "unexists_ele2")
	assert.Nil(t, err)
	assert.True(t, oks[0])
	assert.True(t, oks[1])
	assert.False(t, oks[2])
	assert.False(t, oks[3])

	err = bloom.Del(context.Background())
	assert.Nil(t, err)
}

func BenchmarkRedisSet(b *testing.B) {
	redisExt := NewRedisExt("base/report", "set_test")

	for i := 0; i < b.N; i++ {
		key := randString(10, 20)
		_, err := redisExt.Set(context.Background(), key, key, 5*time.Second)
		if err != nil {
			b.Errorf("redis set error, err: %v", err)
		}
	}
}

func BenchmarkRedisGet(b *testing.B) {
	redisExt := NewRedisExt("base/report", "get_test")
	for i := 0; i < b.N; i++ {
		_, err := redisExt.Get(context.Background(), randString(10, 20))
		if err != nil && err != redis.Nil {
			b.Errorf("redis get error, err: %v", err)
		}
	}
}

func BenchmarkBloomMexists(b *testing.B) {
	bloom, err := NewRedisBloom("base/report", "bloom_test_add", 30000000, 0.01)
	if err != nil {
		b.Errorf("bloom init err: %s", err)
		return
	}

	// 插入一条数据，使得 bloom 在 redis 中实际存在
	err = bloom.Add(context.Background(), randString(10, 20))
	if err != nil {
		b.Errorf("bloom add error, err: %v", err)
	}

	for i := 0; i < b.N; i++ {
		_, err := bloom.Mexists(context.Background(), GlobalUidStrs...)
		if err != nil {
			b.Errorf("bloomÅ exists error, err: %v", err)
		}
	}
	if err := bloom.Del(context.Background()); err != nil {
		b.Errorf("bloom del error, err: %v", err)
	}
}

func BenchmarkBloomAdd(b *testing.B) {
	bloom, err := NewRedisBloom("base/report", "bloom_test_add", 30000000, 0.01)
	if err != nil {
		b.Errorf("bloom init err: %s", err)
		return
	}
	for i := 0; i < b.N; i++ {
		err := bloom.Add(context.Background(), randString(10, 20))
		if err != nil {
			b.Errorf("bloom add error, err: %v", err)
		}
	}
	if err := bloom.Del(context.Background()); err != nil {
		b.Errorf("bloom del error, err: %v", err)
	}
}

func BenchmarkBloomExists(b *testing.B) {
	bloom, err := NewRedisBloom("base/report", "bloom_test_exists", 30000000, 0.01)
	if err != nil {
		b.Errorf("bloom init err: %s", err)
		return
	}

	// 插入一条数据，使得 bloom 在 redis 中实际存在
	err = bloom.Add(context.Background(), randString(10, 20))
	if err != nil {
		b.Errorf("bloom add error, err: %v", err)
	}

	for i := 0; i < b.N; i++ {
		_, err := bloom.Exists(context.Background(), randString(10, 20))
		if err != nil {
			b.Errorf("bloom exists error, err: %v", err)
		}
	}
	if err := bloom.Del(context.Background()); err != nil {
		b.Errorf("bloom del error, err: %v", err)
	}
}

func TestMurmurHash64A(t *testing.T) {
	tests := []struct {
		Input string
		Want  uint64
	}{
		{"hello world", 13468290891533438433},
		{"我是中文", 7973841408974827506},
		{"", 1923352212695860590},
	}

	for _, test := range tests {
		val := murmurHash64A(test.Input, 0xc6a4a7935bd1e995)
		assert.Equal(t, val, test.Want, "input: %s, output: %d, want: %d", test.Input, val, test.Want)
	}
}

func TestBloomCalcHash64(t *testing.T) {
	tests := []struct {
		Input string
		WantA uint64
		WantB uint64
	}{
		{"hello world", 13468290891533438433, 11944398893268354102},
		{"我是中文", 7973841408974827506, 5275313961451744804},
		{"", 1923352212695860590, 13537199645396948295},
	}
	bloom := RedisBloom{}
	for _, test := range tests {
		hash := bloom.bloomCalcHash64(test.Input)
		assert.Equal(t, hash.A, test.WantA, "input: %s, output: %d, want: %d", test.Input, hash.A, test.WantA)
		assert.Equal(t, hash.B, test.WantB, "input: %s, output: %d, want: %d", test.Input, hash.B, test.WantB)
	}
}

func TestBloomInit(t *testing.T) {
	var bloom RedisBloom
	bloom.Init("base/report", "bloom_test", 100, 0.01)
	assert.Equal(t, bloom.hashes, 7, "input: entries: 100, errRate: 0.01， output: %d, want: 7", bloom.hashes)
	assert.Equal(t, bloom.bits, int64(958), "input: entries: 100, errRate: 0.01， output: %d, want: 958", bloom.bits)
}

func BenchmarkRandString20(b *testing.B) {
	for i := 0; i < b.N; i++ {
		randString(10, 20)
	}
}

func randString(left, right int) string {
	rand.Seed(time.Now().UnixNano())
	randomInt := rand.Intn(right-left) + left
	return randomFixLengthString(randomInt)
}

func randomFixLengthString(n int) string {
	if n <= 0 {
		return ""
	}
	randBytes := make([]byte, n/2)
	crypto_rand.Read(randBytes)
	return fmt.Sprintf("%x", randBytes)
}
