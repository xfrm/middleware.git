package redisext

import (
	"context"

	"gitee.com/xfrm/middleware/xstat/xmetric/xprometheus"
	"gitee.com/xfrm/middleware/xtrace"

	go_redis "github.com/go-redis/redis"
)

const (
	namespace = "palfish"
	subsystem = "redis_requests"
)

var (
	buckets = []float64{5, 10, 25, 50, 100, 250, 500, 1000, 2500}

	// caller_endpoint 调用方方法
	// cluster 集群名
	_metricRequestDuration = xprometheus.NewHistogram(&xprometheus.HistogramVecOpts{
		Namespace:  namespace,
		Subsystem:  subsystem,
		Name:       "duration_ms",
		Help:       "redisext requests duration(ms).",
		LabelNames: []string{"namespace", "command", "cluster", xprometheus.LabelCallerEndpoint},
		Buckets:    buckets,
	})

	_metricReqErr = xprometheus.NewCounter(&xprometheus.CounterVecOpts{
		Namespace:  namespace,
		Subsystem:  subsystem,
		Name:       "err_total",
		Help:       "redisext requests error total",
		LabelNames: []string{"namespace", "command", "cluster", xprometheus.LabelCallerEndpoint},
	})
)

func statReqDuration(ctx context.Context, namespace, command string, cluster string, durationMS int64) {
	_metricRequestDuration.With("namespace", namespace, "command", command, "cluster", cluster,
		xprometheus.LabelCallerEndpoint, xtrace.ExtractSpanOperationName(ctx)).Observe(float64(durationMS))
}

func statReqErr(ctx context.Context, namespace, command string, cluster string, err error) {
	if err != nil && err != go_redis.Nil {
		_metricReqErr.With("namespace", namespace, "command", command, "cluster", cluster,
			xprometheus.LabelCallerEndpoint, xtrace.ExtractSpanOperationName(ctx)).Inc()
	}
	return
}
