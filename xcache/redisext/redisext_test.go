package redisext

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

const (
	zsetTestKey = "myzset"
)

func TestRedisExt_Set_(t *testing.T) {
	ctx := context.Background()
	re := NewRedisExt("test/test", "test")
	k, v := "hello", "world"
	_, err := re.Set(ctx, k, v, 10*time.Second)
	assert.NoError(t, err)
}

func TestRedisExt_Type_(t *testing.T) {
	ctx := context.Background()
	re := NewRedisExt("test/test", "test")
	k, v := "hello", "world"
	_, err := re.Set(ctx, k, v, 3*time.Second)
	assert.NoError(t, err)
	valType, err := re.Type(ctx, k)
	assert.NoError(t, err)
	assert.Equal(t, "string", valType)
}

func TestRedisExt_Do_(t *testing.T) {
	ctx := context.Background()
	re := NewRedisExt("test/test", "test")
	k, k2, v, v2 := "hello", "hi", "world", "go"

	_, err := re.Do(ctx, "set", []string{k}, v, "EX", "3")
	assert.NoError(t, err)
	_, err = re.Do(ctx, "set", []string{k2}, v2, "EX", "3")
	assert.NoError(t, err)

	_, err = re.Do(ctx, "mget", []string{k, k2})
	assert.NoError(t, err)

	val, err := re.Get(ctx, k)
	assert.NoError(t, err)
	assert.Equal(t, v, val)
	val, err = re.Get(ctx, k2)
	assert.NoError(t, err)
	assert.Equal(t, v2, val)

	time.Sleep(3 * time.Second)
	val, err = re.Get(ctx, k)
	assert.Error(t, err)
}

func TestRedisExt_ZAdd(t *testing.T) {
	ctx := context.Background()

	re := NewRedisExt("base/report", "test")

	members := []Z{
		{1, "one"},
		{2, "two"},
		{3, "three"},
	}
	n, err := re.ZAdd(ctx, zsetTestKey, members)
	assert.NoError(t, err)
	assert.Equal(t, int64(len(members)), n)

	n, err = re.ZAddNX(ctx, zsetTestKey, members)
	assert.NoError(t, err)
	assert.Equal(t, int64(0), n)

	dn, err := re.Del(ctx, zsetTestKey)
	assert.NoError(t, err)
	assert.Equal(t, int64(1), dn)
}

func TestRedisExt_ZRevRangeByScore(t *testing.T) {
	ctx := context.Background()

	re := NewRedisExt("base/test", "test")

	members := []Z{
		{1, "one"},
		{2, "two"},
		{3, "three"},
	}
	n, err := re.ZAdd(ctx, zsetTestKey, members)
	assert.NoError(t, err)
	assert.Equal(t, int64(len(members)), n)

	ss, err := re.ZRevRangeByScore(ctx, zsetTestKey, ZRangeBy{
		Min: "-inf",
		Max: "+inf",
	})
	assert.NoError(t, err)
	assert.Len(t, ss, 3)
	assert.Equal(t, "three", ss[0])
	assert.Equal(t, "two", ss[1])
	assert.Equal(t, "one", ss[2])

	dn, err := re.Del(ctx, zsetTestKey)
	assert.NoError(t, err)
	assert.Equal(t, int64(1), dn)
}

func TestRedisExt_ZRevRangeByScoreWithScores(t *testing.T) {
	ctx := context.Background()

	re := NewRedisExt("base/test", "test")

	members := []Z{
		{1, "one"},
		{2, "two"},
		{3, "three"},
	}
	n, err := re.ZAdd(ctx, zsetTestKey, members)
	assert.NoError(t, err)
	assert.Equal(t, int64(len(members)), n)

	zs, err := re.ZRevRangeByScoreWithScores(ctx, zsetTestKey, ZRangeBy{
		Min: "-inf",
		Max: "+inf",
	})
	assert.NoError(t, err)
	assert.Len(t, zs, 3)
	assert.Equal(t, []Z{
		{
			Score:  3,
			Member: "three",
		},
		{
			Score:  2,
			Member: "two",
		},
		{
			Score:  1,
			Member: "one",
		},
	}, zs)

	dn, err := re.Del(ctx, zsetTestKey)
	assert.NoError(t, err)
	assert.Equal(t, int64(1), dn)
}

func TestRedisExt_ZRange(t *testing.T) {
	ctx := context.Background()

	re := NewRedisExt("base/report", "test")

	// prepare
	members := []Z{
		{1, "one"},
		{2, "two"},
		{3, "three"},
	}

	n, err := re.ZAdd(ctx, zsetTestKey, members)
	assert.NoError(t, err)
	assert.Equal(t, int64(len(members)), n)

	// tests
	ss, err := re.ZRange(ctx, zsetTestKey, 0, 2)
	assert.NoError(t, err)
	assert.Equal(t, []string{"one", "two", "three"}, ss)

	rss, err := re.ZRevRange(ctx, zsetTestKey, 0, 2)
	assert.NoError(t, err)
	assert.Equal(t, []string{"three", "two", "one"}, rss)

	zs, err := re.ZRangeWithScores(ctx, zsetTestKey, 0, 2)
	assert.NoError(t, err)
	assert.Equal(t, members, zs)

	rzs, err := re.ZRevRangeWithScores(ctx, zsetTestKey, 0, 2)
	assert.NoError(t, err)
	assert.Equal(t, []Z{
		{3, "three"},
		{2, "two"},
		{1, "one"},
	}, rzs)

	// cleanup
	dn, err := re.Del(ctx, zsetTestKey)
	assert.NoError(t, err)
	assert.Equal(t, int64(1), dn)
}

func TestRedisExt_ZRank(t *testing.T) {
	ctx := context.Background()
	re := NewRedisExt("base/report", "test")

	// prepare
	members := []Z{
		{1, "one"},
		{2, "two"},
		{3, "three"},
	}

	n, err := re.ZAdd(ctx, zsetTestKey, members)
	assert.NoError(t, err)
	assert.Equal(t, int64(len(members)), n)

	// tests
	n, err = re.ZRank(ctx, zsetTestKey, "one")
	assert.NoError(t, err)
	assert.Equal(t, int64(0), n)

	n, err = re.ZRevRank(ctx, zsetTestKey, "one")
	assert.NoError(t, err)
	assert.Equal(t, int64(2), n)

	n, err = re.ZRank(ctx, zsetTestKey, "four")
	assert.Error(t, err)
	assert.Equal(t, int64(0), n)

	n, err = re.ZRevRank(ctx, zsetTestKey, "four")
	assert.Error(t, err)
	assert.Equal(t, int64(0), n)

	// cleanup
	dn, err := re.Del(ctx, zsetTestKey)
	assert.NoError(t, err)
	assert.Equal(t, int64(1), dn)
}

func TestRedisExt_ZCount(t *testing.T) {
	ctx := context.Background()
	re := NewRedisExt("base/report", "test")

	// prepare
	members := []Z{
		{1, "one"},
		{2, "two"},
		{3, "three"},
	}

	n, err := re.ZAdd(ctx, zsetTestKey, members)
	assert.NoError(t, err)
	assert.Equal(t, int64(len(members)), n)

	// tests
	n, err = re.ZCount(ctx, zsetTestKey, "2", "3")
	assert.NoError(t, err)
	assert.Equal(t, int64(2), n)

	// cleanup
	dn, err := re.Del(ctx, zsetTestKey)
	assert.NoError(t, err)
	assert.Equal(t, int64(1), dn)
}

func TestRedisExt_ZScore(t *testing.T) {
	ctx := context.Background()
	re := NewRedisExt("base/report", "test")

	// prepare
	members := []Z{
		{1, "one"},
		{2, "two"},
		{3, "three"},
	}

	n, err := re.ZAdd(ctx, zsetTestKey, members)
	assert.NoError(t, err)
	assert.Equal(t, int64(len(members)), n)

	// tests
	f, err := re.ZScore(ctx, zsetTestKey, "two")
	assert.NoError(t, err)
	assert.Equal(t, float64(2), f)

	f, err = re.ZScore(ctx, zsetTestKey, "one")
	assert.NoError(t, err)
	assert.Equal(t, float64(1), f)

	f, err = re.ZScore(ctx, zsetTestKey, "four")
	assert.Error(t, err)
	assert.Equal(t, float64(0), f)

	// cleanup
	dn, err := re.Del(ctx, zsetTestKey)
	assert.NoError(t, err)
	assert.Equal(t, int64(1), dn)
}

func TestRedisExt_Expire(t *testing.T) {
	ctx := context.Background()
	re := NewRedisExt("test/test", "test")

	// prepare
	members := []Z{
		{1, "one"},
		{2, "two"},
		{3, "three"},
	}

	n, err := re.ZAdd(ctx, zsetTestKey, members)
	assert.NoError(t, err)
	assert.Equal(t, int64(len(members)), n)

	expiration := 100 * time.Millisecond
	b, err := re.Expire(ctx, zsetTestKey, expiration)
	assert.NoError(t, err)
	assert.True(t, b)

	time.Sleep(expiration * 2)

	n, err = re.Exists(ctx, zsetTestKey)
	assert.NoError(t, err)
	assert.Equal(t, int64(0), n)
}

func TestRedisExt_SetBit(t *testing.T) {
	ctx := context.Background()
	re := NewRedisExt("base/report", "test")
	_, err := re.Del(ctx, "bitoptest")
	assert.NoError(t, err)
	n, err := re.SetBit(ctx, "bitoptest", 0, 1)
	assert.NoError(t, err)
	assert.Equal(t, int64(0), n)
}

func TestRedisExt_GetBit(t *testing.T) {
	ctx := context.Background()
	re := NewRedisExt("base/report", "test")
	_, err := re.Del(ctx, "bitoptest")
	assert.NoError(t, err)

	n, err := re.GetBit(ctx, "bitoptest", 1)
	assert.NoError(t, err)
	assert.Equal(t, int64(0), n)
}

func TestRedisExt_MSet(t *testing.T) {
	ctx := context.Background()
	re := NewRedisExt("base/report", "test")

	resp, err := re.MSet(ctx, "setkey1", "setvalue1", "setkey2", "setvalue2")
	assert.NoError(t, err)
	assert.Equal(t, "OK", resp)
}

func TestRedisExt_MGet(t *testing.T) {
	ctx := context.Background()
	re := NewRedisExt("base/report", "test")
	re.MSet(ctx, "getkey1", "getvalue1", "getkey2", "getvalue2")
	resp, err := re.MGet(ctx, []string{"getkey1", "getkey2"}...)
	fmt.Printf("resp:%+v\n", resp)
	assert.NoError(t, err)
	assert.Equal(t, len(resp), 2)
	assert.Contains(t, []string{"getvalue1", "getvalue2"}, resp[0])
	assert.Contains(t, []string{"getvalue1", "getvalue2"}, resp[1])
}

func TestRedisExt_HSetNX(t *testing.T) {
	ctx := context.Background()
	re := NewRedisExt("base/report", "test")
	b, err := re.HSetNX(ctx, "hkey1", "field1", "value1")
	assert.NoError(t, err)
	assert.Equal(t, true, b)
	b, err = re.HSetNX(ctx, "hkey1", "field1", "value1")
	assert.NoError(t, err)
	assert.Equal(t, false, b)
	re.Del(ctx, "hkey1")
}

func TestRedisExt_TTL(t *testing.T) {
	ctx := context.Background()
	ttl := 10 * time.Second
	re := NewRedisExt("base/report", "test")
	re.Set(ctx, "getttl1", "test", ttl)
	d, err := re.TTL(ctx, "getttl1")
	assert.NoError(t, err)
	assert.Equal(t, ttl, d)
	d, err = re.TTL(ctx, "getttl2")
	assert.NoError(t, err)
	assert.Equal(t, -2*time.Second, d)
	re.Set(ctx, "getttl3", "test", 0)
	d, err = re.TTL(ctx, "getttl3")
	assert.NoError(t, err)
	assert.Equal(t, -1*time.Second, d)
	re.Del(ctx, "getttl3")
}

func TestNewRedisExtNoPrefix(t *testing.T) {
	ctx := context.Background()
	val := "val"
	re := NewRedisExtNoPrefix("base/report")
	preRedis := NewRedisExt("base/report", "test")

	_, err := re.Set(ctx, "set", val, 10*time.Second)
	assert.NoError(t, err)

	_, err = preRedis.Set(ctx, "set", val+"prefix", 10*time.Second)
	assert.NoError(t, err)

	s, err := re.Get(ctx, "set")
	assert.NoError(t, err)
	assert.Equal(t, s, val)

	s, err = preRedis.Get(ctx, "set")
	assert.NoError(t, err)
	assert.Equal(t, s, val+"prefix")
}

func TestRedisExt_SScan(t *testing.T) {
	ctx := context.Background()
	re := NewRedisExt("base/report", "test")

	i, err := re.SAdd(ctx, "sscantest", 1, 2, 3, 4)
	assert.NoError(t, err)
	assert.Equal(t, int64(4), i)

	b, err := re.SIsMember(ctx, "sscantest", 1)
	assert.NoError(t, err)
	assert.True(t, b)

	i, err = re.SCard(ctx, "sscantest")
	assert.NoError(t, err)
	assert.Equal(t, int64(4), i)

	i, err = re.SRem(ctx, "sscantest", 1)
	assert.NoError(t, err)
	assert.Equal(t, int64(1), i)

	vals, cursor, err := re.SScan(ctx, "sscantest", 0, "", 4)
	assert.NoError(t, err)
	assert.Equal(t, 3, len(vals))
	assert.Equal(t, uint64(0), cursor)

	s, err := re.SPopN(ctx, "sscantest", 4)
	assert.NoError(t, err)
	assert.Equal(t, 3, len(s))
}

func TestRedisExt_HScan(t *testing.T) {
	ctx := context.Background()
	re := NewRedisExt("base/report", "test")
	key := "hscantest"
	data := make(map[string]interface{})
	data["baidu"] = "baidu.com"
	data["163"] = "163.com"

	_, err := re.HMSet(ctx, key, data)
	assert.NoError(t, err)

	vals, cursor, err := re.HScan(ctx, key, 0, "", 2)
	assert.NoError(t, err)
	assert.Equal(t, uint64(0), cursor)
	assert.Equal(t, 4, len(vals))

	n, err := re.HDel(ctx, key, "baidu", "163")
	assert.NoError(t, err)
	assert.Equal(t, int64(2), n)
}

func TestRedisExt_ZScan(t *testing.T) {
	ctx := context.Background()
	re := NewRedisExt("base/report", "test")
	key := "zscantest"
	members := []Z{
		{Score: 2000, Member: "jack"},
		{Score: 3000, Member: "tom"},
		{Score: 5000, Member: "peter"},
	}

	n, err := re.ZAdd(ctx, key, members)
	assert.NoError(t, err)
	assert.Equal(t, int64(3), n)

	vals, cursor, err := re.ZScan(ctx, key, 0, "", 3)
	assert.NoError(t, err)
	assert.Equal(t, uint64(0), cursor)
	assert.Equal(t, 6, len(vals))

	n, err = re.ZRem(ctx, key, []interface{}{"jack", "tom", "peter"})
	assert.NoError(t, err)
	assert.Equal(t, int64(3), n)
}

func TestRedisExt_ZRem(t *testing.T) {
	ctx := context.Background()
	re := NewRedisExt("base/report", "test")

	key := "zremtest"
	members := []Z{
		{Score: 2000, Member: "jack"},
		{Score: 3000, Member: "tom"},
		{Score: 5000, Member: "peter"},
	}
	n, err := re.ZAdd(ctx, key, members)
	assert.NoError(t, err)
	assert.Equal(t, int64(3), n)

	n, err = re.ZRemRangeByRank(ctx, key, 0, 2)
	assert.NoError(t, err)
	assert.Equal(t, int64(3), n)

	n, err = re.ZAdd(ctx, key, members)
	assert.NoError(t, err)
	assert.Equal(t, int64(3), n)

	n, err = re.ZRemRangeByScore(ctx, key, "1500", "3500")
	assert.NoError(t, err)
	assert.Equal(t, int64(2), n)

	ss, err := re.ZRange(ctx, key, 0, -1)
	assert.NoError(t, err)
	assert.Equal(t, 1, len(ss))
	assert.Equal(t, "peter", ss[0])

	_, err = re.ZRem(ctx, "zremtest", []interface{}{ss[0]})
	assert.NoError(t, err)
}

func TestRedisExt_Append(t *testing.T) {
	ctx := context.Background()
	re := NewRedisExt("base/report", "test")
	key := "appendtest"

	s, err := re.Set(ctx, key, "Hello,", 30*time.Second)
	assert.NoError(t, err)
	assert.Equal(t, "OK", s)

	n, err := re.Append(ctx, key, "World")
	assert.NoError(t, err)
	assert.Equal(t, int64(11), n)

	s, err = re.Get(ctx, key)
	assert.NoError(t, err)
	assert.Equal(t, "Hello,World", s)
}

func TestRedisExt_SMembers(t *testing.T) {
	ctx := context.Background()
	re := NewRedisExt("base/report", "test")
	key := "smemberstest"
	members := []string{"m1", "m2", "m3"}

	i, err := re.SAdd(ctx, key, members)
	assert.NoError(t, err)
	assert.Equal(t, int64(3), i)

	arr, err := re.SMembers(ctx, key)
	assert.NoError(t, err)
	assert.Equal(t, 3, len(arr))

	b, err := re.Expire(ctx, key, 5*time.Second)
	assert.NoError(t, err)
	assert.True(t, b)
}

func TestRedisExt_SRandMembers(t *testing.T) {
	ctx := context.Background()
	re := NewRedisExt("base/report", "test")
	key := "smemberstest"
	members := []string{"m1", "m2", "m3"}

	_, err := re.Del(ctx, key)
	assert.NoError(t, err)
	i, err := re.SAdd(ctx, key, members)
	assert.NoError(t, err)
	assert.Equal(t, int64(3), i)

	s, err := re.SRandMember(ctx, key)
	assert.NoError(t, err)
	assert.Equal(t, true, isContain(members, s))

	b, err := re.Expire(ctx, key, 5*time.Second)
	assert.NoError(t, err)
	assert.True(t, b)
}

func TestRedisExt_SRandMembersN(t *testing.T) {
	ctx := context.Background()
	re := NewRedisExt("base/report", "test")
	key := "smemberstest"
	members := []string{"m1", "m2", "m3"}

	_, err := re.Del(ctx, key)
	assert.NoError(t, err)
	i, err := re.SAdd(ctx, key, members)
	assert.NoError(t, err)
	assert.Equal(t, int64(3), i)

	arr, err := re.SRandMemberN(ctx, key, 2)
	assert.NoError(t, err)
	assert.Equal(t, 2, len(arr))
	assert.Equal(t, true, isContain(members, arr[0]))
	assert.Equal(t, true, isContain(members, arr[1]))

	b, err := re.Expire(ctx, key, 5*time.Second)
	assert.NoError(t, err)
	assert.True(t, b)
}

func TestRedisExt_ZRangeByScoreWithScores(t *testing.T) {
	ctx := context.Background()
	re := NewRedisExt("base/report", "test")

	key := "zremtest"
	members := []Z{
		{Score: 2000, Member: "jack"},
		{Score: 3000, Member: "tom"},
		{Score: 5000, Member: "peter"},
	}
	n, err := re.ZAdd(ctx, key, members)
	assert.NoError(t, err)
	assert.Equal(t, int64(3), n)

	zs, err := re.ZRangeByScoreWithScores(ctx, key, ZRangeBy{
		Min: "-inf",
		Max: "4000",
	})
	assert.NoError(t, err)
	assert.Equal(t, 2, len(zs))
	assert.Equal(t, float64(2000), zs[0].Score)
	assert.Equal(t, float64(3000), zs[1].Score)

	b, err := re.Expire(ctx, key, 5*time.Second)
	assert.NoError(t, err)
	assert.True(t, b)
}

func TestRedisExt_SInter(t *testing.T) {
	ctx := context.Background()
	re := NewRedisExt("test/test", "test")
	key1 := "key1"
	key2 := "key2"
	re.Del(ctx, key1)
	re.Del(ctx, key2)
	i, err := re.SAdd(ctx, key1, "a", "b", "c")
	assert.NoError(t, err)
	assert.Equal(t, int64(3), i)

	i, err = re.SAdd(ctx, key2, "c")
	assert.NoError(t, err)
	assert.Equal(t, int64(1), i)

	result, err := re.SInter(ctx, key1, key2)
	assert.NoError(t, err)
	assert.Equal(t, result, []string{"c"})
}

func TestRedisExt_PFAdd(t *testing.T) {
	ctx := context.Background()
	ass := assert.New(t)
	re := NewRedisExt("test/test", "test")
	key1 := "hll1"
	key2 := "hll2"
	key3 := "hll3"
	result, err := re.PFAdd(ctx, key1, "foo", "bar", "zap", "a")
	ass.NoError(err)
	ass.Equal(int64(1), result)

	result, err = re.PFAdd(ctx, key2, "a", "b", "c", "foo")
	ass.NoError(err)
	ass.Equal(int64(1), result)

	str, err := re.PFMerge(ctx, key3, key1, key2)
	ass.NoError(err)
	ass.Equal("OK", str)

	result, err = re.PFCount(ctx, key3)
	ass.NoError(err)
	ass.Equal(int64(6), result)

	re.Del(ctx, key1)
	re.Del(ctx, key2)
	re.Del(ctx, key3)
}

func isContain(items []string, item string) bool {
	for _, eachItem := range items {
		if eachItem == item {
			return true
		}
	}
	return false
}
