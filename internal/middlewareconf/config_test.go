package middlewareconf

import (
	"context"
	"testing"

	"github.com/stretchr/testify/suite"
)

type MiddleConfSuite struct {
	suite.Suite

	config *MiddleConf
}

func (p *MiddleConfSuite) SetupSuite() {
	ctx := context.Background()
	conf, err := New(ctx)
	p.NoError(err)

	p.config = conf
}

func (p *MiddleConfSuite) TestGetMQConfig() {
	mqConfig, err := p.config.GetMQConfig(context.Background(), "palfish.test.test", MiddlewareTypeKafka)
	p.NoError(err)
	p.Equal(true, mqConfig.Async)
	p.Equal(100000, mqConfig.MaxBytes)
}

func TestMiddleConfSuite(t *testing.T) {
	middleConf := &MiddleConfSuite{}
	suite.Run(t, middleConf)
}
