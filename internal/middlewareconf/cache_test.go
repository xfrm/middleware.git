package middlewareconf

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMiddleConf_ParseCacheKey(t *testing.T) {
	middlewareConfig, err := New(context.Background())
	assert.NoError(t, err)
	key, err := middlewareConfig.ParseCacheKey(context.Background(), "base/report.default.redis.addr")
	assert.NoError(t, err)
	assert.Equal(t, "base/report", key.Namespace)
	assert.Equal(t, "default", key.Group)

	key, err = middlewareConfig.ParseCacheKey(context.Background(), "base/report.test.default.redis.addr")
	assert.NoError(t, err)
	assert.Equal(t, "base/report.test", key.Namespace)
	assert.Equal(t, "default", key.Group)

	key, err = middlewareConfig.ParseCacheKey(context.Background(), "base/report.test.test.default.redis.addr")
	assert.NoError(t, err)
	assert.Equal(t, "base/report.test.test", key.Namespace)
	assert.Equal(t, "default", key.Group)
}

func (p *MiddleConfSuite) TestGetNewCacheConfig() {
	cacheConfig, err := p.config.GetCacheConfig(context.Background(), "test/test", MiddlewareTypeNewCache)
	p.NoError(err)
	p.Equal("common.codis.pri.ibanyu.com:19000", cacheConfig.Addr)
	p.Equal("", cacheConfig.Password)

	cacheConfig, err = p.config.GetCacheConfig(context.Background(), "base/report", MiddlewareTypeNewCache)
	p.NoError(err)
	p.Equal("common.codis.pri.ibanyu.com:19000", cacheConfig.Addr)
	p.Equal("", cacheConfig.Password)
}
