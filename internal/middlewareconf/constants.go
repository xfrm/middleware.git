package middlewareconf

type MiddlewareType int

const (
	MiddlewareTypeKafka MiddlewareType = iota
	MiddlewareTypeDelay
	MiddlewareTypeRedis
	MiddlewareTypePulsar
	MiddlewareTypeNewCache
)

func (t MiddlewareType) String() string {
	switch t {
	case MiddlewareTypeKafka:
		return "kafka"
	case MiddlewareTypeDelay:
		return "delay"
	case MiddlewareTypeRedis:
		return "redis"
	case MiddlewareTypePulsar:
		return "pulsar"
	case MiddlewareTypeNewCache:
		return "redis"
	default:
		return ""
	}
}

const (
	defaultRouteGroup = "default"

	apolloConfigSep = "."
)

// MQ
const (
	ApolloMQBrokersSep = ","

	apolloMQBrokersKey        = "brokers"
	apolloMQOffsetAtKey       = "offsetat"
	apolloMQTTRKey            = "ttr"
	apolloMQTTLKey            = "ttl"
	apolloMQTriesKey          = "tries"
	apolloMQBatchSizeKey      = "batchsize"
	apolloMQBatchKBSizeKey    = "batchkbsize"
	apolloMQBatchTimeoutMsKey = "batchtimeoutms"
	apolloMQRequestIntervalMS = "interval"
	apolloTimeoutMsKey        = "timeoutms" // 目前用于writer write timeout
	apolloMQAsyncKey          = "async"
	// 订阅类型(for pulsar)
	apolloMQSubscribeTypeKey = "subscribetype"
	apolloMQMaxBytesKey      = "maxbytes"

	defaultMQTimeout = 3000
	defaultMQTTR     = 3600      // 1 hour
	defaultMQTTL     = 3600 * 24 // 1 day
	defaultMQTries   = 1
	//默认1000毫秒
	defaultMQBatchTimeoutMs = 1000
	defaultMQBatchSize      = 1
	defaultMQBatchKBSize    = 128
	defaultMQInterval       = 300
	defaultMQMaxBytes       = 10e6
	defaultMQSendTimeoutMS  = 3000

	// 默认订阅类型(for pulsar)
	defaultMQSubscribeType = 2

	LastOffset  int64 = -1 // The most recent offset available for a partition.
	FirstOffset       = -2 // The least recent offset available for a partition.
)

// cache
const (
	apolloCacheConfigKeyAddr       = "addr"
	apolloCacheConfigKeyPoolSize   = "poolsize"
	apolloCacheConfigKeyTimeout    = "timeout"
	apolloCacheConfigKeyUseWrapper = "usewrapper"

	defaultCachePoolSize          = 128
	defaultCacheTimeoutNumSeconds = 3
	defaultCacheUseWrapper        = true
)
