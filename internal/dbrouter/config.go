package dbrouter

import (
	"context"
)

// Config ...
type Config struct {
	DBName   string
	DBType   string
	DBAddr   []string
	UserName string
	PassWord string
}

// ChangeIns ...
type ChangeIns struct {
	insNames []string
}

// ConfigChange 配置变更
type ConfigChange struct {
	dbInstanceChange map[string][]string
	dbGroups         []string
}

//Configer 配置接口抽象
type Configer interface {
	GetInstanceName(ctx context.Context, cluster, table string) string
	GetInstanceConfig(ctx context.Context, instance, group string) *Config
	GetAllGroups(ctx context.Context) []string
}

// SendInsChange ...
func SendInsChange(insMap map[string]bool, ch chan ChangeIns) {
	var insNames []string
	for k := range insMap {
		insNames = append(insNames, k)
	}
	if len(insNames) > 0 {
		ch <- ChangeIns{insNames}
	}
}
