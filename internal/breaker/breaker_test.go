package breaker

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestEntry(t *testing.T) {
	testChan := make(chan bool)
	go func() {
		for {
			if Entry("group", "test") {
				testChan <- true
			}
		}
	}()

	go func() {
		for i := 0; i < 20; i++ {
			StatBreaker("group", "test", errors.New("timeout"))
		}
	}()
	success := <-testChan
	assert.True(t, success)
}
