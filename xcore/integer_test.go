package xcore

import (
	"context"
	"testing"
)

func TestBase10ToX(t *testing.T) {
	type args struct {
		ctx context.Context
		v   int64
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "test1",
			args: args{
				ctx: ctx,
				v:   77511809598636032,
			},
			want:    "",
			wantErr: false,
		},
		{
			name: "test2",
			args: args{
				ctx: ctx,
				v:   75657980380856320,
			},
			want:    "",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Base10To62(tt.args.ctx, tt.args.v)
			if (err != nil) != tt.wantErr {
				t.Errorf("Base10ToX() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Base10ToX() got = %v, want %v", got, tt.want)
			}
		})
	}
}
