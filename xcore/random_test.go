package xcore

import (
	"fmt"
	"testing"
)

func Test_WeightRandom(t *testing.T) {
	var elements = []*RandomElement{
		{"20", 20},
		{"10", 10},
		{"30", 30},
	}
	var cmap = make(map[string]int)
	for i := 0; i < 6000; i++ {
		e := WeightRandom(ctx, elements)
		var key = "e"
		if e != nil {
			key = e.Key
		}
		if _, ok := cmap[key]; ok {
			cmap[key]++
		} else {
			cmap[key] = 0
		}
	}
	fmt.Println(cmap)
}
