package xcore

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"gitee.com/xfrm/middleware/xlog"
	"gopkg.in/yaml.v2"
	"strconv"
	"strings"
	"sync"
	"testing"
	"time"
)

type Msg struct {
	Id   int64  `json:"id"`
	Name string `json:"name"`
}

var ctx = context.Background()

func Test_Split(t *testing.T) {
	//type TCASES struct {
	//	input string
	//	v int64
	//	expect bool
	//}
	//
	//tcases := []TCASES{
	//	{"2,3,-2,4",true},
	//}
	//
	//for _, tcase := range tcases {
	//
	//	input := tcase.input
	//	expect := tcase.expect
	//
	//	fmt.Printf("%-10v \t %-10v \t %-10v \t %-10v\n",fmt.Sprintf("%v",input),output,expect,expect==output)
	//}
	fmt.Println(InWhiteList(ctx, "1,2,3,4", ",", 4))
	fmt.Println(InWhiteList(ctx, "1,2,3,4", ",", 5))
	fmt.Println(SplitToInt64(ctx, "1,2,3,4,2", ",", true))
	a := `2147500273
21221554
21221591
2147500273`
	fmt.Println(SplitToInt64(ctx, a, "\n", true))
	//fmt.Println(ParseFullPhone(ctx, "+86-1234-123-1234"))
	//fmt.Println(ParsePhone(ctx, "234 123 1234"))
	//fmt.Println(FormatPhoneForCall(ctx, "+86-1234-123-1234"))
	var ii = 0
	i, e := Try(ctx, 4, []int{200, 500, 1000}, func(ctx context.Context, ctimes int) error {
		fmt.Println("try==", ctimes, ii)
		if ii == 1 {
		}
		ii++
		return fmt.Errorf("error:%d", ii)
	})
	fmt.Println("try:", i, e)
}

func Test_SplitFile(t *testing.T) {
	fun := "Test_SplitFile -->"

	unit_locals := map[string]string{
		"case local file win line break": "./2222.txt",
		"case local file mac line break": "./mac-break-line.txt",
		"case local file , line break":   "./comma.txt",
	}

	for k, v := range unit_locals {
		r, err := ReadAndSplitFormattedLocalFile(ctx, v, true)
		xlog.Infof(ctx, "%s case %s, req:%s, res:%+v, len:%d, err:%+v", fun, k, v, r, len(r), err)
	}

	unit_nets := map[string]string{
		"case net file win line break": "http://known.xin/Downloads/2222.txt",
		"case net file mac line break": "http://known.xin/mac-break-line.txt",
		"case net file , line break":   "http://known.xin/comma.txt",
	}

	for k, v := range unit_nets {
		r, err := ReadAndSplitFormattedNetFile(ctx, v, true, 60)
		xlog.Infof(ctx, "%s case %s, req:%s, res:%+v, len:%d, err:%+v", fun, k, v, r, len(r), err)
	}
}
func Test_ReadAndSplitFormattedNetFileTimeout(t *testing.T) {
	fun := "Test_ReadAndSplitFormattedNetFileTimeout -->"

	unit_nets := map[string]string{
		"case net file win line break": "https://qnyb00.cdn.ipalfish.com/0/img/e4/ed/2d0f25172058f914bbbdd5243bab?Expires=1604832419&OSSAccessKeyId=LTAI4GKMd8rCpCU7yEdss2yS&Signature=Z6u97uDcdFA2Ycs6%2FZHq2Ul%2Fum4%3D",
	}

	for k, v := range unit_nets {
		r, err := ReadAndSplitFormattedNetFileTimeout(ctx, v, true, 5*time.Minute)
		xlog.Infof(ctx, "%s case %s, req:%s, res:%+v, len:%d, err:%+v", fun, k, v, r, len(r), err)
	}
}

func Test_Excel(t *testing.T) {
	fun := "Test_Excel"

	file := "work/phonetest.xlsx"
	data1, err1 := ReadLocalExcel(ctx, file, "Sheet1", false)
	data2, err2 := ReadLocalExcel(ctx, file, "Sheet1", true)

	xlog.Infof(ctx, "%s data1:%+v, data2:%+v, err1:%+v, err2:+%v", fun, data1, data2, err1, err2)
}

func Test_ExcelNet(t *testing.T) {
	fun := "Test_ExcelNet"

	fileUrl := "http://known.xin/phonetest.xlsx"
	data1, err1 := ReadNetExcel(ctx, fileUrl, "Sheet1", false, 30)
	data2, err2 := ReadNetExcel(ctx, fileUrl, "Sheet1", true, 30)

	xlog.Infof(ctx, "%s data1:%+v, data2:%+v, err1:%+v, err2:+%v", fun, data1, data2, err1, err2)
}

type S1 struct {
	A string
}

func (s *S1) String1() string {
	return s.A
}

type S2 struct {
	A string
}

func (s *S2) String1() string {
	return s.A
}

type Stringer1 interface {
	String1() string
}

func TestIntereface(t *testing.T) {
	s1 := []*S1{{"a"}, {"b"}}
	s2 := []*S2{{"c"}, {"d"}}
	ss1, err := ToInterfaceSlice(ctx, s1)
	if err != nil {
		fmt.Println("err1", err)
		return
	}
	ss2, err := ToInterfaceSlice(ctx, s2)
	if err != nil {
		fmt.Println("err2", err)
		return
	}
	iterateStringer1(ss1)
	fmt.Println("=======")
	iterateStringer1(ss2)
	ToInterfaceSliceNilWhenError(ctx, &S1{"a"})
}
func TestTime(t *testing.T) {
	t1 := time.Date(2018, 1, 9, 0, 0, 1, 100, time.Local)
	t2 := time.Date(2018, 1, 10, 0, 0, 1, 100, time.Local)
	//t2 := time.Date(2018, 1, 9, 23, 59, 22, 100, time.Local)
	fmt.Println(DaysInterval(t1, t2))
}
func iterateStringer1(arr []interface{}) {
	for i, v := range arr {
		v1, ok := v.(Stringer1)
		fmt.Println(i, ":", ok, "=", v1)
	}
}

func Test_slicejoin(t *testing.T) {
	s := []int64{1, 2, 3, 4, 5}
	fmt.Println(JoinInt64Slice(ctx, s, ";"))
	s1 := []int{1, 2, 3, 4, 5}
	fmt.Println(JoinIntSlice(ctx, s1, ";"))
	s2 := []string{"1", "2", "", "a", "4"}
	fmt.Println(StrSliceToInt64Slice(ctx, s2))
}
func Test_phone(t *testing.T) {
	var pairs = []struct {
		o string
		f string
		c string
	}{
		{"13111111111", "86-13111111111", "13111111111"},
		{"131 1111 1111", "86-13111111111", "13111111111"},
		{"86-13111111111", "86-13111111111", "13111111111"},
		{"60-13111111111", "60-13111111111", "006013111111111"},
		{"886-13111111111", "886-13111111111", "0088613111111111"},
		{"886-013111111111", "886-013111111111", "0088613111111111"},
		{"852-13111111111", "852-13111111111", "0085213111111111"},
		{"853-13111111111", "853-13111111111", "0085313111111111"},
	}
	for _, v := range pairs {
		wp := WorldPhoneFmt(ctx, v.o)
		if wp != v.f {
			t.Errorf("%s  parsefull error: %s", v.o, wp)
		}
		fc := FormatPhoneForCall(ctx, v.o, "00", true)
		if fc != v.c {
			t.Errorf("%s  parsecall error:%s", v.o, fc)
		}
	}
}

func Test_Encode(t *testing.T) {
	salt := "1234567890123456"
	iv := "1234567890123456"
	encode := Encode(ctx, "86-72118833058", salt, iv)
	t.Log(encode)
	code := Decode(ctx, encode, salt, iv)
	t.Log(code)
}

func Test_ReadLineV2(t *testing.T) {
	lines := ReadLinesV2("file_utils.go", false)
	fmt.Println(strings.Join(lines, "\n"))
}

func Test_checkPhone(t *testing.T) {
	phone := ""
	phone = "+1-18833058989"
	if !CheckPhone(phone) {
		t.Log(phone)
	}
}

func Test_HasArrayElementMatch(t *testing.T) {
	hit, ok := HasArrayElement("中国航天工程Hello", []string{"hello", "中国", "航天", "工程"}, true)
	fmt.Printf("hit:%s, ok:%t\n", hit, ok)
}
func Test_Num(t *testing.T) {
	fmt.Println(strconv.ParseFloat("123123123", 64))
}
func Test_Slice(t *testing.T) {
	var uids = []int64{1, 2, 3}
	fmt.Println("aaa:", uids[:len(uids)])
	fmt.Println("aaaa:", uids[:len(uids)])
	fmt.Println("bbb:", uids[len(uids):])
	fmt.Println("bbbb:", uids[len(uids):])
}
func Test_TrimLeftPhone(t *testing.T) {
	area, phone := TrimLeft0PhoneNum("0123123")
	fmt.Println("aaa:", area, "-", phone)
	area, phone = TrimLeft0PhoneNum("00123123")
	fmt.Println("aaa:", area, "-", phone)
	area, phone = TrimLeft0PhoneNum("1-00123123")
	fmt.Println("aaa:", area, "-", phone)
	area, phone = TrimLeft0PhoneNum("1-0123123")
	fmt.Println("aaa:", area, "-", phone)
	area, phone = TrimLeft0PhoneNum("01-123123")
	fmt.Println("aaa:", area, "-", phone)
	area, phone = TrimLeft0PhoneNum("1-123123")
	fmt.Println("aaa:", area, "-", phone)
}
func Test_SplictTrimLeftPhone(t *testing.T) {
	area, phone := SplitTrimLeft0PhoneNum("0123123", true)
	fmt.Println("aaa:", area, "-", phone)
	area, phone = SplitTrimLeft0PhoneNum("00123123", false)
	fmt.Println("aaa:", area, "-", phone)
	area, phone = SplitTrimLeft0PhoneNum("1-00123123", true)
	fmt.Println("aaa:", area, "-", phone)
	area, phone = SplitTrimLeft0PhoneNum("1-0123123", true)
	fmt.Println("aaa:", area, "-", phone)
	area, phone = SplitTrimLeft0PhoneNum("01-123123", true)
	fmt.Println("aaa:", area, "-", phone)
	area, phone = SplitTrimLeft0PhoneNum("1-123123", true)
	fmt.Println("aaa:", area, "-", phone)
}

func Test_TrimLeft0PhoneNumFormat(t *testing.T) {
	var pairs = []struct {
		a string
		b bool
	}{
		{"0123123", true},
		{"00123123", false},
		{"1-00123123", true},
		{"1-0123123", true},
		{"01-123123", true},
		{"1-123123", true},
		{"1-01231230", true},
	}
	for i, p := range pairs {
		phone := TrimLeft0PhoneNumFormat(p.a, p.b)
		fmt.Println(i, p.a, p.b, "==>", phone)
	}
}
func Test_Loop(t *testing.T) {
	var pairs = []struct {
		a string
		b bool
	}{
		{"0123123", true},
		{"00123123", false},
		{"1-00123123", true},
		{"1-0123123", true},
		{"01-123123", true},
		{"1-123123", true},
		{"1-01231230", true},
	}
	max, m, err := LoopPage(ctx, 4, 0, 2, func(ctx2 context.Context, i int64, i2 int64) (int64, bool, error) {
		fmt.Println(i, ":", i2, "===", pairs[i:i+i2])
		return i + i2, true, nil
	})
	fmt.Println(max, "=========", m, ", errr ", err)
}
func Test_Age(t *testing.T) {

	for _, birth := range []string{
		"2021-09-14",
		"2021-09-13",
		"2021-09-15",
		"2023-09-14",
		"2022-09-14",
		"2022-09-13",
		"2022-09-15",
		"2022-08-14",
		"2022-10-14",
		"2021-08-14",
		"2021-10-14",
		"2023-08-14",
		"2023-10-14",
		"2020-08-14",
		"2020-10-14",
		"1968-10-14",
	} {
		birthTime, _ := time.ParseInLocation("2006-01-02", birth, time.Local)
		fmt.Println(birth, "==", Age(ctx, birthTime.Unix()))
	}

	tt, _ := time.ParseInLocation("2006-01-02", "1969-01-01", time.Local)
	fmt.Println(tt.Unix())

}
func Test_Timerange(t *testing.T) {
	for _, birth := range []string{
		"00:00:00~02:00:00;04:00:00~05:00:00",
		"23:00:00~08:00:00;",
	} {
		in, err := InTimeRange(ctx, birth, HhMmSs{9, 0, 0})
		fmt.Println(birth, " in:  ", in, ", err: ", err)
	}
}
func Test_Hit(t *testing.T) {
	fmt.Println(IsHitRateInt(ctx, 2, 10, 11111111))
	fmt.Println(IsHitRateInt(ctx, 2, 10, 11111109))
	fmt.Println(IsHitRateInt(ctx, 2, 10, 11111190))
	fmt.Println(IsHitRateInt(ctx, 3, 10, 11111109))
}
func Test_Base(t *testing.T) {
	var strs []string
	for _, v := range []int64{
		-1, 0, 1, 2, 63, 64, 100000, pow(62, 3), 3000000000,
	} {
		b, err := Base10To62(ctx, v)
		strs = append(strs, b)
		fmt.Println(v, "=>", b, ", err:", err)
	}
	for _, s := range strs {
		if len(s) > 0 {
			v, err := Base62To10(ctx, s)
			fmt.Println(s, "=>", v, ", err:", err)
		}
	}
}
func Test_RandStr(t *testing.T) {

	fmt.Println(RandomShuffling(ctx, chars))
}
func Test_IntervalExecute(t *testing.T) {
	tt, b := ctx.Deadline()
	fmt.Println("tt: ", tt, ", b:  ", b)
	ctx, ctxf := context.WithTimeout(ctx, time.Second*15)
	tt, b = ctx.Deadline()
	fmt.Println("tt: ", tt, ", b:  ", b, ", fu:", ctxf)
	var i int
	go TickExecute(ctx, time.Second, false, func(ctx context.Context) error {
		i++
		if i >= 10 {
			return fmt.Errorf("error: %d", i)
		}
		return nil
	})
	var j int
	go TickExecute(ctx, time.Second, true, func(ctx context.Context) error {
		j++
		if j >= 10 {
			return fmt.Errorf("error1: %d", j)
		}
		return nil
	})
	var c int
	for {
		c++
		time.Sleep(time.Second)
		if c > 30 {
			return
		}
	}
}
func Test_Ctx(t *testing.T) {
	var ch = make(chan struct{})
	go func() {
		select {
		case v, ok := <-ch:
			fmt.Println("go1  ", v, ", ok: ", ok)
		}
	}()
	go func() {
		select {
		case v, ok := <-ch:
			fmt.Println("go2  ", v, ", ok: ", ok)
		}
	}()
	close(ch)
	//ch<- struct{}{}
	time.Sleep(time.Second)
}
func Test_tick(t *testing.T) {
	ticker := time.Tick(time.Second)
	var i int
	for range ticker {
		i++
		fmt.Println("ticker: ", i)
	}
}
func Test_Inteval(t *testing.T) {
	var i int
	IntervalExecute(ctx, func(ctx2 context.Context) time.Duration {
		if i >= 3 && i <= 6 {
			return 2 * time.Second
		}
		return time.Second
	}, false, func(ctx context.Context) error {
		i++
		fmt.Println(time.Now().Format("2006-01-02 15:04:05"), i)
		if i%7 == 0 {
			return fmt.Errorf("error: %d", i)
		}
		return nil
	})
}
func def() {
	var i int
	for {
		defer func() {
			fmt.Println("defer")
		}()
		i++
		fmt.Println(i)
	}
}
func Test_defer(t *testing.T) {
	def()
}
func makeerr() (i int, err error) {
	return 1, fmt.Errorf("1111111")
}
func checkerr() (err error) {
	i, err := makeerr()
	fmt.Println(i)
	return
}
func Test_err(t *testing.T) {
	err := checkerr()
	fmt.Println("error: ", err)
}
func Test_mpa(t *testing.T) {
	where := map[string]interface{}{}
	where["account in"] = 1
	//where["_forceindex"] = "idx_account_bussid"
	if idx := where["_forceindex"]; idx == nil {
		fmt.Println("indexxxxxxxxx")
	}
}

type CheckLock struct {
	mu sync.RWMutex
	a  int
}

func (m *CheckLock) m1() {
	m.mu.RLock()
	defer m.mu.RUnlock()
	fmt.Println("==m1==")
	time.Sleep(time.Second)
	m.m2()
	return
}
func (m *CheckLock) m2() {
	m.mu.RLock()
	defer m.mu.RUnlock()
	fmt.Println("==m2==")
	time.Sleep(time.Second)
	return
}
func (m *CheckLock) lck() {
	m.mu.Lock()
	defer m.mu.Unlock()
	fmt.Println("==lck==")
	return
}

func Test_Lock(t *testing.T) {
	var c = &CheckLock{}
	go func() {
		c.m1()
	}()
	time.Sleep(20 * time.Millisecond)
	go func() {
		c.lck()
	}()

	time.Sleep(30 * time.Second)

}

var ydata = `
  cluster: 
    ABEXPERIMENT: 
      -
        instance: t_ins_abexperiment
        match: regex
        express: abexperiment_.*
  instances: 
    t_ins_abexperiment: 
      cluster: ABEXPERIMENT
      dbtype: mysql
      dbname: abexperiment
      dbcfg: 
        addrs: ["10.111.131.208:4000"]
        passwd: dddddd
        user: product
`

type ClusterconfYaml struct {
	ClusterMap   map[string][]ClusterinfYaml `yaml:"cluster", json:"cluster"`
	InstancesMap map[string]DbinsYaml        `yaml:"instances", json:"instances"`
}
type ClusterinfYaml struct {
	Instance string `yaml:"instance", json:"instance"`
	Match    string `yaml:"match", json:"match"`
	Express  string `yaml:"express", json:""`
}
type DbcfgYaml struct {
	Addrs  []string `yaml:"addrs", json:"addrs"`
	User   string   `yaml:"user", json:"user"`
	Passwd string   `yaml:"passwd", json:"passwd"`
}
type DbinsYaml struct {
	Cluster string    `yaml:"cluster", json:"cluster"`
	Dbtype  string    `yaml:"dbtype", json:"dbtype"`
	Dbname  string    `yaml:"dbname", json:"dbname"`
	Dbcfg   DbcfgYaml `yaml:"dbcfg", json:"dbcfg"`
}

func Test_Yaml(t *testing.T) {
	d := yaml.NewDecoder(bytes.NewReader([]byte(ydata)))
	var clsConf ClusterconfYaml
	err := d.Decode(&clsConf)
	fmt.Println("err: ", err)
	dd, _ := json.Marshal(clsConf)
	fmt.Println(string(dd))
}
