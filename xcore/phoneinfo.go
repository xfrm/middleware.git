package xcore

import (
	"fmt"
	"github.com/golang/protobuf/proto"
	"math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type PhoneInfo struct {
	Version              int32    `protobuf:"varint,1,opt,name=version,proto3" json:"version,omitempty"`
	Nonce                string   `protobuf:"bytes,2,opt,name=nonce,proto3" json:"nonce,omitempty"`
	Sign                 []byte   `protobuf:"bytes,3,opt,name=sign,proto3" json:"sign,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *PhoneInfo) Reset()         { *m = PhoneInfo{} }
func (m *PhoneInfo) String() string { return proto.CompactTextString(m) }
func (*PhoneInfo) ProtoMessage()    {}
func (*PhoneInfo) Descriptor() ([]byte, []int) {
	return fileDescriptor_c0e585c54dbe5137, []int{0}
}

func (m *PhoneInfo) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_PhoneInfo.Unmarshal(m, b)
}
func (m *PhoneInfo) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_PhoneInfo.Marshal(b, m, deterministic)
}
func (m *PhoneInfo) XXX_Merge(src proto.Message) {
	xxx_messageInfo_PhoneInfo.Merge(m, src)
}
func (m *PhoneInfo) XXX_Size() int {
	return xxx_messageInfo_PhoneInfo.Size(m)
}
func (m *PhoneInfo) XXX_DiscardUnknown() {
	xxx_messageInfo_PhoneInfo.DiscardUnknown(m)
}

var xxx_messageInfo_PhoneInfo proto.InternalMessageInfo

func (m *PhoneInfo) GetVersion() int32 {
	if m != nil {
		return m.Version
	}
	return 0
}

func (m *PhoneInfo) GetNonce() string {
	if m != nil {
		return m.Nonce
	}
	return ""
}

func (m *PhoneInfo) GetSign() []byte {
	if m != nil {
		return m.Sign
	}
	return nil
}

func init() {
	proto.RegisterType((*PhoneInfo)(nil), "logic.PhoneInfo")
}

func init() { proto.RegisterFile("phone.proto", fileDescriptor_c0e585c54dbe5137) }

var fileDescriptor_c0e585c54dbe5137 = []byte{
	// 114 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0xe2, 0x2e, 0xc8, 0xc8, 0xcf,
	0x4b, 0xd5, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0x62, 0xcd, 0xc9, 0x4f, 0xcf, 0x4c, 0x56, 0xf2,
	0xe7, 0xe2, 0x0c, 0x00, 0x89, 0x7a, 0xe6, 0xa5, 0xe5, 0x0b, 0x49, 0x70, 0xb1, 0x97, 0xa5, 0x16,
	0x15, 0x67, 0xe6, 0xe7, 0x49, 0x30, 0x2a, 0x30, 0x6a, 0xb0, 0x06, 0xc1, 0xb8, 0x42, 0x22, 0x5c,
	0xac, 0x79, 0xf9, 0x79, 0xc9, 0xa9, 0x12, 0x4c, 0x0a, 0x8c, 0x1a, 0x9c, 0x41, 0x10, 0x8e, 0x90,
	0x10, 0x17, 0x4b, 0x71, 0x66, 0x7a, 0x9e, 0x04, 0xb3, 0x02, 0xa3, 0x06, 0x4f, 0x10, 0x98, 0x9d,
	0xc4, 0x06, 0x36, 0xde, 0x18, 0x10, 0x00, 0x00, 0xff, 0xff, 0xa9, 0x5e, 0x57, 0xdd, 0x6d, 0x00,
	0x00, 0x00,
}
