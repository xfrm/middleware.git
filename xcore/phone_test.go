package xcore

import (
	"context"
	"testing"
)

func TestDecode(t *testing.T) {
	type args struct {
		ctx    context.Context
		encode string
		salt   string
		iv     string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "test1",
			args: args{
				ctx:    ctx,
				encode: "CAESCDEyMzQ1Njc4GhATSwRayoxFT3N+raaWqpG5",
				salt:   "BpLnfgDsc2WD8F2q",
				iv:     "NfHK5a84jjJkwzDk",
			},
			want: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Decode(tt.args.ctx, tt.args.encode, tt.args.salt, tt.args.iv); got != tt.want {
				t.Errorf("Decode() = %v, want %v", got, tt.want)
			}
		})
	}
}
