package xcore

import (
	"context"
	"gitee.com/xfrm/middleware/xtrace"
)

func RunWithNewTrace(operationName string, execute func(ctx context.Context)) {
	span, ctx := xtrace.StartSpanFromContext(context.Background(), operationName)
	defer span.Finish()
	execute(ctx)

}
