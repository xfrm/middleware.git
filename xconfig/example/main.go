package main

import (
	"context"
	"fmt"

	"gitee.com/xfrm/middleware/xconfig"
)

func main() {
	ctx := context.TODO()
	//Step1 : 构造实例
	configCenter, err := xconfig.NewConfigCenter(ctx, xconfig.ConfigTypeApollo, "base/test", []string{"application", "test"})
	if err != nil {
		fmt.Printf("new configer center err:%s", err.Error())
		return
	}

	//Step2 : 监听变更（改步骤可以省略，如果不需要动态感知变更事件）
	observer := xconfig.NewConfigObserver(func(ctx context.Context, event *xconfig.ChangeEvent) {
		// Todo 处理变更事件
	})
	configCenter.RegisterObserver(ctx, observer)

	//Step3 : 获取配置Value
	configCenter.GetBool(ctx, "key_bool")
	configCenter.GetBoolWithNamespace(ctx, "test", "key_bool_2")
}
