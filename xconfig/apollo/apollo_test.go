package apollo

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"testing"
	"time"

	"gitee.com/xfrm/middleware/xconfig"
	"gitee.com/xfrm/middleware/xconfig/apollo/mockserver"
	"gitee.com/xfrm/middleware/xlog"
	"github.com/stretchr/testify/assert"
)

const (
	cluster  = "default"
	cacheDir = "/tmp/test"
	ipAddr   = ":8888"
)

var center xconfig.ConfigCenter

func TestMain(m *testing.M) {
	setup()

	var err error
	driver := &apolloDriver{}
	namespaces := []string{"application", "test"}
	center, err = driver.New(context.TODO(), "base/test", namespaces, SetCluster(cluster), SetCacheDir(cacheDir), SetIPHost(ipAddr))
	if err != nil {
		xlog.Errorf(context.Background(), "create config center failed. error: %+v", err)
		os.Exit(1)
	}

	code := m.Run()
	teardown()
	os.Exit(code)
}

func setup() {
	go func() {
		if err := mockserver.Run(); err != http.ErrServerClosed {
			xlog.Fatalf(context.Background(), "mockserver.Run() returned error: %+v", err)
		}
	}()
	// wait for mock server to run
	time.Sleep(time.Millisecond * 500)
	mockserver.Set("application", "test_key_1", "test_value_1")
	mockserver.Set("application", "test_key_2", "true")
	mockserver.Set("application", "test_key_3", "1")
	mockserver.Set("application", "test_int_slice_key[0]", "1")

	mockserver.Set("test", "test_key_4", "test_value_2")
	mockserver.Set("test", "test_key_5", "false")
	mockserver.Set("test", "test_key_6", "2")
}

func teardown() {
	mockserver.Close()
}

func TestNewApolloConfigCenter(t *testing.T) {
	ass := assert.New(t)
	ass.NotNil(center)
}

func TestNewApolloConfigCenterDefault(t *testing.T) {
	ass := assert.New(t)
	driver := &apolloDriver{}
	namespaces := []string{"application", "test"}
	center, err := driver.New(context.TODO(), "base/test", namespaces)
	ass.NotNil(center)
	ass.Nil(err)
}

func TestRegisterObserver(t *testing.T) {
	ass := assert.New(t)
	handle := func(ctx context.Context, event *xconfig.ChangeEvent) {
		ass.NotNil(event)
		ass.Equal(xconfig.Apollo, event.Source)
		ass.Equal("test", event.Namespace)
		ass.Equal(1, len(event.Changes))
		ass.Equal("test_value", event.Changes["test_key"].NewValue)
		ass.Equal("", event.Changes["test_key"].OldValue)
		ass.Equal(xconfig.ADD, event.Changes["test_key"].ChangeType)
	}

	ob := xconfig.NewConfigObserver(handle)
	ctx, cancel := context.WithCancel(context.TODO())
	center.RegisterObserver(ctx, ob)
	mockserver.Set("test", "test_key", "test_value")
	time.Sleep(4 * time.Second)
	cancel()
}

func TestSubscribeNamespaces(t *testing.T) {
	ass := assert.New(t)
	err := center.SubscribeNamespaces(context.TODO(), []string{"test2"})
	ass.Nil(err)
}

func TestApolloConfigCenter_GetAllKeys(t *testing.T) {
	ass := assert.New(t)
	time.Sleep(4 * time.Second)
	keys := center.GetAllKeys(context.TODO())
	fmt.Printf("keys:%+v\n", keys)
	ass.Contains(keys, "test_key_1")
	keys2 := center.GetAllKeysWithNamespace(context.TODO(), "test")
	fmt.Printf("keys2:%+v\n", keys2)
	ass.Contains(keys2, "test_key_4")
}

func TestUnmarshalWithNamespace(t *testing.T) {
	ass := assert.New(t)
	type a struct {
		test_key_1 string
	}
	var m = &a{}
	center.Unmarshal(context.TODO(), &m)
	fmt.Printf("unmarshal map1:%+v\n", m)
	ass.NotNil(m)
	//var m2 = map[string]string{}
	//center.Unmarshal(context.TODO(), &m2)
	//fmt.Printf("unmarshal map2:%+v\n", m2)
	//ass.NotNil(m2)
}

func TestGetString(t *testing.T) {
	ass := assert.New(t)
	val1, b1 := center.GetString(context.TODO(), "test_key_1")
	ass.Equal(true, b1)
	ass.Equal("test_value_1", val1)
	val2, b2 := center.GetStringWithNamespace(context.TODO(), "test", "test_key_4")
	ass.Equal(true, b2)
	ass.Equal("test_value_2", val2)
}

func TestGetInt(t *testing.T) {
	ass := assert.New(t)
	val1, b1 := center.GetInt(context.TODO(), "test_key_3")
	ass.Equal(true, b1)
	ass.Equal(1, val1)
	val2, b2 := center.GetIntWithNamespace(context.TODO(), "test", "test_key_6")
	ass.Equal(true, b2)
	ass.Equal(2, val2)
}

func TestGetBool(t *testing.T) {
	ass := assert.New(t)
	val1, b1 := center.GetBool(context.TODO(), "test_key_2")
	ass.Equal(true, b1)
	ass.Equal(true, val1)
	val2, b2 := center.GetBoolWithNamespace(context.TODO(), "test", "test_key_5")
	ass.Equal(true, b2)
	ass.Equal(false, val2)
}

func TestGetIntSlice(t *testing.T) {
	ass := assert.New(t)
	al1, b1 := center.GetIntSlice(context.TODO(), "test_int_slice_key")
	ass.Equal(true, b1)
	ass.Equal(1, al1[0])

	al2, b2 := center.GetInt(context.TODO(), "test_int_slice_key[0]")
	ass.Equal(true, b2)
	ass.Equal(1, al2)
}
