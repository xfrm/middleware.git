package xconfig

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConfErr(t *testing.T) {
	cfg0 := []byte(
		`
a
`)
	tf := NewTierConf()
	err := tf.Load(cfg0)
	assert.NotNil(t, err)
}

func TestConfNormal(t *testing.T) {
	cfg0 := []byte(
		`
[log]
LogDir =
LogLevel = TRACE
ConnPort = 00
[empty]
`)

	tf := NewTierConf()
	err := tf.Load(cfg0)
	assert.Nil(t, err)
	_, err = tf.StringCheck()
	assert.Nil(t, err)
}

func TestConfMultiLayer(t *testing.T) {
	cfg0 := []byte(
		`
[log]
LogDir =
LogLevel = TRACE
ConnPort = 00
[empty]
`)

	tf := NewTierConf()
	err := tf.Load(cfg0)
	assert.Nil(t, err)

	cfg1 := []byte(
		`
[link]
ConnPort = 9988
HeartIntv = 300


[log]
LogDir = ./lua
LogLevel = TRACE
`)

	err = tf.Load(cfg1)
	assert.Nil(t, err)

	t.Log(tf.GetConf())

}

func TestConfVar(t *testing.T) {
	cfg0 := []byte(
		`
[log]
LogDir = test
LogLevel = TRACE
ConnPort = 00
[empty]

[lvar]
hh = lvar/${log.ConnPort}

h0 = h0/${lvar.h1}
h1 = h1/${lvar.h2}
h2 = h2/${lvar.h3}
h3 = h3/${lvar.h4}
h4 = h4/${lvar.h0}

[tvar]
cc = abc
c0 = a/${log.LogDir}
c1 = ${log.LogLevel}
c2 = ${  log.LogLevel }/cc
c3 = ${  log.LogLevel}/bb
c4 = ${  log.  LogLevel}
c5 = ${  log  .  LogLevel  }/d
c6 = ${  log  .  LogLevel  }/a/c/${log.LogLevel}/${log.ConnPort}/dd
c7 = ${  log  .  LogLevel  }/a/c/d/${lvar.hh}/${log.ConnPort}
c8 = ${  log  .  LogLevel  }/a/c/d/${lvar.hh}/${log.ConnPort}/a
c9 = ${log.notexist}/a/c/d/${lvar.hh}/${log.ConnPort}/a
c10 = ${log}/a/c/d/${lvar.hh}/${log.ConnPort}/a
c11 = ${ log.notexist.dd}/a/c/d/${lvar.hh}/${log.ConnPort}/a

c12 = ${log.LogLevel}/${tvar.c12}


c13 = aa/c/${lvar.h0}
`)

	tf := NewTierConf()

	err := tf.Load(cfg0)
	assert.Nil(t, err)

	p, v := "c0", "a/test"
	c, err := tf.ToString("tvar", p)
	assert.Nil(t, err)
	assert.Equal(t, v, c)

	p, v = "c1", "TRACE"
	c, err = tf.ToString("tvar", p)
	assert.Nil(t, err)
	assert.Equal(t, v, c)

	p, v = "cc", "abc"
	c, err = tf.ToString("tvar", p)
	assert.Nil(t, err)
	assert.Equal(t, v, c)

	p, v = "c2", "TRACE/cc"
	c, err = tf.ToString("tvar", p)
	assert.Nil(t, err)
	assert.Equal(t, v, c)

	p, v = "c3", "TRACE/bb"
	c, err = tf.ToString("tvar", p)
	assert.Nil(t, err)
	assert.Equal(t, v, c)

	p, v = "c4", "TRACE"
	c, err = tf.ToString("tvar", p)
	assert.Nil(t, err)
	assert.Equal(t, v, c)

	p, v = "c5", "TRACE/d"
	c, err = tf.ToString("tvar", p)
	assert.Nil(t, err)
	assert.Equal(t, v, c)

	p, v = "c6", "TRACE/a/c/TRACE/00/dd"
	c, err = tf.ToString("tvar", p)
	assert.Nil(t, err)
	assert.Equal(t, v, c)

	p, v = "c7", "TRACE/a/c/d/lvar/00/00"
	c, err = tf.ToString("tvar", p)
	assert.Nil(t, err)
	assert.Equal(t, v, c)

	p, v = "c8", "TRACE/a/c/d/lvar/00/00/a"
	c, err = tf.ToString("tvar", p)
	assert.Nil(t, err)
	assert.Equal(t, v, c)

	p, v = "c9", "${log.notexist}/a/c/d/lvar/00/00/a"
	c, err = tf.ToString("tvar", p)
	assert.Nil(t, err)
	assert.Equal(t, v, c)

	p, v = "c10", "${log}/a/c/d/lvar/00/00/a"
	c, err = tf.ToString("tvar", p)
	assert.Nil(t, err)
	assert.Equal(t, v, c)

	p, v = "c11", "${ log.notexist.dd}/a/c/d/lvar/00/00/a"
	c, err = tf.ToString("tvar", p)
	assert.Nil(t, err)
	assert.Equal(t, v, c)

	c, err = tf.ToString("tvar", "c12")
	assert.NotNil(t, err)
	assert.Equal(t, "cyclic reference:${tvar.c12}", err.Error())

	p, v = "c12", "ddddd"
	c = tf.ToStringWithDefault("tvar", p, "ddddd")
	assert.Equal(t, v, c)

	_, err = tf.ToString("tvar", "c13")
	assert.NotNil(t, err)
	assert.Equal(t, "cyclic reference:${lvar.h0}", err.Error())
}

func TestToInt(t *testing.T) {
	ef := NewTierConf()
	c := make(map[string]map[string]string)
	pv := map[string]string{
		"p0": "v0",
		"p1": "v1",
		"p2": "v2",
		"p3": "1234",
	}
	pv2 := map[string]string{
		"p4": "a,  bc,\t   de   ",
		"p5": "ada;afew;  qwe",

		"p6": "1;2;q;3",
		"p7": "-1;2000;134123;3",
	}

	c["s0"] = pv
	ef.LoadFromConf(c)
	c["s1"] = pv
	ef.LoadFromConf(c)
	c["s0"] = pv2
	c["s1"] = pv2
	c["s2"] = pv2
	ef.LoadFromConf(c)

	v, err := ef.ToInt("nsection", "aa")
	assert.NotNil(t, err)
	assert.Equal(t, "section empty:nsection", err.Error())

	v, err = ef.ToInt("s0", "npro")
	assert.NotNil(t, err)
	assert.Equal(t, "property empty:s0.npro", err.Error())

	v, err = ef.ToInt("s0", "p0")
	assert.NotNil(t, err)
	assert.Equal(t, "strconv.Atoi: parsing \"v0\": invalid syntax", err.Error())

	v, err = ef.ToInt("s1", "p3")
	assert.Nil(t, err)
	assert.Equal(t, 1234, v)

	v = ef.ToIntWithDefault("s0", "npro", 33)
	assert.Equal(t, 33, v)

	s, err := ef.ToString("nsection", "aa")
	assert.NotNil(t, err)
	assert.Equal(t, "section empty:nsection", err.Error())

	s, err = ef.ToString("s2", "p2")
	assert.NotNil(t, err)
	assert.Equal(t, "property empty:s2.p2", err.Error())

	s = ef.ToStringWithDefault("nsection", "aa", "def")
	assert.Equal(t, "def", s)

	ss, err := ef.ToSliceString("nsection", "aa", ",")
	assert.NotNil(t, err)
	assert.Equal(t, "section empty:nsection", err.Error())

	ss, err = ef.ToSliceString("s0", "p4", ",")
	assert.Nil(t, err)
	assert.Equal(t, []string{"a", "bc", "de"}, ss)

	ss, err = ef.ToSliceString("s0", "p4", ";")
	assert.Nil(t, err)
	assert.Equal(t, []string{"a,  bc,\t   de"}, ss)

	ss, err = ef.ToSliceString("s0", "p5", ",")
	assert.Nil(t, err)
	assert.Equal(t, []string{"ada;afew;  qwe"}, ss)

	ss, err = ef.ToSliceString("s0", "p5", ";")
	assert.Nil(t, err)
	assert.Equal(t, []string{"ada", "afew", "qwe"}, ss)

	is, err := ef.ToSliceInt("nsection", "aa", ",")
	assert.NotNil(t, err)
	assert.Equal(t, "section empty:nsection", err.Error())

	is, err = ef.ToSliceInt("s1", "p6", ";")
	assert.NotNil(t, err)
	assert.Equal(t, "strconv.Atoi: parsing \"q\": invalid syntax", err.Error())

	is, err = ef.ToSliceInt("s1", "p7", ";")
	assert.Nil(t, err)
	assert.Equal(t, []int{-1, 2000, 134123, 3}, is)
}

type TConf struct {
	Uame string "user name"
	//Passwd string "user passsword"
	//Fuck int `sconf:"ffff"`
	//Girl int64

	// 不是指针的、是指针的，指针为空的或者不为空的
	Ts *struct {
		AAA string
		BBB int
	}
}

func TestUnmarshal(t *testing.T) {
	cfg0 := []byte(
		`
[ffff]
[uame]
[noexist]
[ts]
AAA=a
CCC=true
BBB=233
LLL=1,2,3
M.k0=a,b,c
M.k1=c,d,e
M.k2=e
M.=ddd
M=asd

[SM.a]
EE=ee
FF=ff

[SM.b]
EE=ff
FF=ee

`)

	tf := NewTierConf()
	err := tf.Load(cfg0)
	assert.Nil(t, err)

	var c TConf
	err = tf.Unmarshal(&c)
	t.Log(err)
	assert.NotNil(t, err)

	err = tf.Unmarshal(&c)
	t.Log(err)
	t.Log(c.Ts)
	assert.NotNil(t, err)

	type TConf2 struct {
		Uname  string "user name"
		Passwd string
		Fuck   int
		Girl   int64

		// 不是指针的、是指针的，指针为空的或者不为空的
		Ts *struct {
			AAA string
			BBB uint8

			CCC bool

			LLL []int `sep:"," sconf:"lll"`
			M   map[string][]string
		}

		Ts1 *string

		Sm map[string]struct {
			Ee string
			Ff string
		}
	}

	var c2 TConf2
	err = tf.Unmarshal(&c2)
	assert.Nil(t, err)

	cfg2 := []byte(
		`
[ffff]
ZZZ=1
YYY=33
AAA=b
MMM.k0=3
MMM.k1=4
MMM.k2=5
MMM=
[uame]
[noexist]
[ts]
AAA=a
CCC=false
XXX=ddd
LLL=1,2,3,4,5
[SM.a]
EE=ee
FF=ff

[SM.b]
EE=ff
FF=ee

`)

	tf2 := NewTierConf()
	err = tf2.Load(cfg2)
	assert.Nil(t, err)
	err = tf2.Unmarshal(&c2)

	var c3 map[int]string
	err = tf2.Unmarshal(&c3)
	assert.NotNil(t, err)

	//======
	var c4 map[string]string
	err = tf2.Unmarshal(&c4)
	assert.NotNil(t, err)

	//======
	var c5 map[string]struct {
		YYY int
		ZZZ string
		AAA string
		MMM map[string]int `sep:"."`
	}
	err = tf2.Unmarshal(&c5)
	assert.Nil(t, err)
}
