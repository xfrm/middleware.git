## 简介
xconfig是的内部配置中心库，支持apollo、etcd等配置中心数据的读取，支持配置热加载。

## 使用说明

### 构造 ConfigCenter 对象 

```golang
import (
    "gitee.com/xfrm/middleware/xconfig/apollo"
    "gitee.com/xfrm/middleware/xconfig"
)

// 初始化Apollo客户端，类型直接使用apollo.ConfigerTypeApollo, AppID字段为你创建Apollo时填入的AppID，此项必须对应(eg."base.servmgr")。最后一项namespace列表按需填写
configCenter, err := xconfig.NewConfigCenter(ctx, apollo.ConfigTypeApollo, "AppID", []string{"application"})
if err != nil {
	fmt.Printf("new configer center err:%s", err.Error())
	return
}
```

apollo 中某些 namespace 我们赋予了特定的含义,如：

- `application` 创建 apollo 项目时默认生成的 namespace，通常存放应用本身需要的配置
- `mysql` & `mango` mysql、mango sdk 相关配置，详见[DB动态参数配置](http://confluence.pri.ibanyu.com/pages/viewpage.action?pageId=20406607)
- `rpc.client` rpc 动态参数配置，详见[rpc动态参数配置](http://confluence.pri.ibanyu.com/pages/viewpage.action?pageId=20406611)

### 监听 ConfigCenter 配置变更
***注：改步骤可以省略，如果不需要动态感知变更事件***

1.构造监听对象，入参是 事件处理函数

2.注册监听对象

```golang
observer := xconfig.NewConfigObserver(func(ctx context.Context, event *xconfig.ChangeEvent) {
   	// Todo 处理变更事件
})
configCenter.RegisterObserver(ctx, observer)
```

### 读取配置

```golang
configCenter.GetBool(ctx, "key_bool")
configCenter.GetBoolWithNamespace(ctx, "test", "key_bool_2")
```


### 代码样例

* 可运行示例

[xconfig/example/main.go](xconfig/example/main.go)

* 代码示例

```golang
package main

import (
	"context"
	"fmt"

	"gitee.com/xfrm/middleware/xconfig/apollo"

	"gitee.com/xfrm/middleware/xconfig"
)

func main() {
	ctx := context.TODO()
	//Step1 : 构造实例
	configCenter, err := xconfig.NewConfigCenter(ctx, apollo.ConfigTypeApollo, "base/test", []string{"application", "test"})
	if err != nil {
		fmt.Printf("new configer center err:%s", err.Error())
		return
	}
	
	//Step2 : 监听变更（改步骤可以省略，如果不需要动态感知变更事件）
	observer := xconfig.NewConfigObserver(func(ctx context.Context, event *xconfig.ChangeEvent) {
		// Todo 处理变更事件
	})
	configCenter.RegisterObserver(ctx, observer)

	//Step3 : 获取配置Value
	configCenter.GetBool(ctx, "key_bool")
	configCenter.GetBoolWithNamespace(ctx, "test", "key_bool_2")
}
```
