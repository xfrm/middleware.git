package yaml

import (
	"context"
	"fmt"
	"testing"
)

var ctx = context.Background()

type SdtConfig struct {
	Corpid  string `json:"corpid"`
	AgentId string `json:"agentId"`
	Secret  string `json:"secret"`
	Host    string `json:"host"`
}

func Test_yamlConfigCenter_UnmarshalKey(t *testing.T) {
	var dr = &yamlDriver{}
	center, _ := dr.New(ctx, "/Users/tanghc/Documents/project/share/gotmr/taskmgr/data/dev/conf", "base/taskmgr", nil)
	fmt.Println(center.GetAllKeys(ctx))
	var conf SdtConfig
	center.UnmarshalKey(ctx, "sdt", &conf)
	fmt.Println(conf)
}
