package xcontainer

type StackElement struct {
	next  *StackElement
	stack *Stack
	Value interface{}
}

type Stack struct {
	root StackElement
	len  int
}

func (s *Stack) init() *Stack {
	s.root.next = &s.root
	s.len = 0
	return s
}

func NewStack() *Stack {
	return new(Stack).init()
}

func (s *Stack) Len() int {
	return s.len
}

func (s *Stack) Push(v interface{}) {
	se := StackElement{s.root.next, s, v}
	s.root.next = &se
	s.len++
}

func (s *Stack) Pop() *StackElement {
	if s.len <= 0 {
		return nil
	}
	retval := s.root.next
	s.root.next = retval.next
	s.len--
	return retval
}
