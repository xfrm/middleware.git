package xcontainer

import (
	"fmt"
	"testing"
)

func TestNewStack(t *testing.T) {
	stack := NewStack()
	stack.Push(12)
	stack.Push(45)

	itm := stack.Pop()
	if itm != nil {
		if i, ok := itm.Value.(int); ok {
			fmt.Println(i)
		}
	}
	itm = stack.Pop()
	if itm != nil {
		if i, ok := itm.Value.(int); ok {
			fmt.Println(i)
		}
	}
	itm = stack.Pop()
	if itm != nil {
		if i, ok := itm.Value.(int); ok {
			fmt.Println(i)
		}
	} else {
		fmt.Println("no element")
	}
	stack.Push(7855)
	itm = stack.Pop()
	if itm != nil {
		if i, ok := itm.Value.(int); ok {
			fmt.Println(i)
		}
	} else {
		fmt.Println("no element")
	}
}
