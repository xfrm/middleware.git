package xcontext

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

type testHead struct {
	UID        int64             `thrift:"uid,1" json:"uid"`
	Source     int32             `thrift:"source,2" json:"source"`
	IP         string            `thrift:"ip,3" json:"ip"`
	Region     string            `thrift:"region,4" json:"region"`
	Dt         int32             `thrift:"dt,5" json:"dt"`
	UnionID    string            `thrift:"unionid,6" json:"unionid"`
	Properties map[string]string `thrift:"properties" json:"properties"`
}

func (m testHead) ToKV() map[string]interface{} {
	return map[string]interface{}{
		"uid":        m.UID,
		"source":     m.Source,
		"ip":         m.IP,
		"region":     m.Region,
		"dt":         m.Dt,
		"unionid":    m.UnionID,
		"properties": m.Properties,
	}
}

func (m *testHead) SetKV(key string, value interface{}) {
	switch key {
	case "uid":
		uid, ok := value.(int64)
		if !ok {
			return
		}
		m.UID = uid
	case "source":
		source, ok := value.(int32)
		if !ok {
			return
		}
		m.Source = source
	case "ip":
		ip, ok := value.(string)
		if !ok {
			return
		}
		m.IP = ip
	case "region":
		region, ok := value.(string)
		if !ok {
			return
		}
		m.Region = region
	case "dt":
		dt, ok := value.(int32)
		if !ok {
			return
		}
		m.Dt = dt
	case "unionid":
		unionid, ok := value.(string)
		if !ok {
			return
		}
		m.UnionID = unionid
	case "properties":
		properties, ok := value.(map[string]string)
		if !ok {
			return
		}
		m.Properties = properties
	}
}

func TestGetUid(t *testing.T) {
	ctx := context.Background()
	ctx = context.WithValue(ctx, ContextKeyHead, &testHead{
		UID: int64(100),
	})

	uid, ok := GetUID(ctx)
	assert.True(t, ok)
	assert.Equal(t, int64(100), uid)
}

func TestGetSource(t *testing.T) {
	ctx := context.Background()
	ctx = context.WithValue(ctx, ContextKeyHead, &testHead{
		Source: int32(1),
	})

	source, ok := GetSource(ctx)
	assert.True(t, ok)
	assert.Equal(t, int32(1), source)
}

func TestGetIp(t *testing.T) {
	ctx := context.Background()
	ctx = context.WithValue(ctx, ContextKeyHead, &testHead{
		IP: "192.168.0.1",
	})
	ip, ok := GetIP(ctx)
	assert.True(t, ok)
	assert.Equal(t, "192.168.0.1", ip)
}

func TestGetRegion(t *testing.T) {
	ctx := context.Background()
	ctx = context.WithValue(ctx, ContextKeyHead, &testHead{
		Region: "asia",
	})

	ctx = NewValueContext(ctx)

	region, ok := GetRegion(ctx)
	assert.True(t, ok)
	assert.Equal(t, "asia", region)
}

func TestGetPropertiesHLC(t *testing.T) {
	ctx := context.Background()
	ctx = context.WithValue(ctx, ContextKeyHead, &testHead{
		Properties: map[string]string{
			ContextPropertiesKeyHLC: "china",
		},
	})

	hlc, ok := GetPropertiesHLC(ctx)
	assert.True(t, ok)
	assert.Equal(t, "china", hlc)
}

func TestGetPropertiesZone(t *testing.T) {
	ctx := context.Background()
	ctx = context.WithValue(ctx, ContextKeyHead, &testHead{
		Properties: map[string]string{
			ContextPropertiesKeyZone: "123",
		},
	})

	zone, ok := GetPropertiesZone(ctx)
	assert.True(t, ok)
	assert.Equal(t, int32(123), zone)
}

func TestSetHeaderPropertiesHLC(t *testing.T) {
	// 初始化factory
	InitContextHeaderFactory(func() ContextHeaderSetter {
		return &testHead{
			Properties: make(map[string]string),
		}
	})
	t.Run("存在properties的情况", func(t *testing.T) {
		ctx := context.Background()
		ctx = context.WithValue(ctx, ContextKeyHead, &testHead{
			Properties: map[string]string{},
		})
		ctx = SetHeaderPropertiesHLC(ctx, "china")
		hlc, ok := GetPropertiesHLC(ctx)
		assert.True(t, ok)
		assert.Equal(t, "china", hlc)
	})

	t.Run("header为空的情况", func(t *testing.T) {
		ctx := context.Background()
		ctx = context.WithValue(ctx, ContextKeyHead, &testHead{})
		ctx = SetHeaderPropertiesHLC(ctx, "japan")
		hlc, ok := GetPropertiesHLC(ctx)
		assert.True(t, ok)
		assert.Equal(t, "japan", hlc)
	})

	t.Run("header也没有的情况", func(t *testing.T) {
		ctx := context.Background()
		ctx = SetHeaderPropertiesHLC(ctx, "mac")
		hlc, ok := GetPropertiesHLC(ctx)
		assert.True(t, ok)
		assert.Equal(t, "mac", hlc)
	})

	t.Run("设置所有的值", func(t *testing.T) {
		ctx := context.Background()
		ctx = SetHeaderPropertiesALL(ctx, "china", 123, "japan")
		hlc, ok := GetPropertiesHLC(ctx)
		assert.True(t, ok)
		assert.Equal(t, "china", hlc)

		zone, ok := GetPropertiesZone(ctx)
		assert.True(t, ok)
		assert.Equal(t, int32(123), zone)

		zoneName, ok := GetPropertiesZoneName(ctx)
		assert.True(t, ok)
		assert.Equal(t, "japan", zoneName)
	})

	t.Run("设置 hiii-header ", func(t *testing.T) {
		ctx := context.Background()
		ctx = SetPropertiesHiiiHeader(ctx, "test")
		data, ok := GetPropertiesHiiiHeader(ctx)
		assert.True(t, ok)
		assert.Equal(t, "test", data)
	})
}

func TestGetDt(t *testing.T) {
	ctx := context.Background()
	ctx = context.WithValue(ctx, ContextKeyHead, &testHead{
		Dt: int32(1),
	})

	dt, ok := GetDt(ctx)
	assert.True(t, ok)
	assert.Equal(t, int32(1), dt)
}

func TestGetUnionId(t *testing.T) {
	ctx := context.Background()
	ctx = context.WithValue(ctx, ContextKeyHead, &testHead{
		UnionID: "xyz",
	})

	unionID, ok := GetUnionID(ctx)
	assert.True(t, ok)
	assert.Equal(t, "xyz", unionID)
}

type testEmptyHead struct{}

func (m testEmptyHead) ToKV() map[string]interface{} {
	return map[string]interface{}{}
}

func TestGetterNegativeCases(t *testing.T) {
	t.Run("empty context", func(t *testing.T) {
		ctx := context.Background()

		ip, ok := GetIP(ctx)
		assert.False(t, ok)
		assert.Equal(t, "", ip)
	})

	t.Run("empty kv", func(t *testing.T) {
		ctx := context.Background()
		ctx = context.WithValue(ctx, ContextKeyHead, &testEmptyHead{})

		ip, ok := GetIP(ctx)
		assert.False(t, ok)
		assert.Equal(t, "", ip)
	})

	t.Run("对于没有实现setter的情况", func(t *testing.T) {
		// 初始化factory
		InitContextHeaderFactory(func() ContextHeaderSetter {
			return &testHead{
				Properties: make(map[string]string),
			}
		})
		// 该情况下，由于testEmptyHead没有实现Set接口，getSet为空，会调用工厂创建一个
		ctx := context.Background()
		ctx = context.WithValue(ctx, ContextKeyHead, &testEmptyHead{})
		ctx = SetHeaderPropertiesHLC(ctx, "mac")
		hlc, ok := GetPropertiesHLC(ctx)
		assert.True(t, ok)
		assert.Equal(t, "mac", hlc)
	})
}

type simpleContextControlRouter struct {
	Group string
}

func (m *simpleContextControlRouter) GetControlRouteGroup() (string, bool) {
	return m.Group, true
}

func (m *simpleContextControlRouter) SetControlRouteGroup(group string) error {
	m.Group = group
	return nil
}

func TestGetRouteGroup(t *testing.T) {
	expectedGroup := "lane1"
	sc := &simpleContextControlRouter{Group: expectedGroup}
	ctx := context.WithValue(context.Background(), ContextKeyControl, sc)

	var group string
	var ok bool
	group, ok = GetControlRouteGroup(ctx)
	assert.True(t, ok)
	assert.Equal(t, expectedGroup, group)

	group, ok = GetControlRouteGroup(context.Background())
	assert.False(t, ok)
	assert.Equal(t, "", group)

	var scr *simpleContextControlRouter
	ctx = context.WithValue(context.Background(), ContextKeyControl, scr)

	group, ok = GetControlRouteGroup(ctx)
	assert.False(t, ok)
	assert.Equal(t, "", group)

	var sci ContextControlRouter = &simpleContextControlRouter{Group: expectedGroup}
	ctx = context.WithValue(context.Background(), ContextKeyControl, sci)

	group, ok = GetControlRouteGroup(ctx)
	assert.True(t, ok)
	assert.Equal(t, expectedGroup, group)
}

func TestGetRouteGroupWithDefault(t *testing.T) {
	groupLane1 := "lane1"
	groupLane2 := "lane2"
	sc := &simpleContextControlRouter{Group: groupLane2}
	ctx := context.WithValue(context.Background(), ContextKeyControl, sc)

	assert.Equal(t, groupLane1, GetControlRouteGroupWithDefault(context.Background(), groupLane1))
	assert.Equal(t, groupLane2, GetControlRouteGroupWithDefault(ctx, groupLane1))
}

type simpleContextControlCaller struct {
	ServerName string
	ServerID   string
	Method     string
}

func (m *simpleContextControlCaller) GetControlCallerServerName() (string, bool) {
	return m.ServerName, true
}

func (m *simpleContextControlCaller) SetControlCallerServerName(serverName string) error {
	m.ServerName = serverName
	return nil
}

func (m *simpleContextControlCaller) GetControlCallerServerId() (string, bool) {
	return m.ServerID, true
}

func (m *simpleContextControlCaller) SetControlCallerServerId(serverID string) error {
	m.ServerID = serverID
	return nil
}

func (m *simpleContextControlCaller) GetControlCallerMethod() (string, bool) {
	return m.Method, true
}

func (m *simpleContextControlCaller) SetControlCallerMethod(method string) error {
	m.Method = method
	return nil
}

func TestContextControlCaller(t *testing.T) {
	simpleCaller := &simpleContextControlCaller{"report", "0", "GetRtcRuntimeLog"}
	var caller ContextControlCaller
	caller = simpleCaller

	serverName, ok := caller.GetControlCallerServerName()
	assert.True(t, ok)
	assert.Equal(t, "report", serverName)

	serverID, ok := caller.GetControlCallerServerId()
	assert.True(t, ok)
	assert.Equal(t, "0", serverID)

	method, ok := caller.GetControlCallerMethod()
	assert.True(t, ok)
	assert.Equal(t, "GetRtcRuntimeLog", method)
}

func TestCallerMethod(t *testing.T) {
	ctx := SetCallerMethod(context.TODO(), "SetCaller")
	method, ok := GetCallerMethod(ctx)
	assert.True(t, ok)
	assert.Equal(t, "SetCaller", method)
}

func TestCancel(t *testing.T) {
	ctx, _ := context.WithTimeout(context.Background(), time.Millisecond*100)
	ctx1 := NewValueContext(ctx)
	time.Sleep(time.Second * 1)
	select {
	case <-ctx1.Done():
		t.Error("ctx1 done")
	case <-ctx.Done():
		t.Log("ctx done")
	default:
		t.Error("no cancel")
	}
}

func TestGetControlRouteGroupWithMasterName(t *testing.T) {
	t1Lane := new(simpleContextControlRouter)
	t1Lane.SetControlRouteGroup("t1")
	masterLane := new(simpleContextControlRouter)
	masterLane.SetControlRouteGroup("")
	type args struct {
		ctx    context.Context
		master string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "未存在泳道信息时，默认主干分支名称",
			args: args{
				ctx:    context.Background(),
				master: "default",
			},
			want: "default",
		},
		{
			name: "t1泳道名称",
			args: args{
				ctx:    context.WithValue(context.Background(), ContextKeyControl, t1Lane),
				master: "default",
			},
			want: "t1",
		},
		{
			name: "修改主干泳道名称为default",
			args: args{
				ctx:    context.WithValue(context.Background(), ContextKeyControl, masterLane),
				master: "default",
			},
			want: "default",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetControlRouteGroupWithMasterName(tt.args.ctx, tt.args.master); got != tt.want {
				t.Errorf("GetControlRouteGroupWithMasterName() = %v, want %v", got, tt.want)
			}
		})
	}
}
func TestContext(t *testing.T) {
	var empty = context.Background()
	cancelCtx1, cancel1 := context.WithCancel(empty)
	fmt.Println(cancel1)
	valCtx := context.WithValue(cancelCtx1, "aaa", "123")
	cancelCtx2, cancel2 := context.WithCancel(valCtx)
	fmt.Println(cancelCtx2.Value("aaa"), cancel2)
}
