// Copyright 2014 The sutil Author. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package xcmd

import (
	"testing"
	"time"

	"log"
)

func TestIt(t *testing.T) {
	//testProg0(t)
	//testProg1(t)
	//testProg2(t)

	testStart0(t)
	//time.Sleep(20 * time.Second)
	//testStart1(t)
	//testStartStop(t)
}

func testStart0(t *testing.T) {

	c := NewScmd("grep", "A", "/home/fenggx/shawn_go/src/github.com/shawnfeng/sutil/scmd/*") // * 执行是有错误的，因为  *  通配符是shell支持的
	//c := NewScmd("grep", "A", "/home/fenggx/shawn_go/src/github.com/shawnfeng/sutil/scmd/scmd.go")
	//c := NewScmd("ls", "/tmp")

	//c := NewScmd("echo", "AAAA", "BBBB")

	stdout, stderr, err := c.StartWaitOutput()
	if err != nil {
		t.Errorf("%s", err)
	}

	log.Printf("o:%s e:%s c:%v", stdout, stderr, c.prog)

	time.Sleep(time.Second)
	log.Printf("o:%s e:%s c:%v", stdout, stderr, c.prog)
}
