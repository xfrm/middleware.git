package xlog

import (
	"context"
	"testing"

	"gitee.com/xfrm/middleware/xlog"
	"github.com/opentracing/opentracing-go"
	"github.com/stretchr/testify/assert"

	"gitee.com/xfrm/middleware/xtrace"
)

func newConsole() (*xlog.XLogger, error) {
	xl, err := xlog.NewConsole(xlog.InfoLevel)
	return xl, err
}

func TestNewConsole(t *testing.T) {
	_, err := xlog.NewConsole(xlog.InfoLevel)
	if err != nil {
		t.Fatalf("test NewConsole failed, %v", err)
	}
}

func TestLevel(t *testing.T) {
	xl, _ := newConsole()
	if xl.Level() != xlog.InfoLevel {
		t.Fatalf("test Level failed")
	}
}

func TestSetLevel(t *testing.T) {
	xl, _ := newConsole()
	xl.Info(context.TODO(), "test...")
	xl.Infof(context.TODO(), "test %s", "msg")
	xl.Infow(context.TODO(), "test msg", "k1", "value1")
	xl.SetLevel(xlog.ErrorLevel)
	xl.Info(context.TODO(), "test...")
	if xl.Level() != xlog.ErrorLevel {
		t.Fatalf("test SetLevel failed")
	}
}

func TestTraceId(t *testing.T) {
	xl, _ := newConsole()
	ctx := context.Background()
	xtrace.InitDefaultTracer("test")
	tracer := xtrace.GlobalTracer()
	span := tracer.StartSpan("test log")
	ctx = opentracing.ContextWithSpan(ctx, span)
	xl.Infof(ctx, "test trace id ....")
}

func TestXLogger_Infom(t *testing.T) {
	type TestInfom struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}
	xl, _ := newConsole()
	ctx := context.Background()
	xtrace.InitDefaultTracer("test")
	tracer := xtrace.GlobalTracer()
	span := tracer.StartSpan("test log")
	ctx = opentracing.ContextWithSpan(ctx, span)
	kvs := make(map[string]interface{})
	kvs["str"] = "str test"
	kvs["int"] = "int test"
	kvs["strucet"] = TestInfom{
		Username: "username",
		Password: "password",
	}
	xl.Infom(ctx, "test msg", kvs)
}

func TestConveryLevel(t *testing.T) {
	levelStrArray := []struct {
		name  string
		want  xlog.Level
		level string
	}{
		{"testDebug", xlog.DebugLevel, "Debug"},
		{"testInfo", xlog.InfoLevel, "Info"},
		{"testWarn", xlog.WarnLevel, "Warn"},
		{"testError", xlog.ErrorLevel, "Error"},
		{"testFatal", xlog.FatalLevel, "Fatal"},
		{"testPanic", xlog.PanicLevel, "panic"},
	}
	for _, tt := range levelStrArray {
		t.Run(tt.name, func(t *testing.T) {
			level := xlog.ConvertLevel(tt.level)
			assert.Equal(t, tt.want, level)
		})
	}
}
