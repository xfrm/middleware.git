package xlog

import (
	"context"
	"testing"

	"gitee.com/xfrm/middleware/xlog"
	"github.com/stretchr/testify/assert"
)

func TestLogStat(t *testing.T) {
	ctx := context.Background()
	xl, _ := newConsole()
	xl.SetLevel(xlog.InfoLevel)
	xl.Debugf(ctx, "debug")
	xl.Info(ctx, "info")
	xl.Info(ctx, "info2")
	xl.Warn(ctx, "warn1")
	xl.Warn(ctx, "warn2")
	xl.Error(ctx, "error1")
	xl.Error(ctx, "error2")
	xl.Error(ctx, "error3")

	result, logs := xlog.LogStat()
	assert.Equal(t, int64(0), result["TRACE"])
	assert.Equal(t, int64(0), result["DEBUG"])
	assert.Equal(t, int64(2), result["INFO"])
	assert.Equal(t, int64(3), result["ERROR"])

	assert.Equal(t, 3, len(logs))
}
