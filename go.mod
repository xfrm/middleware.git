module gitee.com/xfrm/middleware

go 1.12

replace (
	github.com/coreos/bbolt v1.3.4 => go.etcd.io/bbolt v1.3.4
	github.com/golang/protobuf => github.com/golang/protobuf v1.4.2
	github.com/stretchr/testify v1.6.1 => github.com/stretchr/testify v1.6.0
	go.etcd.io/bbolt v1.3.4 => github.com/coreos/bbolt v1.3.4
	golang.org/x/net => golang.org/x/net v0.0.0-20201021035429-f5854403a974
	google.golang.org/grpc => google.golang.org/grpc v1.29.1
	google.golang.org/protobuf => google.golang.org/protobuf v1.25.0
)

require (
	gitee.com/xfrm/structs v1.0.0
	github.com/360EntSecGroup-Skylar/excelize v1.4.1
	github.com/DATA-DOG/go-sqlmock v1.3.3
	github.com/HdrHistogram/hdrhistogram-go v1.1.0 // indirect
	github.com/StackExchange/wmi v1.2.1 // indirect
	github.com/VividCortex/gohistogram v1.0.0
	github.com/VividCortex/mysqlerr v1.0.0 // indirect
	github.com/ZhengHe-MD/agollo/v4 v4.1.4
	github.com/ZhengHe-MD/properties v0.2.3
	github.com/agiledragon/gomonkey v2.0.1+incompatible
	github.com/apache/pulsar-client-go v0.5.0
	github.com/blang/semver/v4 v4.0.0
	github.com/coreos/bbolt v1.3.4 // indirect
	github.com/coreos/etcd v3.3.22+incompatible
	github.com/coreos/go-semver v0.3.1 // indirect
	github.com/coreos/go-systemd v0.0.0-20191104093116-d3cd4ed1dbcf // indirect
	github.com/coreos/pkg v0.0.0-20230601102743-20bbbf26f4d8 // indirect
	github.com/dustin/go-humanize v1.0.1 // indirect
	github.com/fsnotify/fsnotify v1.6.0
	github.com/gin-gonic/gin v1.7.2
	github.com/go-redis/redis v6.15.1+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang/protobuf v1.5.3
	github.com/google/btree v1.1.2 // indirect
	github.com/google/uuid v1.3.0
	github.com/grpc-ecosystem/go-grpc-middleware v1.4.0 // indirect
	github.com/jmoiron/sqlx v1.2.1-0.20190826204134-d7d95172beb5
	github.com/jonboulle/clockwork v0.4.0 // indirect
	github.com/julienschmidt/httprouter v1.3.1-0.20200921135023-fe77dd05ab5a
	github.com/kaneshin/go-pkg v0.0.0-20150919125626-a8e1479186cf
	github.com/opentracing/opentracing-go v1.1.0
	github.com/panjf2000/ants/v2 v2.4.1
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.11.1
	github.com/segmentio/kafka-go v0.3.10
	github.com/serialx/hashring v0.0.0-20200727003509-22c0c7ab6b1b
	github.com/shawnfeng/lumberjack.v2 v0.0.0-20181226094728-63d76296ede8
	github.com/shirou/gopsutil v2.19.12+incompatible
	github.com/soheilhy/cmux v0.1.5 // indirect
	github.com/spf13/viper v1.16.0
	github.com/stretchr/testify v1.8.3
	github.com/tmc/grpc-websocket-proxy v0.0.0-20220101234140-673ab2c3ae75 // indirect
	github.com/uber/jaeger-client-go v2.20.1+incompatible
	github.com/uber/jaeger-lib v2.4.1+incompatible // indirect
	github.com/vaughan0/go-ini v0.0.0-20130923145212-a98ad7ee00ec
	github.com/xiang90/probing v0.0.0-20221125231312-a49e3df8f510 // indirect
	github.com/xwb1989/sqlparser v0.0.0-20180606152119-120387863bf2
	github.com/zhulongcheng/testsql v0.0.0-20190926072326-75d045b177ec
	go.etcd.io/bbolt v1.3.4 // indirect
	go.uber.org/zap v1.21.0
	google.golang.org/genproto v0.0.0-20230410155749-daa745c078e1
	google.golang.org/grpc v1.55.0
	gopkg.in/yaml.v2 v2.4.0
	sigs.k8s.io/yaml v1.3.0 // indirect
)
