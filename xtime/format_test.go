// Copyright 2014 The sutil Author. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package xtime

import (
	"testing"

	"time"
)

const (
	ANSIC       = "Mon Jan _2 15:04:05 2006"
	UnixDate    = "Mon Jan _2 15:04:05 MST 2006"
	RubyDate    = "Mon Jan 02 15:04:05 -0700 2006"
	RFC822      = "02 Jan 06 15:04 MST"
	RFC822Z     = "02 Jan 06 15:04 -0700" // RFC822 with numeric zone
	RFC850      = "Monday, 02-Jan-06 15:04:05 MST"
	RFC1123     = "Mon, 02 Jan 2006 15:04:05 MST"
	RFC1123Z    = "Mon, 02 Jan 2006 15:04:05 -0700" // RFC1123 with numeric zone
	RFC3339     = "2006-01-02T15:04:05Z07:00"
	RFC3339Nano = "2006-01-02T15:04:05.999999999Z07:00"
	Kitchen     = "3:04PM"
)

func TestFormat(t *testing.T) {
	now := time.Now()
	var cstZone = time.FixedZone("CST", 8*3600)
	for _, fomat := range []string{
		ANSIC, UnixDate, RubyDate,
		RFC822,
		RFC822Z,
		RFC850,
		RFC1123,
		RFC1123Z,
		RFC3339,
		RFC3339Nano,
		Kitchen,
	} {
		t.Logf("%s => %s ==> %s", fomat, now.Format(fomat), now.In(cstZone).Format(fomat))
	}

}
