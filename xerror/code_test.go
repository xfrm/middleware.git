package xerror

import (
	"github.com/blang/semver/v4"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc/codes"
	"net/http"
	"testing"
)

func TestErrorCodeMapper_MapToHTTPStatusCode_WithMinVersions(t *testing.T) {
	picturebook := int32(4)
	picturebookMinVer, err := semver.Make("2.3.1")
	assert.Nil(t, err)

	notFoundApp := int32(404)
	notFoundAppMinVer, err := semver.Make("999.999.999")
	assert.Nil(t, err)

	config := map[int32]semver.Version{
		picturebook: picturebookMinVer,
		notFoundApp: notFoundAppMinVer,
	}
	ecm := NewErrorCodeMapper(config)

	lowerVer, err := semver.New("1.2.3")
	assert.Nil(t, err)
	sameVer, err := semver.New("2.3.1")
	assert.Nil(t, err)
	higherVer, err := semver.New("2.3.2")
	assert.Nil(t, err)

	assert.Equal(t, http.StatusOK, ecm.MapToHTTPStatusCode(codes.Unknown, picturebook, lowerVer))
	assert.Equal(t, http.StatusInternalServerError, ecm.MapToHTTPStatusCode(codes.Unknown, picturebook, sameVer))
	assert.Equal(t, http.StatusInternalServerError, ecm.MapToHTTPStatusCode(codes.Unknown, picturebook, higherVer))
	assert.Equal(t, http.StatusOK, ecm.MapToHTTPStatusCode(codes.Unknown, 666, higherVer))
}

func TestErrorCodeMapper_MapToHTTPStatusCode_WithVersionRange(t *testing.T) {
	picturebook := int32(4)
	r, err := semver.ParseRange(">=1.0.0 <2.0.0 !=1.2.3")
	assert.Nil(t, err)
	config := map[int32]semver.Range{
		picturebook: r,
	}
	ecm := NewErrorCodeMapperWithVersionRange(config)

	type testcase struct {
		expectedHTTPStatusCode int
		app                    int32
		version                string
		errorCode              codes.Code
	}
	cases := []testcase{
		{
			expectedHTTPStatusCode: http.StatusOK,
			app:                    picturebook,
			version:                "0.9.9",
			errorCode:              codes.Internal,
		},
		{
			expectedHTTPStatusCode: http.StatusOK,
			app:                    picturebook,
			version:                "6.11.17",
			errorCode:              codes.Internal,
		},
		{
			expectedHTTPStatusCode: http.StatusInternalServerError,
			app:                    picturebook,
			version:                "1.2.1",
			errorCode:              codes.Internal,
		},
		{
			expectedHTTPStatusCode: http.StatusOK,
			app:                    picturebook,
			version:                "1.2.3",
			errorCode:              codes.Internal,
		},
		{
			expectedHTTPStatusCode: http.StatusOK,
			app:                    666, // non-existing-app
			version:                "1.2.1",
			errorCode:              codes.Internal,
		},
	}

	for _, c := range cases {
		ver, err := semver.New(c.version)
		assert.Nil(t, err)
		assert.Equal(t, c.expectedHTTPStatusCode, ecm.MapToHTTPStatusCode(c.errorCode, c.app, ver))
	}
}
