// Code generated by protoc-gen-go. DO NOT EDIT.
// source: palfisherrorinfo.proto

package xerror

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type PalfishErrorInfo struct {
	Domain               string            `protobuf:"bytes,1,opt,name=domain,proto3" json:"domain,omitempty"`
	BizCode              int32             `protobuf:"varint,2,opt,name=biz_code,proto3" json:"biz_code,omitempty"`
	Metadata             map[string]string `protobuf:"bytes,3,rep,name=metadata,proto3" json:"metadata,omitempty" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	XXX_NoUnkeyedLiteral struct{}          `json:"-"`
	XXX_unrecognized     []byte            `json:"-"`
	XXX_sizecache        int32             `json:"-"`
}

func (m *PalfishErrorInfo) Reset()         { *m = PalfishErrorInfo{} }
func (m *PalfishErrorInfo) String() string { return proto.CompactTextString(m) }
func (*PalfishErrorInfo) ProtoMessage()    {}
func (*PalfishErrorInfo) Descriptor() ([]byte, []int) {
	return fileDescriptor_7f4321d8d8686ccb, []int{0}
}

func (m *PalfishErrorInfo) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_PalfishErrorInfo.Unmarshal(m, b)
}
func (m *PalfishErrorInfo) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_PalfishErrorInfo.Marshal(b, m, deterministic)
}
func (m *PalfishErrorInfo) XXX_Merge(src proto.Message) {
	xxx_messageInfo_PalfishErrorInfo.Merge(m, src)
}
func (m *PalfishErrorInfo) XXX_Size() int {
	return xxx_messageInfo_PalfishErrorInfo.Size(m)
}
func (m *PalfishErrorInfo) XXX_DiscardUnknown() {
	xxx_messageInfo_PalfishErrorInfo.DiscardUnknown(m)
}

var xxx_messageInfo_PalfishErrorInfo proto.InternalMessageInfo

func (m *PalfishErrorInfo) GetDomain() string {
	if m != nil {
		return m.Domain
	}
	return ""
}

func (m *PalfishErrorInfo) GetBizCode() int32 {
	if m != nil {
		return m.BizCode
	}
	return 0
}

func (m *PalfishErrorInfo) GetMetadata() map[string]string {
	if m != nil {
		return m.Metadata
	}
	return nil
}

func init() {
	proto.RegisterType((*PalfishErrorInfo)(nil), "xerror.PalfishErrorInfo")
	proto.RegisterMapType((map[string]string)(nil), "xerror.PalfishErrorInfo.MetadataEntry")
}

func init() { proto.RegisterFile("palfisherrorinfo.proto", fileDescriptor_7f4321d8d8686ccb) }

var fileDescriptor_7f4321d8d8686ccb = []byte{
	// 186 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x12, 0x2b, 0x48, 0xcc, 0x49,
	0xcb, 0x2c, 0xce, 0x48, 0x2d, 0x2a, 0xca, 0x2f, 0xca, 0xcc, 0x4b, 0xcb, 0xd7, 0x2b, 0x28, 0xca,
	0x2f, 0xc9, 0x17, 0x62, 0xab, 0x00, 0x8b, 0x28, 0x1d, 0x67, 0xe4, 0x12, 0x08, 0x80, 0x28, 0x71,
	0x05, 0x09, 0x78, 0xe6, 0xa5, 0xe5, 0x0b, 0x89, 0x71, 0xb1, 0xa5, 0xe4, 0xe7, 0x26, 0x66, 0xe6,
	0x49, 0x30, 0x2a, 0x30, 0x6a, 0x70, 0x06, 0x41, 0x79, 0x42, 0x52, 0x5c, 0x1c, 0x49, 0x99, 0x55,
	0xf1, 0xc9, 0xf9, 0x29, 0xa9, 0x12, 0x4c, 0x0a, 0x8c, 0x1a, 0xac, 0x41, 0x70, 0xbe, 0x90, 0x13,
	0x17, 0x47, 0x6e, 0x6a, 0x49, 0x62, 0x4a, 0x62, 0x49, 0xa2, 0x04, 0xb3, 0x02, 0xb3, 0x06, 0xb7,
	0x91, 0x9a, 0x1e, 0xc4, 0x0e, 0x3d, 0x74, 0xf3, 0xf5, 0x7c, 0xa1, 0x0a, 0x5d, 0xf3, 0x4a, 0x8a,
	0x2a, 0x83, 0xe0, 0xfa, 0xa4, 0xac, 0xb9, 0x78, 0x51, 0xa4, 0x84, 0x04, 0xb8, 0x98, 0xb3, 0x53,
	0x2b, 0xa1, 0xae, 0x00, 0x31, 0x85, 0x44, 0xb8, 0x58, 0xcb, 0x12, 0x73, 0x4a, 0x21, 0xf6, 0x73,
	0x06, 0x41, 0x38, 0x56, 0x4c, 0x16, 0x8c, 0x49, 0x6c, 0x60, 0x8f, 0x19, 0x03, 0x02, 0x00, 0x00,
	0xff, 0xff, 0x66, 0x18, 0x67, 0x38, 0xf2, 0x00, 0x00, 0x00,
}
