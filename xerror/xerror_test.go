package xerror

import (
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc/status"
	"strings"
	"testing"
)

func TestJSONMarshalStatus(t *testing.T) {
	pei := &PalfishErrorInfo{
		Domain:   "base/metadatacenter",
		BizCode:  1192,
		Metadata: map[string]string{"foo": "bar"},
	}
	s := NewStatusFromPalfishErrorInfo(RateLimited, "rate limited...hold on", pei)
	data, err := JSONMarshalStatus(s)
	// 确保 JSON 序列化后 biz_code 字段仍为 biz_code 而非 bizCode
	assert.NotEqual(t, -1, strings.Index(string(data), "biz_code"))
	assert.Equal(t, -1, strings.Index(string(data), "bizCode"))
	assert.Nil(t, err)
}

func TestJSONUnmarshalStatus_Success(t *testing.T) {
	var s status.Status
	data := `{"code":52,"message":"rate limited...hold on","details":[{"@type":"type.googleapis.com/xerror.PalfishErrorInfo","domain":"base/metadatacenter","biz_code":1192,"metadata":{"foo":"bar"}}]}`
	err := JSONUnmarshalStatus([]byte(data), &s)
	assert.Nil(t, err)
	assert.Equal(t, RateLimited, s.Code())
	assert.Equal(t, "rate limited...hold on", s.Message())

	// check detail
	assert.Equal(t, 1, len(s.Details()))
	detail := s.Details()[0]
	pei, ok := detail.(*PalfishErrorInfo)
	assert.True(t, ok)
	assert.Equal(t, "base/metadatacenter", pei.Domain)
	assert.Equal(t, int32(1192), pei.BizCode)
	assert.Equal(t, "bar", pei.Metadata["foo"])
}

func TestJSONUnmarshalStatus_Fail(t *testing.T) {
	var s status.Status
	data := `{"code":52,"message":"rate limited...hold on","details":[{"@type":"type.googleapis.com/xerror.PalfishErrorInfoxxx","domain":"base/metadatacenter","biz_code":1192,"metadata":{"foo":"bar"}}]}`
	// PalfishErrorInfoxxx 类型未注册，反序列化失败
	err := JSONUnmarshalStatus([]byte(data), &s)
	assert.NotNil(t, err)
}
