package xhttp

import (
	"net/http"
	"strings"

	"gitee.com/xfrm/middleware/xnet"
)

//根据请求头获取ip
//取请求头中X-Forwarded-For，如果有多个ip，取第一个外网ip。如果无外网ip 取第一个ip
func IpAddressHttpClient(r *http.Request) string {
	hdr := r.Header
	hdrRealIp := hdr.Get("X-Real-Ip")
	hdrForwardedFor := hdr.Get("X-Forwarded-For")

	if hdrRealIp == "" && hdrForwardedFor == "" {
		return xnet.IpAddrFromRemoteAddr(r.RemoteAddr)
	}

	if hdrForwardedFor != "" {
		// X-Forwarded-For is potentially a list of addresses separated with ","
		parts := strings.Split(hdrForwardedFor, ",")
		for i, p := range parts {
			parts[i] = strings.TrimSpace(p)
		}
		// TODO: should return first non-local address
		for _, ip := range parts {
			ok, _ := xnet.IsInterIp(ip)
			if !ok && len(ip) > 5 && xnet.IsPublicIPStr(ip) {
				return ip
			}
		}

	}

	return hdrRealIp
}
