package xsql

const (
	// MysqlConfNamespace mysql apollo conf namespace
	MysqlConfNamespace = "mysql"
	// MaxIdleConnsKey ...
	MaxIdleConnsKey = "maxIdleConns"
	// MaxOpenConnsKey ...
	MaxOpenConnsKey = "maxOpenConns"
	// MaxLifeTimeSecKey ...
	MaxLifeTimeSecKey = "maxLifeTimeSec"
	// TimeoutMsecKey ...
	TimeoutMsecKey = "timeoutMsec"
	// ReadTimeoutMsecKey ...
	ReadTimeoutMsecKey = "readTimeoutMsec"
	// WriteTimeoutMsecKey ...
	WriteTimeoutMsecKey = "writeTimeoutMsec"
	UserNameKey         = "username"
	PasswordKey         = "password"
	// KeySep
	KeySep = "."

	defaultMaxIdleConns       = 64
	defaultMaxOpenConns       = 128
	defaultReadTimeoutSecond  = 10
	defaultWriteTimeoutSecond = 10
	defaultMaxLifeTimeSecond  = 3600 * 6
	defaultTimeoutSecond      = 3
)

// MysqlConf ...
type MysqlConf struct {
	DBName  string   `properties:"dbName" json:"dbName" yaml:"dbName"`
	DBAddrs []string `properties:"dbAddrs" json:"dbAddrs" yaml:"dbAddrs"`

	MaxIdleConns     int    `properties:"maxIdleConns" json:"maxIdleConns" yaml:"maxIdleConns"`
	MaxOpenConns     int    `properties:"maxOpenConns" json:"maxOpenConns" yaml:"maxOpenConns"`
	MaxLifeTimeSec   int    `properties:"maxLifeTimeSec" json:"maxLifeTimeSec" yaml:"maxLifeTimeSec"`
	TimeoutMsec      int    `properties:"timeoutMsec" json:"timeoutMsec" yaml:"timeoutMsec"`
	ReadTimeoutMsec  int    `properties:"readTimeoutMsec" json:"readTimeoutMsec" yaml:"readTimeoutMsec"`
	WriteTimeoutMsec int    `properties:"writeTimeoutMsec" json:"writeTimeoutMsec" yaml:"writeTimeoutMsec"`
	Username         string `properties:"username" json:"username" yaml:"username"`
	Password         string `properties:"password" json:"password" yaml:"password"`
}

func (c *MysqlConf) LoadDefault(insName string) {
	if c.TimeoutMsec == 0 {
		c.TimeoutMsec = defaultTimeoutSecond
	}
	if c.ReadTimeoutMsec == 0 {
		c.ReadTimeoutMsec = defaultReadTimeoutSecond
	}
	if c.WriteTimeoutMsec == 0 {
		c.WriteTimeoutMsec = defaultWriteTimeoutSecond
	}
	if c.MaxLifeTimeSec == 0 {
		c.MaxLifeTimeSec = defaultMaxLifeTimeSecond
	}
	if c.MaxIdleConns == 0 {
		c.MaxIdleConns = defaultMaxIdleConns
	}
	if c.MaxOpenConns == 0 {
		c.MaxOpenConns = defaultMaxOpenConns
	}

}
