### Builder

Builder是一个构建sql的工具。注意：一些复杂的sql语句，通过builder构建并非最优的，建议采用裸写sql

### QuickStart

#### example_1

``` go
package main

import (
    qb "gitee.com/xfrm/middleware/xsql/builder"
    "gitee.com/xfrm/middleware/xsql/manager"
)

func main() {
    db := manager.GetDB(context.Todo(), "ACCOUT", "user")
    mp := map[string]interface{}{
    	"country": "China",
    	"role": "driver",
    	"age >": 45,
        "_groupby": "name",
        "_having": map[string]interface{}{
            "total >": 1000,
            "total <=": 50000,
        },
    	"_orderby": "age desc",
    }
    cond,vals,err := qb.BuildSelect("tableName", where, []string{"name", "count(price) as total", "age"})
    
    //cond: SELECT name,count(price) as total,age FROM tableName WHERE (age>? AND country=? AND role=?) GROUP BY name HAVING (total>? AND total<=?) ORDER BY age DESC
    //vals: []interface{}{45, "China", "driver", 1000, 50000}

	if nil != err {
		panic(err)
	}	
	
    rows,err := db.Query(cond, vals...)
    if nil != err {
        panic(err)
    }
    defer rows.Close()
    for rows.Next() {
        var id int
        var name,phone string
        rows.Scan(&id, &name, &phone)
        fmt.Println(id, name, phone)
    }

    //have fun !!
}
```

---

## API

#### `BuildSelect`

sign: `BuildSelect(table string, where map[string]interface{}, field []string) (string,[]interface{},error)`

支持的操作符:

* =
* &gt;
* &lt;
* =
* &lt;=
* &gt;=
* !=
* &lt;&gt;
* in
* not in
* like
* not like
* between
* not between

``` go
where := map[string]interface{}{
	"foo <>": "aha",
	"bar <=": 45,
	"sex in": []interface{}{"girl", "boy"},
	"name like": "%James",
}
```

其他关键字支持:

* _orderby
* _groupby
* _having
* _limit
* _forceindex

``` go
where := map[string]interface{}{
	"age >": 100,
	"_orderby": "fieldName asc",
	"_groupby": "fieldName",
	"_having": map[string]interface{}{"foo":"bar",},
	"_limit": []uint{offset, row_count},
	"_forceindex": "PRIMARY",
}
```
注:
* 如果设置了_groupby将忽略_having
* _limit 使用形式:
    * `"_limit": []uint{a,b}` => `LIMIT a,b`
    * `"_limit": []uint{a}` => `LIMIT 0,a`

#### Aggregate

sign: `AggregateQuery(ctx context.Context, db manager.XDB, table string, where map[string]interface{}, aggregate AggregateSymbleBuilder) (ResultResolver, error)`

聚合操作:
* sum
* avg
* max
* min
* count

example:
```go
where := map[string]interface{}{
    "score > ": 100,
    "city in": []interface{}{"Beijing", "Shijiazhuang",}
}
// supported: AggregateSum,AggregateMax,AggregateMin,AggregateCount,AggregateAvg
result, err := AggregateQuery(ctx, db, "tableName", where, AggregateSum("age"))
sumAge := result.Int64()

result,err = AggregateQuery(ctx, db, "tableName", where, AggregateCount("*")) 
numberOfRecords := result.Int64()

result,err = AggregateQuery(ctx, db, "tableName", where, AggregateAvg("score"))
averageScore := result.Float64()
```

#### `BuildSelectWithContext`

sign: `BuildSelectWithContext(ctx context.Context, table string, where map[string]interface{}, field []string) (string,[]interface{},error)`

BuildSelectWithContext 支持通过context来传递一些固定的配置：

`创建新的context传入，不要传入上游context`

* force index

推荐使用statementConditions方式传入
example: 
```go
statementConditions := map[string]interface{}{
     // 两种方式均可
    "_forceindex" : "idx",
    "_forceindex" : []string{"idx"}
}

```
若使用context请遵循以下规则

`使用时请编辑修改dao层相关方法，不要在外层传入`

example:
```go
// 创建新的ctx1传入，一定不要给ctx赋值
ctx1 := context.WithValue(ctx, builder.ContextKeyForceIndex, []string{"PRIMARY", "idx_xxx"})

cond, vals, err := builder.BuilderSelectWithContext(ctx1, table, where, selectFields)
// cond = SELECT * FROM TABLE FORCE INDEX (PRIMARY,idx_xxx)
rows,err := db.QueryContext(ctx, cond, values...)
```
#### `BuildUpdate`

sign: `BuildUpdate(table string, where map[string]interface{}, update map[string]interface{}) (string, []interface{}, error)`

BuildUpdate和BuildSelect相似，但**不支持**:

* _orderby
* _groupby
* _limit
* _having

``` go
where := map[string]interface{}{
	"foo <>": "aha",
	"bar <=": 45,
	"sex in": []interface{}{"girl", "boy"},
}
update := map[string]interface{}{
	"role": "primaryschoolstudent",
	"rank": 5,
}
cond,vals,err := qb.BuildUpdate("table_name", where, update)

db.Exec(cond, vals...)
```

#### `BuildInsert`

sign: `BuildInsert(table string, data []map[string]interface{}) (string, []interface{}, error)`

data是slice形式，每一个元素为map类型:

``` go
var data []map[string]interface{}
data = append(data, map[string]interface{}{
    "name": "deen",
    "age":  23,
})
data = append(data, map[string]interface{}{
    "name": "Tony",
    "age":  30,
})
cond, vals, err := qb.BuildInsert(table, data)
db.Exec(cond, vals...)
```

#### `BuildInsertIgnore`

sign: `BuildInsertIgnore(table string, data []map[string]interface{}) (string, []interface{}, error)`

data是slice形式，每一个元素为map类型:

``` go
var data []map[string]interface{}
data = append(data, map[string]interface{}{
    "name": "deen",
    "age":  23,
})
data = append(data, map[string]interface{}{
    "name": "Tony",
    "age":  30,
})
cond, vals, err := qb.BuildInsertIgnore(table, data)
db.Exec(cond, vals...)
```

#### `BuildReplaceInsert`

sign: `BuildReplaceInsert(table string, data []map[string]interface{}) (string, []interface{}, error)`

data是slice形式，每一个元素为map类型:

``` go
var data []map[string]interface{}
data = append(data, map[string]interface{}{
    "name": "deen",
    "age":  23,
})
data = append(data, map[string]interface{}{
    "name": "Tony",
    "age":  30,
})
cond, vals, err := qb.BuildReplaceInsert(table, data)
db.Exec(cond, vals...)
```

#### `NamedQuery`

sign: `func NamedQuery(sql string, data map[string]interface{}) (string, []interface{}, error)`

对于复杂sql，建议采用这种方式

```go
cond, vals, err := builder.NamedQuery("select * from tb where name={{name}} and id in (select uid from anothertable where score in {{m_score}})", map[string]interface{}{
	"name": "caibirdme",
	"m_score": []float64{3.0, 5.8, 7.9},
})

assert.Equal("select * from tb where name=? and id in (select uid from anothertable where score in (?,?,?))", cond)
assert.Equal([]interface{}{"caibirdme", 3.0, 5.8, 7.9}, vals)
```

#### `BuildDelete`

sign: `BuildDelete(table string, where map[string]interface{}) (string, []interface{}, error)`

------

## Safety
如果使用 `Prepare && stmt.SomeMethods` 则不必担心安全问题.
Prepare机制防止sql注入问题

如果直接调用 `db.Query(cond, vals...)` , 并且不设置`interpolateParams` 为`true`, 驱动最终还是会采用prepare方式，同样不需要担心安全问题

