package main

import (
	"context"
	"fmt"

	"gitee.com/xfrm/middleware/xsql/manager"
)

func main() {
	fmt.Println("begin")
	db, err := manager.GetDB(context.TODO(), "COURSEWAREX", "coursewarex_1")

	fmt.Printf("db:%+v\n", db)
	fmt.Printf("err:%+v\n", err)

}
