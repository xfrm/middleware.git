package xdb

import (
	"context"
	"database/sql"
)

// XDB should work with database/sql.DB and database/sql.Tx, 方便在生成的dao函数里使用事务Tx和非事务DB，XDB自身没有事务相关接口
type XDB interface {
	ExecContext(ctx context.Context, query string, args ...interface{}) (sql.Result, error)
	QueryContext(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error)
	QueryRowContext(ctx context.Context, query string, args ...interface{}) *sql.Row

	SelectOne(ctx context.Context, table string, where map[string]interface{}, item interface{}) error
	Select(ctx context.Context, table string, where map[string]interface{}, items interface{}) error
	Insert(ctx context.Context, table string, data []map[string]interface{}) (int64, error)
	Update(ctx context.Context, table string, where, data map[string]interface{}) (int64, error)
	Upsert(ctx context.Context, table string, where, data, insertSet map[string]interface{}) (int64, error)
	Delete(ctx context.Context, table string, where map[string]interface{}) (int64, error)
	SelectCount(ctx context.Context, table string, where map[string]interface{}) (count int, err error)
}
