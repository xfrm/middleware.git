package manager

import (
	"context"
	"gitee.com/xfrm/middleware/xsql"

	"gitee.com/xfrm/middleware/internal/dbrouter"
)

func factory(ctx context.Context, cluster string, sqlcfg *xsql.MysqlConf) (in dbrouter.Instancer, err error) {
	return NewDBInstance(cluster, sqlcfg)
}
