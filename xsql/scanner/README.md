# Scaner

### Scan

```go
import (
	"fmt"
	
    "gitee.com/xfrm/middleware/xsql/scanner"
    "gitee.com/xfrm/middleware/xsql/manager"
)

type Person struct {
    Name string `bdb:"name"`
    Age int `bdb:"m_age"`
}

var db manager.XDB
db = manager.GetDB(context.Todo(), "ACCOUT", "user")
rows,_ := db.QueryContext(ctx, "select name,m_age from person")
var students []Person
err := scanner.Scan(rows, &students)
for _,student := range students {
	fmt.Println(student)
}
```

*确保Scan的第二个参数为指针类型*

### ScanClose
`ScanClose` 作用同Scan，使用时会自动关闭打开的rows

### ScanMap
ScanMap 返回结果的形式为 []map[string]interface{}

```go
rows,_ := db.Query("select name,m_age from person")
result,err := scanner.ScanMap(rows)
for _,record := range result {
	fmt.Println(record["name"], record["m_age"])
}
```
如果不想定义一个struct接收结果,就可以使用ScanMap，返回结果的形式为`map[string]interface{}`，不建议使用

### ScanMapClose
ScanMapClose 作用同 ScanMap，使用时会自动关闭打开的rows

### Map
`Map` 将struct类型转换为map结构

测试用例如下

```go
package scaner

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestMap(t *testing.T) {
	type Person struct {
		Name string `bdb:"name"`
		Age  int    `bdb:"age"`
		foo  byte   `bdb:"foo"`
	}
	a := Person{"deen", 22, 1}
	b := &Person{"caibirdme", 23, 1}
	c := &b
	mapA, err := Map(a, DefaultTagName)
	ass := assert.New(t)
	ass.NoError(err)
	ass.Equal("deen", mapA["name"])
	ass.Equal(22, mapA["age"])
	_, ok := mapA["foo"]
	ass.False(ok)
	mapB, err := Map(c, "")
	ass.NoError(err)
	ass.Equal("caibirdme", mapB["Name"])
	ass.Equal(23, mapB["Age"])
}
```
* 不可导出的字段会被忽略
* 指针类型字段会被忽略
* 第二个参数指定了struct中的Tagname
