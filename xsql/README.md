## 简介
xsql是内部进行数据库操作的sql builder基础库，易用性介于裸写sql和orm之间，而性能与裸写sql相当。xsql最初基于开源项目[gendry](https://github.com/didi/gendry)，v1.3.1版本，考虑部分功能不能满足我们的需求、生态也比较脆弱，所以我们打算自己去维护、开发xsql.

xsql主要分为三个独立基础库和一个web生成器`生成sql`，如果只是使用xsql，根据[生成sql](http://confluence.pri.ibanyu.com/pages/viewpage.action?pageId=20381903)进行操作即可，本文档主要阐述xsql的模块原理和手工使用方式等:
* [manager](#manager)
* [builder](#builder)
* [scanner](#scanner)
* [生成sql](http://confluence.pri.ibanyu.com/pages/viewpage.action?pageId=20381903)

<h3 id="manager">Manager</h3>

[manager文档](manager/README.md)

manager管理应用DB连接池(manager作为应用/model/model.go中的全局变量，GetDB(...)的返回值不要作为全局变量，应用示例见[生成sql](http://confluence.pri.ibanyu.com/pages/viewpage.action?pageId=20381903))，简单示例代码如下:

``` go
var db *sql.DB
var err error
ctx := context.TODO()
// 获取非事务的DB去执行到函数
db = manager.GetDB(ctx, "ACCOUT", "user")
或者
// 获取事务句柄tx执行接下来的dao函数
tx = db.Begin(ctx)
...
tx.Commit(ctx)
```
事实上，manager做的事情就是就是生成并且管理`dataSouceName`

<h3 id="builder">Builder</h3>

[builder文档](builder/README.md)

builder顾名思义，就是构建生成sql语句。手写sql虽然直观简单，但是可维护性差，最主要的是硬编码容易出错。而且如果遇到大where in查询，而in的集合内容又是动态的，这就非常麻烦了。

builder不是一个ORM,它只是提供简单的API帮你生成sql语句，如：

```go
where := map[string]interface{}{
	"city in": []string{"beijing", "shanghai"},
	"score": 5,
	"age >": 35,
	"address": builder.IsNotNull,
	"_orderby": "bonus desc",
	"_groupby": "department",
}
table := "some_table"
selectFields := []string{"name", "age", "sex"}
cond, values, err := builder.BuildSelect(table, where, selectFields)

//cond = SELECT name,age,sex FROM g_xxx WHERE (score=? AND city IN (?,?) AND age>? AND address IS NOT NULL) GROUP BY department ORDER BY bonus DESC
//values = []interface{}{"beijing", "shanghai", 5, 35}

rows,err := db.QueryContext(ctx, cond, values...)
```

如果你想清除where map中的零值可以使用 builder.OmitEmpty
``` go
where := map[string]interface{}{
		"score": 0,
		"age": 35,
	}
finalWhere := builder.OmitEmpty(where, []string{"score", "age"})
// finalWhere = map[string]interface{}{"age": 35}

// support: Bool, Array, String, Float32, Float64, Int, Int8, Int16, Int32, Int64, Uint, Uint8, Uint16, Uint32, Uint64, Uintptr, Map, Slice, Interface, Struct
```

同时，builder还提供一个便捷方法来进行聚合查询，比如：count,sum,max,min,avg

```go
where := map[string]interface{}{
    "score > ": 100,
    "city in": []interface{}{"Beijing", "Shijiazhuang",}
}
// AggregateSum,AggregateMax,AggregateMin,AggregateCount,AggregateAvg is supported
result, err := AggregateQuery(ctx, db, "tableName", where, AggregateSum("age"))
sumAge := result.Int64()

result,err = AggregateQuery(ctx, db, "tableName", where, AggregateCount("*"))
numberOfRecords := result.Int64()

result,err = AggregateQuery(ctx, db, "tableName", where, AggregateAvg("score"))
averageScore := result.Float64()
```

对于比较复杂的查询, `NamedQuery`将会派上用场:
```go
cond, vals, err := builder.NamedQuery("select * from tb where name={{name}} and id in (select uid from anothertable where score in {{m_score}})", map[string]interface{}{
	"name": "caibirdme",
	"m_score": []float64{3.0, 5.8, 7.9},
})

assert.Equal("select * from tb where name=? and id in (select uid from anothertable where score in (?,?,?))", cond)
assert.Equal([]interface{}{"caibirdme", 3.0, 5.8, 7.9}, vals)
```
slice类型的值会根据slice的长度自动展开
这种方式基本上就是手写sql，非常便于DBA review同时也方便开发者进行复杂sql的调优
**对于关键系统，推荐使用这种方式**


<h3 id="scanner">Scanner</h3>

[scanner文档](scanner/README.md)

执行了数据库操作之后，要把返回的结果集和自定义的struct进行映射。Scanner提供一个简单的接口通过反射来进行结果集和自定义类型的绑定:

```go
type Person struct {
	Name string `bdb:"name"`
	Age int `bdb:"m_age"`
}

rows,err := db.QueryContext(ctx, "SELECT age as m_age,name from g_xxx where xxx")
defer rows.Close()

var students []Person

scanner.Scan(rows, &students)

for _,student := range students {
	fmt.Println(student)
}
```

scanner进行反射时会使用结构体的tag，如上所示，scanner会把结果集中的 m_age 绑定到结构体的Age域上。默认使用的tagName是`bdb:"xxx"`，你也可以自定义。

``` go

type Person struct {
	Name string `json:"name" bdb:"name"`
	Age int `json:"m_age" bdb:"m_age"`
}

// ...
var student Person
scaner.Scan(rows, &student)
```

### ScanMap
ScanMap方法返回的是一个map，有时候你可能不太像定义一个结构体去存你的中间结果，那么ScanMap或许比较有帮助

```go
rows,_ := db.QueryContext(ctx, "select name,m_age from person")
result,err := scanner.ScanMap(rows)
for _,record := range result {
	fmt.Println(record["name"], record["m_age"])
}
rows.Close()
```

注意：

* 不要使用mysql关键字作为表名、列名
* 如果是使用Scan或者ScanMap的话，你必须在之后手动close rows
* 传给Scan的必须是引用
* ScanClose和ScanMapClose不需要手动close rows

### ByteUnmarshaler
ByteUnmarshaler是一个接口，实现此接口的类型可以直接将json字段解析上去。
如果希望在返回后保留数据，UnmarshalByte方法必须复制JSON数据。

```go
// ByteUnmarshaler is the interface implemented by types
// that can unmarshal a JSON description of themselves.
// The input can be assumed to be a valid encoding of
// a JSON value. UnmarshalByte must copy the JSON data
// if it wishes to retain the data after returning.
//
// By convention, to approximate the behavior of Unmarshal itself,
// ByteUnmarshaler implement UnmarshalByte([]byte("null")) as a no-op.
type ByteUnmarshaler interface {
	UnmarshalByte(data []byte) error
}

// example
type taskContent struct {
    Name string `json:"name"`
    Age  int    `json:"age"`
}

func (p *taskContent) UnmarshalByte(data []byte) error {
    return json.Unmarshal(data, p)
}
```
