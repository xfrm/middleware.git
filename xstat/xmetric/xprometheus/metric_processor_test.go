package xprometheus

import (
	"testing"
)

func TestDriver(t *testing.T) {
	mp := NewMetricProcessor()
	addr, handler := mp.Driver()
	t.Log(addr)
	t.Log(handler)
}
