package xprometheus

import (
	"testing"
)

func TestSafePromethuesValue(t *testing.T) {
	in := "a.b-c"
	want := "a_b_c"
	out := SafePromethuesValue(in)
	if out != want {
		t.Errorf("test SafePromethuesValue failed, want: %s, result: %s\n", want, out)
	}
}
