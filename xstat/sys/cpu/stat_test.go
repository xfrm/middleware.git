package cpu

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestStat(t *testing.T) {
	var s Stat
	var i Info
	var err error
	stats, err = NewPsutilCPU(0)
	assert.NoError(t, err)
	ReadStat(&s)
	i = GetInfo()

	assert.Zero(t, s.Usage)
	assert.NotZero(t, i.Frequency)
	assert.NotZero(t, i.Quota)
}
