package xutil

import (
	"fmt"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestNewWorkerPool(t *testing.T) {
	p, err := NewWorkerPoolWithOptions(10)
	if err != nil {
		t.Error(err)
	}
	if p == nil {
		t.Error("test NewWorkerPool failed, pool is nil")
	}
}

func demoFunc() {
	time.Sleep(10 * time.Millisecond)
	fmt.Println("Hello World!")
}
func TestSubmit(t *testing.T) {
	// 需要等待，去掉WaitGroup即无需等待执行完成
	var wg sync.WaitGroup
	p, _ := NewWorkerPool(10)
	task := func() {
		demoFunc()
		wg.Done()
	}
	wg.Add(1)
	err := p.Submit(task)
	assert.Nil(t, err)
	assert.Equal(t, 1, p.Running())
	wg.Wait()

	for i := 0; i < 10; i++ {
		p.Submit(func() { demoFunc() })
		time.Sleep(time.Second * 1)
	}
	p.Release()
}
