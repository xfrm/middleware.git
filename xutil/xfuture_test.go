package xutil

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func Foo(ctx context.Context) (string, error) {
	return "bar", nil
}

func TestFutureGet(t *testing.T) {
	ctx := context.Background()
	var rv string
	f := NewFuture(func() error {
		var err error
		rv, err = Foo(ctx)
		return err
	})
	err := f.Get(ctx)
	assert.NoError(t, err)
	assert.Equal(t, "bar", rv)
	assert.Equal(t, true, f.Done())
}

func TestWaitAllFutures(t *testing.T) {
	f1 := NewFuture(func() error {
		time.Sleep(time.Second * 1)
		return nil
	})
	f2 := NewFuture(func() error {
		time.Sleep(time.Second * 1)
		return nil
	})
	WaitAllFutures(context.Background(), f1, f2)
	assert.Equal(t, true, f1.Done())
	assert.Equal(t, true, f2.Done())
}
