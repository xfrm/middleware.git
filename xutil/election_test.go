package xutil

import (
	"context"
	"fmt"
	"sync"
	"testing"
	"time"

	"gitee.com/xfrm/middleware/xlog"

	etcd "github.com/coreos/etcd/client"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
)

type ElectionSuite struct {
	suite.Suite

	keysAPI  etcd.KeysAPI
	election *Election
}

func (p *ElectionSuite) SetupSuite() {
	client, err := etcd.New(etcd.Config{
		Endpoints: []string{"http://infra0.etcd.ibanyu.com:20002"},
	})
	p.Require().NoError(err)
	keysAPI := etcd.NewKeysAPI(client)
	p.election, err = NewElectionV2(context.Background(), "test/test", []string{"http://infra0.etcd.ibanyu.com:20002"})
	p.Require().NoError(err)
	p.keysAPI = keysAPI
}

func (p *ElectionSuite) Test_setPrevValue() {
	ctx := context.Background()
	path := "/seaweed/lock/test/test/test"
	value := "localtest"

	// 修改value
	res, err := p.keysAPI.Set(ctx, path, value, &etcd.SetOptions{TTL: 10 * time.Second})
	p.NoError(err)
	fmt.Println(res)

	// 空值 prevValue set，设置成功
	err = p.election.setPrevValue(ctx, path, value, 10*time.Second)
	p.NoError(err)

	// 存在相同的值，err = nil
	err = p.election.setPrevValue(ctx, path, value, 10*time.Second)
	p.NoError(err)

	// 修改value
	res, err = p.keysAPI.Set(ctx, path, value+"1", &etcd.SetOptions{TTL: 10 * time.Second})
	p.NoError(err)
	fmt.Println(res)

	// 设置不同值 prev value set 返回 err
	err = p.election.setPrevValue(ctx, path, value, 10*time.Second)
	p.Error(err)
	p.Contains(err.Error(), "101: Compare failed")

	_, err = p.keysAPI.Delete(ctx, path, &etcd.DeleteOptions{})
	p.NoError(err)
}

func (p *ElectionSuite) Test_setPrevNoExist() {
	ctx := context.Background()
	path := "/seaweed/lock/test/test/no_exist"
	value := "localtest"

	err := p.election.setPrevNoExist(ctx, path, value, 10*time.Second)
	p.NoError(err)

	err = p.election.setPrevNoExist(ctx, path, value, 10*time.Second)
	p.Error(err)

	_, err = p.keysAPI.Delete(ctx, path, &etcd.DeleteOptions{})
	p.NoError(err)
}

func TestElection(t *testing.T) {
	suite.Run(t, new(ElectionSuite))
}

func TestCampaign(t *testing.T) {
	t.Run("模拟多个实例", func(t *testing.T) {
		ctx := context.Background()
		wg := sync.WaitGroup{}
		for i := 0; i < 10; i++ {
			wg.Add(1)
			go func(k int) {
				ctx1, _ := context.WithTimeout(ctx, 3*time.Second)
				election, err := NewElection(ctx1, "test/test")
				assert.NoError(t, err)
				assert.NotNil(t, election)
				t.Logf("协程: %d, 等待成为leader", k)
				err = election.Campaign(ctx1, &ElectionOptions{
					Name:  "test",
					Value: fmt.Sprintf("%d", k),
					TTL:   30 * time.Second,
				})
				assert.NoError(t, err)
				t.Logf("协程: %d, 成为leader", k)
				t.Logf("业务逻辑执行完毕，重新发起选举: %d", k)
				err = election.Resign(ctx1, &ElectionOptions{
					Name:  "test",
					Value: fmt.Sprintf("%d", k),
				})
				assert.NoError(t, err)
				wg.Done()
			}(i)
		}
		wg.Wait()
	})

	t.Run("节点值被改变", func(t *testing.T) {
		ctx := context.Background()
		wg := sync.WaitGroup{}
		election, err := NewElectionV2(ctx, "test/test", []string{"http://infra0.etcd.ibanyu.com:20002"})
		assert.NoError(t, err)

		opt := &ElectionOptions{
			Name: "test",
		}
		wg.Add(1)
		err = election.Campaign(ctx, opt)
		assert.NoError(t, err)
		go func() {
			time.Sleep(5 * time.Second)
			etcdClient, err := etcd.New(etcd.Config{
				Endpoints: []string{"http://infra0.etcd.ibanyu.com:20002"},
			})
			assert.NoError(t, err)
			keysAPI := etcd.NewKeysAPI(etcdClient)
			_, err = keysAPI.Set(ctx, election.path("test"), "test", &etcd.SetOptions{
				TTL: 120 * time.Second,
			})
			assert.NoError(t, err)
			wg.Done()
		}()
		wg.Wait()
	})
}

func TestCampaignDeadLine(t *testing.T) {
	ctx := context.Background()
	ctx, _ = context.WithTimeout(ctx, time.Second)
	election, err := NewElection(ctx, "test/test")
	assert.NoError(t, err)
	time.Sleep(time.Second)
	err = election.Campaign(ctx, &ElectionOptions{
		Name:  "test",
		Value: fmt.Sprintf("1"),
		TTL:   10 * time.Second,
	})
	assert.Error(t, err, "context deadline exceeded")
}

func TestClose(t *testing.T) {
	// 模拟单个服务中，有两个任务，关闭选举组件，两个任务相继退出
	t.Run("模拟单个服务中有两个任务", func(t *testing.T) {
		ctx := context.Background()
		oneOpt := &ElectionOptions{
			Name: "test1",
		}
		twoOpt := &ElectionOptions{
			Name: "test2",
		}
		election, err := NewElection(ctx, "test/test")
		assert.NoError(t, err)

		wg := sync.WaitGroup{}
		wg.Add(1)
		go func() {
			err = election.Campaign(ctx, oneOpt)
			assert.NoError(t, err)
			wg.Done()
		}()

		wg.Add(1)
		go func() {
			err = election.Campaign(ctx, twoOpt)
			assert.NoError(t, err)
			wg.Done()
		}()

		wg.Wait()
		election.Close()
	})
}

func TestTryCampaign(t *testing.T) {
	t.Run("模拟多个程序竞争选主", func(t *testing.T) {
		ctx := context.Background()
		election, err := NewElection(ctx, "test/test")
		if err != nil {
			xlog.Errorf(ctx, "初始化选举失败, err: %v", err)
			return
		}
		wg := sync.WaitGroup{}
		for i := 0; i < 10; i++ {
			wg.Add(1)
			go func(k int) {
				defer wg.Done()
				opt := &ElectionOptions{
					Name:  "test",
					Value: fmt.Sprintf("%d", k),
					TTL:   10 * time.Second,
				}
				xlog.Infof(ctx, "协程: %d, 等待成为leader", k)
				get, err := election.TryCampaign(ctx, opt)
				assert.NoError(t, err)
				if get {
					xlog.Infof(ctx, "协程: %d, 成为leader", k)
					time.Sleep(5 * time.Second)
					xlog.Infof(ctx, "业务逻辑执行完毕，重新发起选举: %d", k)
					election.Resign(ctx, opt)
					return
				}
				xlog.Infof(ctx, "election fail, return")
			}(i)
		}
		wg.Wait()
		election.Close()
	})

	t.Run("测试pre泳道是否生效", func(t *testing.T) {
		ctx := context.Background()
		election, err := NewElection(ctx, "test/test")
		assert.NoError(t, err)

		opt := &ElectionOptions{
			Name: "test",
			Lane: "pre",
		}
		ok, err := election.TryCampaign(ctx, opt)
		assert.False(t, ok)
		assert.Error(t, err, ERRPreLane)
	})
}
