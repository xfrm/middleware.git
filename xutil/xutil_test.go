package xutil

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIsNotFound(t *testing.T) {
	var ErrNoRows = errors.New("sql: no rows in result set")
	var ErrNotFound = errors.New("not found")
	var RedisNil = errors.New("redis: nil")
	var RPCRespNil = errors.New(RPCRespNil)
	var XsqlErrNilRows = errors.New(`[scanner]: empty result`)
	var XsqlErrEmptyResult = errors.New("[scanner]: rows can't be nil")

	err := errors.New("random")

	assert.Equal(t, true, IsNotFound(ErrNoRows))
	assert.Equal(t, true, IsNotFound(ErrNotFound))
	assert.Equal(t, true, IsNotFound(RedisNil))
	assert.Equal(t, true, IsNotFound(RPCRespNil))
	assert.Equal(t, true, IsNotFound(XsqlErrNilRows))
	assert.Equal(t, true, IsNotFound(XsqlErrEmptyResult))
	assert.Equal(t, false, IsNotFound(err))
}

func TestSafeFuncWrapper(t *testing.T) {
	f := func() {
		panic("test")
	}
	err := SafeFuncWrapper(f)
	assert.Error(t, err, "test")

	f = func() {
		t.Log("doing")
	}
	err = SafeFuncWrapper(f)
	assert.NoError(t, err)
}
