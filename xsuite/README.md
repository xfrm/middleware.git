## 简介
xsuite是一款单元测试套件库，对常用单元测试组件进行了封装及打桩

### Logic层

logic层主要对manager.GetDB进行了打桩，并使用testsql，mockDB两种方式对实际测试环境db进行了替换。

* testsql：访问真实测试数据库
* mockdb：通过mock数据来测试

#### LogicTestSqlSuite

* logic层测试套件，其中直接提供了可以使用的DB对象，通过DSN来创建
* 访问本地或docker真实测试数据库，通过指定文件生成表结构，灌入测试数据的方式进行测试
* 需提前准备好相关sql语句，同时可按需设置DSN，schema文件名称，fixture文件夹路径
* 需在使用某个数据文件时自行指定文件，使用后请清除，避免影响其他用例

example：

```go
import (
 "gitee.com/xfrm/middleware/xsuite"
)

type ExampleLogicTestSqlSuite struct {
	xsuite.LogicTestSqlSuite
}

func (p *ExampleLogicTestSqlSuite) TestQueryChange() {
	// 指定数据文件
	p.TS.Use("changeboard_change_board_event.sql")
	res := HandleGrpcChangeBoard.QueryChange(ctx, &QueryChangeRequest{
		ServiceName:          "rtcupgrade",
		BeginTime:            int32(time.Date(1995, 9, 2, 1,1,1,1, time.Local).Unix()),
		EndTime:              int32(time.Now().Unix()),
		ChangeType:           3,
		Offset:               0,
		Limit:                2,
	})
	p.Equal(2, len(res.Data.Items))
	// 清除数据
	p.TS.Clear()
}

func TestExampleLogicTestSqlSuite(t *testing.T) {
	testSqlSuite := &ExampleLogicTestSqlSuite{
		LogicTestSqlSuite: xsuite.LogicTestSqlSuite{
			DSN: "root:123456@tcp(hub.pri.ibanyu.com-devops-mysql:3306)/test_changeboard?charset=utf8mb4&parseTime=true&loc=Asia%2FShanghai",
			TableSchemaPath: "../testsql/schema.sql",
			FixtureDirPath: "../testsql/fixtures",
		},
	}
	suite.Run(t, testSqlSuite)
}
```

#### LogicMockDBSuite

* 提供创建好的DB对象
* 在测试前，需手动设置Mock对象，给出期望返回结果

```go
import (
 "gitee.com/xfrm/middleware/xsuite"
)

type ExampleMockDBSuite struct {
	xsuite.LogicMockDBSuite
}

func (p *ExampleMockDBSuite) TestQueryService() {
	// mock 数据
	rows := sqlmock.NewRows([]string{"id", "service_id", "service_name"}).
		AddRow(1, 5, "change_board")
	p.Mock.ExpectQuery("SELECT").WillReturnRows(rows).WillReturnError(nil)
	res := HandleGrpcChangeBoard.QueryChangeService(ctx, new(QueryChangeServiceRequest))
	p.Equal(1, len(res.Body.Items))
	p.Equal("change_board", res.Body.Items[0].ServiceName)
}

func TestExampleMockDBSuite(t *testing.T) {
	suite.Run(t, new(ExampleMockDBSuite))
}
```

### Dao层

dao层测试文件，会在生成dao层代码中直接生成好mockDB使用的样例。
同时提供了testsql，mockDB两种方式进行dao层测试。
下面是两种套件的使用样。

#### DaoTestSqlSuite

* 提供DB对象
* 访问真实数据库，通过灌数据的方式测试
* 需在使用某个数据文件时自行指定文件，使用后请清除，避免影响其他用例

```go
import (
 "gitee.com/xfrm/middleware/xsuite"
)

type ExampleDaoTestSqlSuite struct {
	xsuite.DaoTestSqlSuite
}

func (p *ExampleDaoTestSqlSuite) TestGetNumberChangeBoardEvent(){
	// 指定数据文件
	p.TS.Use("changeboard_change_board_event.sql")
	where := make(map[string]interface{})
    
	//  传入db
	count, err := GetNumberChangeBoardEvent(context.Background(), p.DB, where)
	p.NoError(err)
	p.Equal(int32(11), count)
	// 清除数据
	p.TS.Clear()
}

func TestExampleDaoTestSqlSuite(t *testing.T) {
	exampleDaoTestSql := &ExampleDaoTestSqlSuite{
		DaoTestSqlSuite: xsuite.DaoTestSqlSuite{
			DSN:             "root:123456@tcp(hub.pri.ibanyu.com-devops-mysql:3306)/test_changeboard?charset=utf8mb4",
			// schema 文件
			TableSchemaPath: "../../testsql/schema.sql",
			// fixtures 目录
			FixtureDirPath:  "../../testsql/fixtures",
		},
	}
	suite.Run(t, exampleDaoTestSql)
}
```

#### DaoMockDBSuite

* 提供DB对象
* 使用前，通过设置Mock对象，给出期望返回结果

example:

```go
import (
 "gitee.com/xfrm/middleware/xsuite"
)

type ExampleDaoMockDBSuite struct {
	xsuite.DaoMockDBSuite
}

func (p *ExampleDaoMockDBSuite) TestGetMultiChangeBoardEvent() {
	rows := sqlmock.NewRows([]string{"id", "service_name", "content"}).
		AddRow(1, "changeboard", "deploy a project")
	p.Mock.ExpectQuery("SELECT").
		WillReturnRows(rows).
		WillReturnError(nil)
	where := make(map[string]interface{})
	res, err := GetMultiChangeBoardEvent(context.Background(), p.DB, where)
	p.NoError(err)
	p.Equal(1, len(res))
	p.Equal("changeboard", res[0].ServiceName)
}

func TestExampleDaoMockDBSuite(t *testing.T) {
	suite.Run(t, new(ExampleDaoMockDBSuite))
}
```


