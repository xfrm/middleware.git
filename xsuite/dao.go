package xsuite

import (
	"database/sql"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/suite"
	"github.com/zhulongcheng/testsql"
)

// DaoTestSqlSuite dao层代码 testsql库
type DaoTestSqlSuite struct {
	suite.Suite

	DB *sql.DB          // 模拟库的DB连接
	TS *testsql.TestSQL // TestSQL 用于生成/清除 测试数据

	DSN             string // see https://github.com/go-sql-driver/mysql#dsn-data-source-name 数据源名称
	TableSchemaPath string // schema 文件路径
	FixtureDirPath  string // 准备数据文件夹路径
}

func (p *DaoTestSqlSuite) SetupSuite() {
	if p.DSN == "" {
		p.DSN = defaultTestSqlDSN
	}
	if p.TableSchemaPath == "" {
		p.TableSchemaPath = "../../testsql/schema.sql"
	}
	if p.FixtureDirPath == "" {
		p.FixtureDirPath = "../../testsql/fixtures"
	}
	ts := testsql.New(p.DSN, p.TableSchemaPath, p.FixtureDirPath)
	p.TS = ts
	p.DB = ts.DB
}

func (p *DaoTestSqlSuite) TearDownSuite() {
	p.TS.DropTestDB()
	p.DB.Close()
}

// DaoMockDBSuite
type DaoMockDBSuite struct {
	suite.Suite

	DB   *sql.DB         // mock db连接
	Mock sqlmock.Sqlmock // Sqlmock 用来设置期望值
}

func (p *DaoMockDBSuite) SetupSuite() {
	db, mock, err := sqlmock.New()
	p.NoError(err)

	p.Mock = mock
	p.DB = db
}

func (p *DaoMockDBSuite) TearDownSuite() {
	p.DB.Close()
}
